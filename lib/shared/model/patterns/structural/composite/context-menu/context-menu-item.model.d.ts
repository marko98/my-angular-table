import { Collection } from '../collection.model';
export declare interface ContextMenuItemValue {
    context: string;
    function: Function;
    imgSrc?: string;
}
export declare class ContextMenuItem extends Collection {
    context: string;
    f: Function;
    imgSrc?: string;
    constructor(context: string, f: Function, imgSrc?: string);
    addChild(child: Collection): boolean;
    removeChild(child: Collection): boolean;
    getChildIndex(child: Collection): number;
    shouldHaveChildren(): boolean;
}
