import { Collection } from '../collection.model';
import { ContextMenuItemValue, ContextMenuItem } from './context-menu-item.model';
export declare interface ContextMenuValue {
    contextMenuItems: ContextMenuItemValue[];
}
export declare class ContextMenu extends Collection {
    private contextMenuValue;
    constructor(contextMenuValue: ContextMenuValue);
    addChild(child: ContextMenuItem): boolean;
    addChildValue: (value: ContextMenuItemValue) => boolean;
    removeChild(child: ContextMenuItem): boolean;
    getChildIndex(child: ContextMenuItem): number;
    shouldHaveChildren(): boolean;
}
