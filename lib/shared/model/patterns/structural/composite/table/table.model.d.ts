import { Collection } from "../collection.model";
import { Row, RowValue, HeaderValue } from "./row/row.model";
import { ContextMenu, ContextMenuValue } from "../context-menu/context-menu.model";
export declare interface TableValue {
    header: HeaderValue;
    rows?: RowValue[];
    contextMenu?: ContextMenuValue;
}
export declare class Table extends Collection {
    private header;
    private sortedASC;
    private index;
    private contextMenu;
    constructor(tableValue?: TableValue);
    setContextMenuValue: (contextMenuValue: ContextMenuValue) => boolean;
    getContextMenu: () => ContextMenu;
    setHeader: (header: Row) => boolean;
    setHeaderValue: (headerValue: HeaderValue) => boolean;
    getHeader: () => Row;
    addChild: (child: Row) => boolean;
    addChildValue: (rowValue: RowValue) => boolean;
    removeChild: (child: Row) => boolean;
    removeChildren: () => boolean;
    getFilteredChildren: () => Row[];
    shouldHaveChildren: () => boolean;
    getChildIndex: (child: Row) => number;
    onSort: (index: number) => void;
    onASC: (index: number) => void;
    onDESC: (index: number) => void;
    validate: () => boolean;
}
