import { Collection } from '../../collection.model';
import { Prototype } from '../../../../creational/prototype/prototype.interface';
export declare interface RowItemValue {
    context: string | number | Date;
    imgSrc?: string;
    button?: RowItemButtonValue;
    rating?: RowItemRatingValue;
}
export declare interface RowItemHeaderValue {
    context: string | number | Date;
}
export declare interface RowItemButtonValue {
    function: Function;
}
export declare interface RowItemRatingValue {
    ratingImgSrc?: string;
}
export declare class RowItem extends Collection implements Prototype<RowItem> {
    context: string | number | Date;
    private imgSrc;
    private button;
    private function;
    private rating;
    private ratingImgSrc;
    constructor(context: string | number | Date);
    addChild: (child: Collection) => boolean;
    removeChild: (child: Collection) => boolean;
    shouldHaveChildren: () => boolean;
    getChildIndex: (child: Collection) => number;
    getImgSrc: () => string;
    setImgSrc: (imgSrc: string) => void;
    getButton: () => boolean;
    setButton: (button: boolean) => void;
    getFunction: () => Function;
    setFunction: (f: Function) => void;
    getRating: () => boolean;
    setRating: (rating: boolean) => void;
    getRatingImgSrc: () => string;
    setRatingImgSrc: (ratingImgSrc: string) => void;
    getRatingCollection: () => any[];
    clone: () => RowItem;
    prototype: (rowItem: RowItem) => void;
}
