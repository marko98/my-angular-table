import { Prototype } from "../../../../creational/prototype/prototype.interface";
import { Collection } from "../../collection.model";
import { RowItem, RowItemValue, RowItemHeaderValue } from "./row-item.model";
import { ElementRef } from "@angular/core";
import { BehaviorSubject } from "rxjs";
export declare interface RowValue {
    data: any;
    dragPreviewTempRefSubject?: BehaviorSubject<ElementRef>;
    color?: string;
    rowItems: RowItemValue[];
}
export declare interface HeaderValue {
    headerItems: RowItemHeaderValue[];
}
export declare class Row extends Collection implements Prototype<Row> {
    show: boolean;
    data: any[];
    color: string;
    dragPreviewTempRefSubject: BehaviorSubject<ElementRef>;
    constructor(color?: string);
    addChild: (child: RowItem) => boolean;
    addChildValue: (value: RowItemValue) => boolean;
    removeChild: (child: RowItem) => boolean;
    shouldHaveChildren: () => boolean;
    getChildIndex: (child: RowItem) => number;
    clone: () => Row;
    prototype: (row: Row) => void;
}
