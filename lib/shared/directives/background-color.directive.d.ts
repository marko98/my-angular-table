import { ElementRef, SimpleChanges, OnChanges } from "@angular/core";
export declare class LibElColorDirective implements OnChanges {
    private elRef;
    libElColorValue: string;
    constructor(elRef: ElementRef);
    ngOnChanges(changes: SimpleChanges): void;
}
