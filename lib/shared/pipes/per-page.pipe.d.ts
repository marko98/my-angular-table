import { PipeTransform } from '@angular/core';
export declare class PaginationPipe implements PipeTransform {
    transform(value: any[], data: {
        page: number;
        perPage: number;
    }): any[];
}
