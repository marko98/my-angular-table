import { MatSnackBar } from '@angular/material/snack-bar';
export declare class UiService {
    private matSnackBar;
    constructor(matSnackBar: MatSnackBar);
    onShowSnackBar: (message: string, action: any, duration: number) => void;
}
