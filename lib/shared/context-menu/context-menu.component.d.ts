import { ChangeDetectorRef } from '@angular/core';
import { ContextMenu } from '../model/patterns/structural/composite/context-menu/context-menu.model';
import { ContextMenuItem } from '../model/patterns/structural/composite/context-menu/context-menu-item.model';
export declare class ContextMenuComponent {
    private changeDetectorRef;
    contextMenuView: any;
    contextMenu: ContextMenu;
    data: any[];
    contextMenuData: {
        state: boolean;
        menuPositionX: any;
        menuPositionY: any;
        menuPosition: any;
        menuWidth: any;
        menuHeight: any;
        windowWidth: any;
        windowHeight: any;
    };
    constructor(changeDetectorRef: ChangeDetectorRef);
    setModel: (model: ContextMenu) => boolean;
    getContextMenuItems: () => ContextMenuItem[];
    showContextMenu: (event: MouseEvent) => void;
    positionMenu(event: MouseEvent): void;
    getPosition(event: MouseEvent): {
        x: number;
        y: number;
    };
    documentClick(event: MouseEvent): void;
    documentRClick(event: MouseEvent): void;
}
