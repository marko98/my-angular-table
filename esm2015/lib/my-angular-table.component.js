/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewChild, HostListener, Output, EventEmitter, ElementRef, } from "@angular/core";
// model
import { Table } from "./shared/model/patterns/structural/composite/table/table.model";
import { UiService } from "./shared/service/ui.service";
import { ContextMenuComponent } from "./shared/context-menu/context-menu.component";
import { CdkDropList, } from "@angular/cdk/drag-drop";
export class MyAngularTableComponent {
    /**
     * @param {?} uiService
     */
    constructor(uiService) {
        this.uiService = uiService;
        this.tempRef = undefined;
        this.criteria = "";
        this.selectedRows = [];
        this.isValid = false;
        this.perPage = 2;
        this.page = 0;
        this.onListInit = new EventEmitter();
        this.showContextMenu = false;
        this.onSort = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            // console.log(index);
            this.table.onSort(index);
            // console.log(this.table.getChildren());
        });
        // getRows = (): Row[] => {
        //   return <Row[]>this.table.getChildren();
        // }
        this.onCriteriaChange = (/**
         * @return {?}
         */
        () => {
            // console.log(this.criteria);
            this.table.getChildren().forEach((/**
             * @param {?} row
             * @return {?}
             */
            (row) => {
                /** @type {?} */
                let value = "";
                row.getChildren().forEach((/**
                 * @param {?} rowItem
                 * @return {?}
                 */
                (rowItem) => {
                    value += rowItem.context.toString().toLowerCase();
                }));
                if (value.includes(this.criteria))
                    row.show = true;
                else
                    row.show = false;
            }));
        });
        this.onShowSnackBar = (/**
         * @param {?} message
         * @return {?}
         */
        (message) => {
            this.uiService.onShowSnackBar(message, null, 1500);
        });
        this.onSelect = (/**
         * @param {?} event
         * @param {?} row
         * @return {?}
         */
        (event, row) => {
            if (event.ctrlKey) {
                if (this.selectedRows.find((/**
                 * @param {?} r
                 * @return {?}
                 */
                (r) => r === row))) {
                    this.selectedRows = this.selectedRows.filter((/**
                     * @param {?} r
                     * @return {?}
                     */
                    (r) => r !== row));
                }
                else {
                    this.selectedRows.push(row);
                }
            }
            else if (this.selectedRows.length > 0) {
                this.selectedRows = [];
            }
        });
        this.isSelected = (/**
         * @param {?} row
         * @return {?}
         */
        (row) => {
            if (this.selectedRows.find((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r === row))) {
                return true;
            }
            return false;
        });
        this.onContextMenu = (/**
         * @param {?} event
         * @param {?} row
         * @return {?}
         */
        (event, row) => {
            // console.log(event);
            // console.log(row);
            if (this.table.getContextMenu()) {
                this.showContextMenu = true;
                if (this.selectedRows.length > 0)
                    this.contextMenuComponent.data = this.selectedRows.map((/**
                     * @param {?} r
                     * @return {?}
                     */
                    (r) => r.data));
                else
                    this.contextMenuComponent.data = [row.data];
                this.contextMenuComponent.showContextMenu(event);
            }
        });
        this.next = (/**
         * @return {?}
         */
        () => {
            if (this.page + 1 <
                Math.ceil(this.table.getChildren().length / this.perPage))
                this.page++;
        });
        this.previous = (/**
         * @return {?}
         */
        () => {
            if (this.page > 0)
                this.page--;
        });
    }
    /**
     * @param {?} list
     * @return {?}
     */
    set list(list) {
        list.data = this.table;
        // console.log(list);
        this.onListInit.emit(list);
    }
    /**
     * @param {?} contextMenuComponent
     * @return {?}
     */
    set contextMenu(contextMenuComponent) {
        this.contextMenuComponent = contextMenuComponent;
        this.contextMenuComponent.setModel(this.table.getContextMenu());
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentClick(event) {
        this.showContextMenu = false;
        if (!event.ctrlKey)
            this.selectedRows = [];
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentRClick(event) {
        this.showContextMenu = false;
        if (!event.ctrlKey)
            this.selectedRows = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.isValid = this.table.validate();
        if (!this.isValid)
            throw new Error("Number of header items must be the same as the number in each row");
        // console.log("MyAngularTableComponent init");
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        // console.log(changes);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        // console.log("MyAngularTableComponent destroyed");
    }
}
MyAngularTableComponent.decorators = [
    { type: Component, args: [{
                selector: "lib-my-angular-table",
                template: "<style>\r\n\r\n    .example-list {\r\n        /* width: 500px; */\r\n        /* max-width: 100%; */\r\n        /* border: solid 1px #ccc; */\r\n        /* min-height: 60px; */\r\n        display: block;\r\n        /* background: white; */\r\n        /* border-radius: 4px; */\r\n        /* overflow: hidden; */\r\n    }\r\n\r\n    .example-box {\r\n        /* padding: 20px 10px; */\r\n        /* border-bottom: solid 1px #ccc; */\r\n        /* color: rgba(0, 0, 0, 0.87); */\r\n        /* display: flex; */\r\n        /* flex-direction: row; */\r\n        /* align-items: center; */\r\n        /* justify-content: space-between; */\r\n        /* box-sizing: border-box; */\r\n        cursor: move;\r\n        /* background: white; */\r\n        /* font-size: 14px; */\r\n    }\r\n\r\n    .cdk-drag-preview {\r\n        box-sizing: border-box;\r\n        border-radius: 4px;\r\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\r\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\r\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\r\n    }\r\n\r\n    .cdk-drag-placeholder {\r\n        opacity: 0;\r\n    }\r\n\r\n    .cdk-drag-animating {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n    .example-box:last-child {\r\n        border: none;\r\n    }\r\n\r\n    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n</style>\r\n\r\n<section\r\n    *ngIf=\"this.isValid\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\">\r\n        <section\r\n            class=\"no-padding\"\r\n            fxLayout=\"column\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"25px\">\r\n\r\n                <!-- FILTER -->\r\n                <mat-form-field>\r\n                    <mat-label>filter...</mat-label>\r\n                    <input \r\n                        matInput\r\n                        type=\"text\"\r\n                        [(ngModel)]=\"criteria\"\r\n                        (ngModelChange)=\"onCriteriaChange()\">\r\n                </mat-form-field>\r\n\r\n                \r\n                <section \r\n                    class=\"no-padding example-list\"\r\n                    cdkDropList\r\n                    #list=\"cdkDropList\">\r\n                    <!-- HEADER -->\r\n                    <section\r\n                        class=\"header\"\r\n                        fxLayout\r\n                        fxLayoutAlign=\"center center\"\r\n                        fxLayoutGap=\"10px\">\r\n                            <p\r\n                                class=\"header-item\"\r\n                                *ngFor=\"let headerItem of table.getHeader().getChildren()\"\r\n                                fxFlex\r\n                                (click)=\"onSort(headerItem.getParent().getChildIndex(headerItem))\">\r\n                                    {{ headerItem.context }}\r\n                            </p>\r\n                    </section>\r\n\r\n                    <section\r\n                        class=\"no-padding example-box\"\r\n                        cdkDrag\r\n                        *ngFor=\"let row of this.table.getFilteredChildren() | pagination: {page: this.page, perPage: this.perPage}\"\r\n                        libElColor\r\n                        [libElColorValue]=\"this.row.color\"\r\n                        [ngClass]=\"{'selected': this.isSelected(row)}\"\r\n                        (click)=\"onSelect($event, row)\"\r\n                        (contextmenu)=\"onContextMenu($event, row); false\">\r\n                            <section\r\n                                class=\"row\"\r\n                                *ngIf=\"row.show\"\r\n                                fxLayout\r\n                                fxLayoutAlign=\"start center\"\r\n                                fxLayoutGap=\"10px\">\r\n\r\n                                    <section\r\n                                        class=\"row-item no-padding\"\r\n                                        *ngFor=\"let rowItem of row.getChildren()\"\r\n                                        fxLayoutAlign=\"start center\"\r\n                                        fxLayoutGap=\"10px\"\r\n                                        fxFlex>\r\n                                            <img \r\n                                                fxHide.xs=\"true\"\r\n                                                *ngIf=\"rowItem.getImgSrc() ? true : false\"\r\n                                                [src]=\"rowItem.getImgSrc()\" \r\n                                                alt=\"row icon\">\r\n                                            <p \r\n                                                *ngIf=\"!rowItem.getButton() && !rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }}\r\n                                            </p>\r\n                                            <section\r\n                                                class=\"no-padding\"\r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\">\r\n                                                <img \r\n                                                    class=\"rating\"\r\n                                                    *ngFor=\"let i of rowItem.getRatingCollection()\"\r\n                                                    [src]=\"rowItem.getRatingImgSrc()\" \r\n                                                    alt=\"row icon\">\r\n                                            </section>\r\n                                            <!-- <p \r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }} rating\r\n                                            </p> -->\r\n                                            <button\r\n                                                *ngIf=\"rowItem.getButton()\"\r\n                                                (click)=\"rowItem.getFunction()([row.data])\"\r\n                                                class=\"row-item\">\r\n                                                    {{ rowItem.context }}\r\n                                            </button>\r\n                                    </section>\r\n                            </section>                            \r\n\r\n                            <section \r\n                                style=\"padding: 0;\"\r\n                                *ngIf='this.tempRef'>\r\n                                    <section *cdkDragPreview>\r\n                                        <section \r\n                                            *ngIf=\"false else this.tempRef\"></section>\r\n                                    </section>\r\n                            </section>\r\n\r\n                    </section>\r\n                </section>\r\n                \r\n                <section\r\n                    fxLayout\r\n                    fxLayoutAlign=\"center center\"\r\n                    fxLayoutGap=\"50px\">\r\n                    <mat-form-field\r\n                        fxFlex=\"50px\">\r\n                            <mat-label>per page:</mat-label>\r\n                            <input \r\n                                matInput\r\n                                type=\"number\"\r\n                                min=\"1\"\r\n                                [(ngModel)]=\"this.perPage\">\r\n                    </mat-form-field>\r\n                    <mat-button-toggle-group appearance=\"legacy\">\r\n                        <mat-button-toggle\r\n                            (click)=\"previous()\">\r\n                                <mat-icon>keyboard_arrow_left</mat-icon>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle\r\n                            (click)=\"next()\">\r\n                                <mat-icon>keyboard_arrow_right</mat-icon>\r\n                        </mat-button-toggle>\r\n                    </mat-button-toggle-group>\r\n                </section>\r\n        </section>\r\n\r\n</section>\r\n\r\n<lib-my-angular-table-context-menu\r\n    [hidden]=\"!this.showContextMenu\"></lib-my-angular-table-context-menu>",
                styles: ["section{padding:10px}section.header{width:80vw;height:25px;padding:20px;border-top:1px solid grey;border-bottom:1px solid #d3d3d3}p.header-item{color:silver;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}p.header-item:hover{cursor:pointer;border-radius:2px;box-shadow:0 1px #d3d3d3}section.row{width:80vw;height:25px;padding:20px;box-shadow:0 1px #d3d3d3}section.row-item{cursor:default;overflow:hidden}p.row-item{color:grey;text-align:center;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item{overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item:focus{outline:0}.no-padding{padding:0}.box-shadow{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}mat-form-field{max-width:300px}img{width:15%;height:15%;border-radius:40%}img.rating{width:12%;height:12%}hr{width:20px}button{border:none;background-color:transparent;cursor:pointer;height:30px;border-radius:5px}button:hover{background-color:#ebebeb}.no-margin{margin:0}.selected{background-color:rgba(221,221,221,.2);box-shadow:0 1px 1px rgba(0,0,0,.2)}"]
            }] }
];
/** @nocollapse */
MyAngularTableComponent.ctorParameters = () => [
    { type: UiService }
];
MyAngularTableComponent.propDecorators = {
    tempRef: [{ type: Input }],
    table: [{ type: Input }],
    perPage: [{ type: Input }],
    onListInit: [{ type: Output }],
    list: [{ type: ViewChild, args: ["list", { static: false },] }],
    contextMenu: [{ type: ViewChild, args: [ContextMenuComponent, { static: false },] }],
    documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }],
    documentRClick: [{ type: HostListener, args: ["document:contextmenu", ["$event"],] }]
};
if (false) {
    /** @type {?} */
    MyAngularTableComponent.prototype.tempRef;
    /** @type {?} */
    MyAngularTableComponent.prototype.table;
    /** @type {?} */
    MyAngularTableComponent.prototype.criteria;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.contextMenuComponent;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.selectedRows;
    /** @type {?} */
    MyAngularTableComponent.prototype.isValid;
    /** @type {?} */
    MyAngularTableComponent.prototype.perPage;
    /** @type {?} */
    MyAngularTableComponent.prototype.page;
    /** @type {?} */
    MyAngularTableComponent.prototype.onListInit;
    /** @type {?} */
    MyAngularTableComponent.prototype.showContextMenu;
    /** @type {?} */
    MyAngularTableComponent.prototype.onSort;
    /** @type {?} */
    MyAngularTableComponent.prototype.onCriteriaChange;
    /** @type {?} */
    MyAngularTableComponent.prototype.onShowSnackBar;
    /** @type {?} */
    MyAngularTableComponent.prototype.onSelect;
    /** @type {?} */
    MyAngularTableComponent.prototype.isSelected;
    /** @type {?} */
    MyAngularTableComponent.prototype.onContextMenu;
    /** @type {?} */
    MyAngularTableComponent.prototype.next;
    /** @type {?} */
    MyAngularTableComponent.prototype.previous;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.uiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktYW5ndWxhci10YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL215LWFuZ3VsYXItdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULEtBQUssRUFHTCxTQUFTLEVBQ1QsWUFBWSxFQUNaLE1BQU0sRUFDTixZQUFZLEVBR1osVUFBVSxHQUNYLE1BQU0sZUFBZSxDQUFDOztBQUd2QixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZ0VBQWdFLENBQUM7QUFHdkYsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3BGLE9BQU8sRUFHTCxXQUFXLEdBRVosTUFBTSx3QkFBd0IsQ0FBQztBQU9oQyxNQUFNLE9BQU8sdUJBQXVCOzs7O0lBNkJsQyxZQUFvQixTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBNUIvQixZQUFPLEdBQWdCLFNBQVMsQ0FBQztRQUVuQyxhQUFRLEdBQVcsRUFBRSxDQUFDO1FBRXJCLGlCQUFZLEdBQVUsRUFBRSxDQUFDO1FBQzFCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFDdkIsWUFBTyxHQUFXLENBQUMsQ0FBQztRQUN0QixTQUFJLEdBQVcsQ0FBQyxDQUFDO1FBRWQsZUFBVSxHQUE4QixJQUFJLFlBQVksRUFFL0QsQ0FBQztRQWVHLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBSXhDLFdBQU07Ozs7UUFBRyxDQUFDLEtBQWEsRUFBUSxFQUFFO1lBQy9CLHNCQUFzQjtZQUV0QixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUV6Qix5Q0FBeUM7UUFDM0MsQ0FBQyxFQUFDOzs7O1FBTUYscUJBQWdCOzs7UUFBRyxHQUFTLEVBQUU7WUFDNUIsOEJBQThCO1lBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTzs7OztZQUFDLENBQUMsR0FBUSxFQUFFLEVBQUU7O29CQUN4QyxLQUFLLEdBQUcsRUFBRTtnQkFDZCxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTzs7OztnQkFBQyxDQUFDLE9BQWdCLEVBQUUsRUFBRTtvQkFDN0MsS0FBSyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ3BELENBQUMsRUFBQyxDQUFDO2dCQUNILElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUFFLEdBQUcsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDOztvQkFDOUMsR0FBRyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7WUFDeEIsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUM7UUFFRixtQkFBYzs7OztRQUFHLENBQUMsT0FBZSxFQUFRLEVBQUU7WUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUM7UUFFRixhQUFROzs7OztRQUFHLENBQUMsS0FBaUIsRUFBRSxHQUFRLEVBQVEsRUFBRTtZQUMvQyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJOzs7O2dCQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFDLEVBQUU7b0JBQzVDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNOzs7O29CQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFDLENBQUM7aUJBQ2hFO3FCQUFNO29CQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUM3QjthQUNGO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QyxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQzthQUN4QjtRQUNILENBQUMsRUFBQztRQUVGLGVBQVU7Ozs7UUFBRyxDQUFDLEdBQVEsRUFBVyxFQUFFO1lBQ2pDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxHQUFHLEVBQUMsRUFBRTtnQkFDNUMsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDO1FBRUYsa0JBQWE7Ozs7O1FBQUcsQ0FBQyxLQUFpQixFQUFFLEdBQVEsRUFBUSxFQUFFO1lBQ3BELHNCQUFzQjtZQUN0QixvQkFBb0I7WUFFcEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxFQUFFO2dCQUMvQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztnQkFFNUIsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDO29CQUM5QixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRzs7OztvQkFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBQyxDQUFDOztvQkFDbkUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFakQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNsRDtRQUNILENBQUMsRUFBQztRQWtCRixTQUFJOzs7UUFBRyxHQUFTLEVBQUU7WUFDaEIsSUFDRSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUM7Z0JBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2dCQUV6RCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQyxFQUFDO1FBRUYsYUFBUTs7O1FBQUcsR0FBUyxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDO2dCQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNqQyxDQUFDLEVBQUM7SUExRnlDLENBQUM7Ozs7O0lBaEI1QyxJQUEwQyxJQUFJLENBQUMsSUFBaUI7UUFDOUQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3ZCLHFCQUFxQjtRQUVyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM3QixDQUFDOzs7OztJQUVELElBQXdELFdBQVcsQ0FDakUsb0JBQXlCO1FBRXpCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxvQkFBb0IsQ0FBQztRQUNqRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztJQUNsRSxDQUFDOzs7OztJQW9Fa0QsYUFBYSxDQUM5RCxLQUFpQjtRQUVqQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUU3QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87WUFBRSxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUV3RCxjQUFjLENBQ3JFLEtBQWlCO1FBRWpCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBRTdCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTztZQUFFLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQzdDLENBQUM7Ozs7SUFjRCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRXJDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUNmLE1BQU0sSUFBSSxLQUFLLENBQ2IsbUVBQW1FLENBQ3BFLENBQUM7UUFFSiwrQ0FBK0M7SUFDakQsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMseUdBQXlHO1FBQ3pHLDZDQUE2QztRQUM3Qyx3QkFBd0I7SUFDMUIsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxvREFBb0Q7SUFDdEQsQ0FBQzs7O1lBakpGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsc0JBQXNCO2dCQUNoQyxtb1JBQWdEOzthQUVqRDs7OztZQWJRLFNBQVM7OztzQkFlZixLQUFLO29CQUNMLEtBQUs7c0JBS0wsS0FBSzt5QkFHTCxNQUFNO21CQUdOLFNBQVMsU0FBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOzBCQU9uQyxTQUFTLFNBQUMsb0JBQW9CLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOzRCQXlFakQsWUFBWSxTQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDOzZCQVF6QyxZQUFZLFNBQUMsc0JBQXNCLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7SUFwR2hELDBDQUEwQzs7SUFDMUMsd0NBQXNCOztJQUN0QiwyQ0FBNkI7Ozs7O0lBQzdCLHVEQUFtRDs7Ozs7SUFDbkQsK0NBQWlDOztJQUNqQywwQ0FBZ0M7O0lBQ2hDLDBDQUE2Qjs7SUFDN0IsdUNBQXdCOztJQUV4Qiw2Q0FFSTs7SUFlSixrREFBd0M7O0lBSXhDLHlDQU1FOztJQU1GLG1EQVVFOztJQUVGLGlEQUVFOztJQUVGLDJDQVVFOztJQUVGLDZDQUtFOztJQUVGLGdEQWFFOztJQWtCRix1Q0FNRTs7SUFFRiwyQ0FFRTs7Ozs7SUExRlUsNENBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgQ29tcG9uZW50LFxuICBPbkluaXQsXG4gIElucHV0LFxuICBPbkRlc3Ryb3ksXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxuICBWaWV3Q2hpbGQsXG4gIEhvc3RMaXN0ZW5lcixcbiAgT3V0cHV0LFxuICBFdmVudEVtaXR0ZXIsXG4gIFNpbXBsZUNoYW5nZXMsXG4gIE9uQ2hhbmdlcyxcbiAgRWxlbWVudFJlZixcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcblxuLy8gbW9kZWxcbmltcG9ydCB7IFRhYmxlIH0gZnJvbSBcIi4vc2hhcmVkL21vZGVsL3BhdHRlcm5zL3N0cnVjdHVyYWwvY29tcG9zaXRlL3RhYmxlL3RhYmxlLm1vZGVsXCI7XG5pbXBvcnQgeyBSb3cgfSBmcm9tIFwiLi9zaGFyZWQvbW9kZWwvcGF0dGVybnMvc3RydWN0dXJhbC9jb21wb3NpdGUvdGFibGUvcm93L3Jvdy5tb2RlbFwiO1xuaW1wb3J0IHsgUm93SXRlbSB9IGZyb20gXCIuL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS90YWJsZS9yb3cvcm93LWl0ZW0ubW9kZWxcIjtcbmltcG9ydCB7IFVpU2VydmljZSB9IGZyb20gXCIuL3NoYXJlZC9zZXJ2aWNlL3VpLnNlcnZpY2VcIjtcbmltcG9ydCB7IENvbnRleHRNZW51Q29tcG9uZW50IH0gZnJvbSBcIi4vc2hhcmVkL2NvbnRleHQtbWVudS9jb250ZXh0LW1lbnUuY29tcG9uZW50XCI7XG5pbXBvcnQge1xuICBDZGtEcmFnRHJvcCxcbiAgbW92ZUl0ZW1JbkFycmF5LFxuICBDZGtEcm9wTGlzdCxcbiAgdHJhbnNmZXJBcnJheUl0ZW0sXG59IGZyb20gXCJAYW5ndWxhci9jZGsvZHJhZy1kcm9wXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJsaWItbXktYW5ndWxhci10YWJsZVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL215LWFuZ3VsYXItdGFibGUuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL215LWFuZ3VsYXItdGFibGUuY29tcG9uZW50LmNzc1wiXSxcbn0pXG5leHBvcnQgY2xhc3MgTXlBbmd1bGFyVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzIHtcbiAgQElucHV0KCkgdGVtcFJlZj86IEVsZW1lbnRSZWYgPSB1bmRlZmluZWQ7XG4gIEBJbnB1dCgpIHRhYmxlOiBUYWJsZTtcbiAgcHVibGljIGNyaXRlcmlhOiBzdHJpbmcgPSBcIlwiO1xuICBwcml2YXRlIGNvbnRleHRNZW51Q29tcG9uZW50OiBDb250ZXh0TWVudUNvbXBvbmVudDtcbiAgcHJpdmF0ZSBzZWxlY3RlZFJvd3M6IGFueVtdID0gW107XG4gIHB1YmxpYyBpc1ZhbGlkOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpIHBlclBhZ2U6IG51bWJlciA9IDI7XG4gIHB1YmxpYyBwYWdlOiBudW1iZXIgPSAwO1xuXG4gIEBPdXRwdXQoKSBvbkxpc3RJbml0OiBFdmVudEVtaXR0ZXI8Q2RrRHJvcExpc3Q+ID0gbmV3IEV2ZW50RW1pdHRlcjxcbiAgICBDZGtEcm9wTGlzdFxuICA+KCk7XG4gIEBWaWV3Q2hpbGQoXCJsaXN0XCIsIHsgc3RhdGljOiBmYWxzZSB9KSBzZXQgbGlzdChsaXN0OiBDZGtEcm9wTGlzdCkge1xuICAgIGxpc3QuZGF0YSA9IHRoaXMudGFibGU7XG4gICAgLy8gY29uc29sZS5sb2cobGlzdCk7XG5cbiAgICB0aGlzLm9uTGlzdEluaXQuZW1pdChsaXN0KTtcbiAgfVxuXG4gIEBWaWV3Q2hpbGQoQ29udGV4dE1lbnVDb21wb25lbnQsIHsgc3RhdGljOiBmYWxzZSB9KSBzZXQgY29udGV4dE1lbnUoXG4gICAgY29udGV4dE1lbnVDb21wb25lbnQ6IGFueVxuICApIHtcbiAgICB0aGlzLmNvbnRleHRNZW51Q29tcG9uZW50ID0gY29udGV4dE1lbnVDb21wb25lbnQ7XG4gICAgdGhpcy5jb250ZXh0TWVudUNvbXBvbmVudC5zZXRNb2RlbCh0aGlzLnRhYmxlLmdldENvbnRleHRNZW51KCkpO1xuICB9XG5cbiAgcHVibGljIHNob3dDb250ZXh0TWVudTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgdWlTZXJ2aWNlOiBVaVNlcnZpY2UpIHt9XG5cbiAgb25Tb3J0ID0gKGluZGV4OiBudW1iZXIpOiB2b2lkID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyhpbmRleCk7XG5cbiAgICB0aGlzLnRhYmxlLm9uU29ydChpbmRleCk7XG5cbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnRhYmxlLmdldENoaWxkcmVuKCkpO1xuICB9O1xuXG4gIC8vIGdldFJvd3MgPSAoKTogUm93W10gPT4ge1xuICAvLyAgIHJldHVybiA8Um93W10+dGhpcy50YWJsZS5nZXRDaGlsZHJlbigpO1xuICAvLyB9XG5cbiAgb25Dcml0ZXJpYUNoYW5nZSA9ICgpOiB2b2lkID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmNyaXRlcmlhKTtcbiAgICB0aGlzLnRhYmxlLmdldENoaWxkcmVuKCkuZm9yRWFjaCgocm93OiBSb3cpID0+IHtcbiAgICAgIGxldCB2YWx1ZSA9IFwiXCI7XG4gICAgICByb3cuZ2V0Q2hpbGRyZW4oKS5mb3JFYWNoKChyb3dJdGVtOiBSb3dJdGVtKSA9PiB7XG4gICAgICAgIHZhbHVlICs9IHJvd0l0ZW0uY29udGV4dC50b1N0cmluZygpLnRvTG93ZXJDYXNlKCk7XG4gICAgICB9KTtcbiAgICAgIGlmICh2YWx1ZS5pbmNsdWRlcyh0aGlzLmNyaXRlcmlhKSkgcm93LnNob3cgPSB0cnVlO1xuICAgICAgZWxzZSByb3cuc2hvdyA9IGZhbHNlO1xuICAgIH0pO1xuICB9O1xuXG4gIG9uU2hvd1NuYWNrQmFyID0gKG1lc3NhZ2U6IHN0cmluZyk6IHZvaWQgPT4ge1xuICAgIHRoaXMudWlTZXJ2aWNlLm9uU2hvd1NuYWNrQmFyKG1lc3NhZ2UsIG51bGwsIDE1MDApO1xuICB9O1xuXG4gIG9uU2VsZWN0ID0gKGV2ZW50OiBNb3VzZUV2ZW50LCByb3c6IFJvdyk6IHZvaWQgPT4ge1xuICAgIGlmIChldmVudC5jdHJsS2V5KSB7XG4gICAgICBpZiAodGhpcy5zZWxlY3RlZFJvd3MuZmluZCgocikgPT4gciA9PT0gcm93KSkge1xuICAgICAgICB0aGlzLnNlbGVjdGVkUm93cyA9IHRoaXMuc2VsZWN0ZWRSb3dzLmZpbHRlcigocikgPT4gciAhPT0gcm93KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWRSb3dzLnB1c2gocm93KTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHRoaXMuc2VsZWN0ZWRSb3dzLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuc2VsZWN0ZWRSb3dzID0gW107XG4gICAgfVxuICB9O1xuXG4gIGlzU2VsZWN0ZWQgPSAocm93OiBSb3cpOiBib29sZWFuID0+IHtcbiAgICBpZiAodGhpcy5zZWxlY3RlZFJvd3MuZmluZCgocikgPT4gciA9PT0gcm93KSkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfTtcblxuICBvbkNvbnRleHRNZW51ID0gKGV2ZW50OiBNb3VzZUV2ZW50LCByb3c6IFJvdyk6IHZvaWQgPT4ge1xuICAgIC8vIGNvbnNvbGUubG9nKGV2ZW50KTtcbiAgICAvLyBjb25zb2xlLmxvZyhyb3cpO1xuXG4gICAgaWYgKHRoaXMudGFibGUuZ2V0Q29udGV4dE1lbnUoKSkge1xuICAgICAgdGhpcy5zaG93Q29udGV4dE1lbnUgPSB0cnVlO1xuXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZFJvd3MubGVuZ3RoID4gMClcbiAgICAgICAgdGhpcy5jb250ZXh0TWVudUNvbXBvbmVudC5kYXRhID0gdGhpcy5zZWxlY3RlZFJvd3MubWFwKChyKSA9PiByLmRhdGEpO1xuICAgICAgZWxzZSB0aGlzLmNvbnRleHRNZW51Q29tcG9uZW50LmRhdGEgPSBbcm93LmRhdGFdO1xuXG4gICAgICB0aGlzLmNvbnRleHRNZW51Q29tcG9uZW50LnNob3dDb250ZXh0TWVudShldmVudCk7XG4gICAgfVxuICB9O1xuXG4gIEBIb3N0TGlzdGVuZXIoXCJkb2N1bWVudDpjbGlja1wiLCBbXCIkZXZlbnRcIl0pIHB1YmxpYyBkb2N1bWVudENsaWNrKFxuICAgIGV2ZW50OiBNb3VzZUV2ZW50XG4gICk6IHZvaWQge1xuICAgIHRoaXMuc2hvd0NvbnRleHRNZW51ID0gZmFsc2U7XG5cbiAgICBpZiAoIWV2ZW50LmN0cmxLZXkpIHRoaXMuc2VsZWN0ZWRSb3dzID0gW107XG4gIH1cblxuICBASG9zdExpc3RlbmVyKFwiZG9jdW1lbnQ6Y29udGV4dG1lbnVcIiwgW1wiJGV2ZW50XCJdKSBwdWJsaWMgZG9jdW1lbnRSQ2xpY2soXG4gICAgZXZlbnQ6IE1vdXNlRXZlbnRcbiAgKTogdm9pZCB7XG4gICAgdGhpcy5zaG93Q29udGV4dE1lbnUgPSBmYWxzZTtcblxuICAgIGlmICghZXZlbnQuY3RybEtleSkgdGhpcy5zZWxlY3RlZFJvd3MgPSBbXTtcbiAgfVxuXG4gIG5leHQgPSAoKTogdm9pZCA9PiB7XG4gICAgaWYgKFxuICAgICAgdGhpcy5wYWdlICsgMSA8XG4gICAgICBNYXRoLmNlaWwodGhpcy50YWJsZS5nZXRDaGlsZHJlbigpLmxlbmd0aCAvIHRoaXMucGVyUGFnZSlcbiAgICApXG4gICAgICB0aGlzLnBhZ2UrKztcbiAgfTtcblxuICBwcmV2aW91cyA9ICgpOiB2b2lkID0+IHtcbiAgICBpZiAodGhpcy5wYWdlID4gMCkgdGhpcy5wYWdlLS07XG4gIH07XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5pc1ZhbGlkID0gdGhpcy50YWJsZS52YWxpZGF0ZSgpO1xuXG4gICAgaWYgKCF0aGlzLmlzVmFsaWQpXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgIFwiTnVtYmVyIG9mIGhlYWRlciBpdGVtcyBtdXN0IGJlIHRoZSBzYW1lIGFzIHRoZSBudW1iZXIgaW4gZWFjaCByb3dcIlxuICAgICAgKTtcblxuICAgIC8vIGNvbnNvbGUubG9nKFwiTXlBbmd1bGFyVGFibGVDb21wb25lbnQgaW5pdFwiKTtcbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICAvL0NhbGxlZCBiZWZvcmUgYW55IG90aGVyIGxpZmVjeWNsZSBob29rLiBVc2UgaXQgdG8gaW5qZWN0IGRlcGVuZGVuY2llcywgYnV0IGF2b2lkIGFueSBzZXJpb3VzIHdvcmsgaGVyZS5cbiAgICAvL0FkZCAnJHtpbXBsZW1lbnRzIE9uQ2hhbmdlc30nIHRvIHRoZSBjbGFzcy5cbiAgICAvLyBjb25zb2xlLmxvZyhjaGFuZ2VzKTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIC8vIGNvbnNvbGUubG9nKFwiTXlBbmd1bGFyVGFibGVDb21wb25lbnQgZGVzdHJveWVkXCIpO1xuICB9XG59XG4iXX0=