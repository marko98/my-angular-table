/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
export class PaginationPipe {
    /**
     * @param {?} value
     * @param {?} data
     * @return {?}
     */
    transform(value, data) {
        // console.log(data);
        return value.slice(data.page * data.perPage, data.page * data.perPage + data.perPage);
    }
}
PaginationPipe.decorators = [
    { type: Pipe, args: [{ name: 'pagination' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyLXBhZ2UucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3BpcGVzL3Blci1wYWdlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBR3BELE1BQU0sT0FBTyxjQUFjOzs7Ozs7SUFDekIsU0FBUyxDQUFDLEtBQVksRUFBRSxJQUFxQztRQUUzRCxxQkFBcUI7UUFFckIsT0FBTyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxPQUFPLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2xGLENBQUM7OztZQVBGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxZQUFZLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7bmFtZTogJ3BhZ2luYXRpb24nfSlcclxuZXhwb3J0IGNsYXNzIFBhZ2luYXRpb25QaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcbiAgdHJhbnNmb3JtKHZhbHVlOiBhbnlbXSwgZGF0YToge3BhZ2U6IG51bWJlciwgcGVyUGFnZTogbnVtYmVyfSk6IGFueVtdIHtcclxuXHJcbiAgICAvLyBjb25zb2xlLmxvZyhkYXRhKTtcclxuXHJcbiAgICByZXR1cm4gdmFsdWUuc2xpY2UoZGF0YS5wYWdlKmRhdGEucGVyUGFnZSwgZGF0YS5wYWdlKmRhdGEucGVyUGFnZStkYXRhLnBlclBhZ2UpO1xyXG4gIH1cclxufSJdfQ==