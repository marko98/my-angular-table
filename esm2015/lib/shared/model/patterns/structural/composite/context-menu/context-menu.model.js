/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// model
import { Collection } from '../collection.model';
import { ContextMenuItem } from './context-menu-item.model';
export class ContextMenu extends Collection {
    /**
     * @param {?} contextMenuValue
     */
    constructor(contextMenuValue) {
        super();
        this.contextMenuValue = contextMenuValue;
        this.addChildValue = (/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            if (value.context && value.function) {
                /** @type {?} */
                let i = new ContextMenuItem(value.context, value.function, value.imgSrc);
                this.addChild(i);
                return true;
            }
            return false;
        });
        contextMenuValue.contextMenuItems.forEach((/**
         * @param {?} i
         * @return {?}
         */
        (i) => {
            this.addChildValue(i);
        }));
    }
    /**
     * @param {?} child
     * @return {?}
     */
    addChild(child) {
        this.children.push(child);
        child.setParent(this);
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    removeChild(child) {
        this.children = this.children.filter((/**
         * @param {?} c
         * @return {?}
         */
        c => c !== child));
        child.setParent(undefined);
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    getChildIndex(child) {
        return this.children.findIndex((/**
         * @param {?} c
         * @return {?}
         */
        c => c === child));
    }
    /**
     * @return {?}
     */
    shouldHaveChildren() {
        return true;
    }
}
if (false) {
    /** @type {?} */
    ContextMenu.prototype.addChildValue;
    /**
     * @type {?}
     * @private
     */
    ContextMenu.prototype.contextMenuValue;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvcGF0dGVybnMvc3RydWN0dXJhbC9jb21wb3NpdGUvY29udGV4dC1tZW51L2NvbnRleHQtbWVudS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNqRCxPQUFPLEVBQXdCLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBTWxGLE1BQU0sT0FBTyxXQUFZLFNBQVEsVUFBVTs7OztJQUV2QyxZQUFvQixnQkFBa0M7UUFDbEQsS0FBSyxFQUFFLENBQUM7UUFEUSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBYXRELGtCQUFhOzs7O1FBQUcsQ0FBQyxLQUEyQixFQUFXLEVBQUU7WUFDckQsSUFBRyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQUM7O29CQUUzQixDQUFDLEdBQUcsSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUM7Z0JBQ3hFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBRWpCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDLEVBQUE7UUFwQkcsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsT0FBTzs7OztRQUFDLENBQUMsQ0FBdUIsRUFBRSxFQUFFO1lBQ2xFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFzQjtRQUMzQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBYUQsV0FBVyxDQUFDLEtBQXNCO1FBQzlCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxFQUFDLENBQUM7UUFDdkQsS0FBSyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzQixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxLQUFzQjtRQUNoQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBQyxDQUFDO0lBQ3JELENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDZCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBRUo7OztJQXpCRyxvQ0FTQzs7Ozs7SUF0QlcsdUNBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4uL2NvbGxlY3Rpb24ubW9kZWwnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudUl0ZW1WYWx1ZSwgQ29udGV4dE1lbnVJdGVtIH0gZnJvbSAnLi9jb250ZXh0LW1lbnUtaXRlbS5tb2RlbCc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgQ29udGV4dE1lbnVWYWx1ZSB7XHJcbiAgICBjb250ZXh0TWVudUl0ZW1zOiBDb250ZXh0TWVudUl0ZW1WYWx1ZVtdXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudSBleHRlbmRzIENvbGxlY3Rpb24ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udGV4dE1lbnVWYWx1ZTogQ29udGV4dE1lbnVWYWx1ZSl7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBjb250ZXh0TWVudVZhbHVlLmNvbnRleHRNZW51SXRlbXMuZm9yRWFjaCgoaTogQ29udGV4dE1lbnVJdGVtVmFsdWUpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZFZhbHVlKGkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZENoaWxkKGNoaWxkOiBDb250ZXh0TWVudUl0ZW0pOiBib29sZWFuIHtcclxuICAgICAgICB0aGlzLmNoaWxkcmVuLnB1c2goY2hpbGQpO1xyXG4gICAgICAgIGNoaWxkLnNldFBhcmVudCh0aGlzKTtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRDaGlsZFZhbHVlID0gKHZhbHVlOiBDb250ZXh0TWVudUl0ZW1WYWx1ZSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgICAgIGlmKHZhbHVlLmNvbnRleHQgJiYgdmFsdWUuZnVuY3Rpb24pe1xyXG5cclxuICAgICAgICAgICAgbGV0IGkgPSBuZXcgQ29udGV4dE1lbnVJdGVtKHZhbHVlLmNvbnRleHQsIHZhbHVlLmZ1bmN0aW9uLCB2YWx1ZS5pbWdTcmMpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkKGkpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVDaGlsZChjaGlsZDogQ29udGV4dE1lbnVJdGVtKTogYm9vbGVhbiB7XHJcbiAgICAgICAgdGhpcy5jaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW4uZmlsdGVyKGMgPT4gYyAhPT0gY2hpbGQpO1xyXG4gICAgICAgIGNoaWxkLnNldFBhcmVudCh1bmRlZmluZWQpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENoaWxkSW5kZXgoY2hpbGQ6IENvbnRleHRNZW51SXRlbSk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2hpbGRyZW4uZmluZEluZGV4KGMgPT4gYyA9PT0gY2hpbGQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3VsZEhhdmVDaGlsZHJlbigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbn0iXX0=