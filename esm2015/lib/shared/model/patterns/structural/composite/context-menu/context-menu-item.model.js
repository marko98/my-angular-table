/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// model
import { Collection } from '../collection.model';
export class ContextMenuItem extends Collection {
    /**
     * @param {?} context
     * @param {?} f
     * @param {?=} imgSrc
     */
    constructor(context, f, imgSrc) {
        super();
        this.context = context;
        this.f = f;
        this.imgSrc = imgSrc;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    addChild(child) {
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    removeChild(child) {
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    getChildIndex(child) {
        return -1;
    }
    /**
     * @return {?}
     */
    shouldHaveChildren() {
        return false;
    }
}
if (false) {
    /** @type {?} */
    ContextMenuItem.prototype.context;
    /** @type {?} */
    ContextMenuItem.prototype.f;
    /** @type {?} */
    ContextMenuItem.prototype.imgSrc;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LWl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51LWl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFRakQsTUFBTSxPQUFPLGVBQWdCLFNBQVEsVUFBVTs7Ozs7O0lBRTNDLFlBQW1CLE9BQWUsRUFBUyxDQUFXLEVBQVMsTUFBZTtRQUMxRSxLQUFLLEVBQUUsQ0FBQztRQURPLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFBUyxNQUFDLEdBQUQsQ0FBQyxDQUFVO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUztJQUU5RSxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFpQjtRQUN0QixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFpQjtRQUN6QixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxLQUFpQjtRQUMzQixPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2QsQ0FBQzs7OztJQUVELGtCQUFrQjtRQUNkLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Q0FFSjs7O0lBcEJlLGtDQUFzQjs7SUFBRSw0QkFBa0I7O0lBQUUsaUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gJy4uL2NvbGxlY3Rpb24ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIENvbnRleHRNZW51SXRlbVZhbHVlIHtcclxuICAgIGNvbnRleHQ6IHN0cmluZyxcclxuICAgIGZ1bmN0aW9uOiBGdW5jdGlvbixcclxuICAgIGltZ1NyYz86IHN0cmluZ1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgQ29udGV4dE1lbnVJdGVtIGV4dGVuZHMgQ29sbGVjdGlvbiB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGNvbnRleHQ6IHN0cmluZywgcHVibGljIGY6IEZ1bmN0aW9uLCBwdWJsaWMgaW1nU3JjPzogc3RyaW5nKXtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZENoaWxkKGNoaWxkOiBDb2xsZWN0aW9uKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlQ2hpbGQoY2hpbGQ6IENvbGxlY3Rpb24pOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDaGlsZEluZGV4KGNoaWxkOiBDb2xsZWN0aW9uKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdWxkSGF2ZUNoaWxkcmVuKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIFxyXG59Il19