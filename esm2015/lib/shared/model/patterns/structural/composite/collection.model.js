/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
export class Collection {
    constructor() {
        this.children = [];
        this.parent = undefined;
        this.getChildren = (/**
         * @return {?}
         */
        () => {
            // vracamo kopiju
            // return this.children.slice();
            return this.children;
        });
        this.getParent = (/**
         * @return {?}
         */
        () => {
            return this.parent;
        });
        this.setParent = (/**
         * @param {?} parent
         * @return {?}
         */
        (parent) => {
            this.parent = parent;
            return true;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    Collection.prototype.children;
    /**
     * @type {?}
     * @protected
     */
    Collection.prototype.parent;
    /** @type {?} */
    Collection.prototype.getChildren;
    /** @type {?} */
    Collection.prototype.getParent;
    /** @type {?} */
    Collection.prototype.setParent;
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.addChild = function (child) { };
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.removeChild = function (child) { };
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.getChildIndex = function (child) { };
    /**
     * @abstract
     * @return {?}
     */
    Collection.prototype.shouldHaveChildren = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL3BhdHRlcm5zL3N0cnVjdHVyYWwvY29tcG9zaXRlL2NvbGxlY3Rpb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBLE1BQU0sT0FBZ0IsVUFBVTtJQUFoQztRQUNjLGFBQVEsR0FBaUIsRUFBRSxDQUFDO1FBQzVCLFdBQU0sR0FBZSxTQUFTLENBQUM7UUFVekMsZ0JBQVc7OztRQUFHLEdBQWlCLEVBQUU7WUFDN0IsaUJBQWlCO1lBQ2pCLGdDQUFnQztZQUNoQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDekIsQ0FBQyxFQUFBO1FBRUQsY0FBUzs7O1FBQUcsR0FBZSxFQUFFO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDLEVBQUE7UUFFRCxjQUFTOzs7O1FBQUcsQ0FBQyxNQUFrQixFQUFXLEVBQUU7WUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxFQUFBO0lBQ0wsQ0FBQztDQUFBOzs7Ozs7SUF6QkcsOEJBQXNDOzs7OztJQUN0Qyw0QkFBeUM7O0lBVXpDLGlDQUlDOztJQUVELCtCQUVDOztJQUVELCtCQUdDOzs7Ozs7SUFyQkQscURBQThDOzs7Ozs7SUFFOUMsd0RBQWlEOzs7Ozs7SUFFakQsMERBQWtEOzs7OztJQUVsRCwwREFBdUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgYWJzdHJhY3QgY2xhc3MgQ29sbGVjdGlvbiB7XHJcbiAgICBwcm90ZWN0ZWQgY2hpbGRyZW46IENvbGxlY3Rpb25bXSA9IFtdO1xyXG4gICAgcHJvdGVjdGVkIHBhcmVudDogQ29sbGVjdGlvbiA9IHVuZGVmaW5lZDtcclxuXHJcbiAgICBhYnN0cmFjdCBhZGRDaGlsZChjaGlsZDogQ29sbGVjdGlvbik6IGJvb2xlYW47XHJcblxyXG4gICAgYWJzdHJhY3QgcmVtb3ZlQ2hpbGQoY2hpbGQ6IENvbGxlY3Rpb24pOiBib29sZWFuO1xyXG5cclxuICAgIGFic3RyYWN0IGdldENoaWxkSW5kZXgoY2hpbGQ6IENvbGxlY3Rpb24pOiBudW1iZXI7XHJcblxyXG4gICAgYWJzdHJhY3Qgc2hvdWxkSGF2ZUNoaWxkcmVuKCk6IGJvb2xlYW47XHJcblxyXG4gICAgZ2V0Q2hpbGRyZW4gPSAoKTogQ29sbGVjdGlvbltdID0+IHtcclxuICAgICAgICAvLyB2cmFjYW1vIGtvcGlqdVxyXG4gICAgICAgIC8vIHJldHVybiB0aGlzLmNoaWxkcmVuLnNsaWNlKCk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2hpbGRyZW47XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UGFyZW50ID0gKCk6IENvbGxlY3Rpb24gPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBhcmVudDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRQYXJlbnQgPSAocGFyZW50OiBDb2xsZWN0aW9uKTogYm9vbGVhbiA9PiB7XHJcbiAgICAgICAgdGhpcy5wYXJlbnQgPSBwYXJlbnQ7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn0iXX0=