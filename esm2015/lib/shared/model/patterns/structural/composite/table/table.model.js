/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// model
import { Collection } from "../collection.model";
import { Row } from "./row/row.model";
import { RowItem, } from "./row/row-item.model";
import { ContextMenu, } from "../context-menu/context-menu.model";
export class Table extends Collection {
    // protected children: Collection[] = [];
    // protected parent: Collection = undefined;
    /**
     * @param {?=} tableValue
     */
    constructor(tableValue) {
        super();
        this.sortedASC = false;
        this.index = undefined;
        this.contextMenu = undefined;
        this.setContextMenuValue = (/**
         * @param {?} contextMenuValue
         * @return {?}
         */
        (contextMenuValue) => {
            if (contextMenuValue.contextMenuItems) {
                this.contextMenu = new ContextMenu(contextMenuValue);
                return true;
            }
            return false;
        });
        this.getContextMenu = (/**
         * @return {?}
         */
        () => {
            return this.contextMenu;
        });
        this.setHeader = (/**
         * @param {?} header
         * @return {?}
         */
        (header) => {
            this.header = header;
            this.header.setParent(this);
            this.header.getChildren().unshift(new RowItem("No."));
            return true;
        });
        this.setHeaderValue = (/**
         * @param {?} headerValue
         * @return {?}
         */
        (headerValue) => {
            /** @type {?} */
            let header = new Row();
            if (this.setHeader(header)) {
                headerValue.headerItems.forEach((/**
                 * @param {?} headerItemValue
                 * @return {?}
                 */
                (headerItemValue) => {
                    header.addChildValue(headerItemValue);
                }));
                return true;
            }
            return false;
        });
        this.getHeader = (/**
         * @return {?}
         */
        () => {
            return this.header;
        });
        this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children.push(child);
            child.setParent(this);
            child.getChildren().unshift(new RowItem(this.children.length + "."));
            return true;
        });
        this.addChildValue = (/**
         * @param {?} rowValue
         * @return {?}
         */
        (rowValue) => {
            /** @type {?} */
            let row = new Row();
            if (rowValue.color)
                row.color = rowValue.color;
            if (rowValue.dragPreviewTempRefSubject)
                row.dragPreviewTempRefSubject = rowValue.dragPreviewTempRefSubject;
            if (this.addChild(row)) {
                row.data = rowValue.data;
                rowValue.rowItems.forEach((/**
                 * @param {?} rowItemValue
                 * @return {?}
                 */
                (rowItemValue) => {
                    row.addChildValue(rowItemValue);
                }));
                return true;
            }
            return false;
        });
        this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children = this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => c !== child));
            child.setParent(undefined);
            return true;
        });
        this.removeChildren = (/**
         * @return {?}
         */
        () => {
            this.children.forEach((/**
             * @param {?} child
             * @return {?}
             */
            (child) => this.removeChild(child)));
            return true;
        });
        this.getFilteredChildren = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let rows = [];
            this.children.forEach((/**
             * @param {?} row
             * @return {?}
             */
            (row) => {
                if (row.show)
                    rows.push(row);
            }));
            return rows;
        });
        this.shouldHaveChildren = (/**
         * @return {?}
         */
        () => {
            return true;
        });
        this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return this.children.findIndex((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r === child));
        });
        this.onSort = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            if (this.index !== index) {
                this.index = index;
                this.sortedASC = false;
            }
            if (!this.sortedASC)
                return this.onASC(index);
            return this.onDESC(index);
        });
        this.onASC = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            this.children.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => {
                if (((/** @type {?} */ (a.getChildren()[index]))).context <
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return -1;
                if (((/** @type {?} */ (a.getChildren()[index]))).context >
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return 1;
                return 0;
            }));
            this.sortedASC = true;
        });
        this.onDESC = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            this.children.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => {
                if (((/** @type {?} */ (a.getChildren()[index]))).context >
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return -1;
                if (((/** @type {?} */ (a.getChildren()[index]))).context <
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return 1;
                return 0;
            }));
            this.sortedASC = false;
        });
        this.validate = (/**
         * @return {?}
         */
        () => {
            if (!this.header)
                throw Error("Table must have header");
            /** @type {?} */
            let valid = true;
            this.children.forEach((/**
             * @param {?} row
             * @return {?}
             */
            (row) => {
                if (row.getChildren().length !== this.header.getChildren().length)
                    valid = false;
            }));
            return valid;
        });
        if (tableValue) {
            if (!tableValue.header) {
                throw Error("Header must exists");
            }
            else {
                this.setHeaderValue(tableValue.header);
                if (tableValue.rows) {
                    tableValue.rows.forEach((/**
                     * @param {?} rowValue
                     * @return {?}
                     */
                    (rowValue) => {
                        this.addChildValue(rowValue);
                    }));
                }
                if (tableValue.contextMenu)
                    this.setContextMenuValue(tableValue.contextMenu);
            }
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    Table.prototype.header;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.sortedASC;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.index;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.contextMenu;
    /** @type {?} */
    Table.prototype.setContextMenuValue;
    /** @type {?} */
    Table.prototype.getContextMenu;
    /** @type {?} */
    Table.prototype.setHeader;
    /** @type {?} */
    Table.prototype.setHeaderValue;
    /** @type {?} */
    Table.prototype.getHeader;
    /** @type {?} */
    Table.prototype.addChild;
    /** @type {?} */
    Table.prototype.addChildValue;
    /** @type {?} */
    Table.prototype.removeChild;
    /** @type {?} */
    Table.prototype.removeChildren;
    /** @type {?} */
    Table.prototype.getFilteredChildren;
    /** @type {?} */
    Table.prototype.shouldHaveChildren;
    /** @type {?} */
    Table.prototype.getChildIndex;
    /** @type {?} */
    Table.prototype.onSort;
    /** @type {?} */
    Table.prototype.onASC;
    /** @type {?} */
    Table.prototype.onDESC;
    /** @type {?} */
    Table.prototype.validate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS90YWJsZS90YWJsZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUsR0FBRyxFQUF5QixNQUFNLGlCQUFpQixDQUFDO0FBQzdELE9BQU8sRUFDTCxPQUFPLEdBR1IsTUFBTSxzQkFBc0IsQ0FBQztBQUM5QixPQUFPLEVBQ0wsV0FBVyxHQUVaLE1BQU0sb0NBQW9DLENBQUM7QUFRNUMsTUFBTSxPQUFPLEtBQU0sU0FBUSxVQUFVOzs7Ozs7SUFRbkMsWUFBWSxVQUF1QjtRQUNqQyxLQUFLLEVBQUUsQ0FBQztRQVBGLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsVUFBSyxHQUFXLFNBQVMsQ0FBQztRQUMxQixnQkFBVyxHQUFnQixTQUFTLENBQUM7UUF5QjdDLHdCQUFtQjs7OztRQUFHLENBQUMsZ0JBQWtDLEVBQVcsRUFBRTtZQUNwRSxJQUFJLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO2dCQUNyQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBRXJELE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQztRQUVGLG1CQUFjOzs7UUFBRyxHQUFnQixFQUFFO1lBQ2pDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDLEVBQUM7UUFFRixjQUFTOzs7O1FBQUcsQ0FBQyxNQUFXLEVBQVcsRUFBRTtZQUNuQyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3RELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsbUJBQWM7Ozs7UUFBRyxDQUFDLFdBQXdCLEVBQVcsRUFBRTs7Z0JBQ2pELE1BQU0sR0FBRyxJQUFJLEdBQUcsRUFBRTtZQUN0QixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzFCLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTzs7OztnQkFBQyxDQUFDLGVBQW1DLEVBQUUsRUFBRTtvQkFDdEUsTUFBTSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDeEMsQ0FBQyxFQUFDLENBQUM7Z0JBRUgsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUVELE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDO1FBRUYsY0FBUzs7O1FBQUcsR0FBUSxFQUFFO1lBQ3BCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDLEVBQUM7UUFFRixhQUFROzs7O1FBQUcsQ0FBQyxLQUFVLEVBQVcsRUFBRTtZQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNyRSxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLGtCQUFhOzs7O1FBQUcsQ0FBQyxRQUFrQixFQUFXLEVBQUU7O2dCQUMxQyxHQUFHLEdBQUcsSUFBSSxHQUFHLEVBQUU7WUFFbkIsSUFBSSxRQUFRLENBQUMsS0FBSztnQkFBRSxHQUFHLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7WUFDL0MsSUFBSSxRQUFRLENBQUMseUJBQXlCO2dCQUNwQyxHQUFHLENBQUMseUJBQXlCLEdBQUcsUUFBUSxDQUFDLHlCQUF5QixDQUFDO1lBRXJFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDdEIsR0FBRyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUV6QixRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7Z0JBQUMsQ0FBQyxZQUEwQixFQUFFLEVBQUU7b0JBQ3ZELEdBQUcsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsRUFBQyxDQUFDO2dCQUVILE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQztRQUVGLGdCQUFXOzs7O1FBQUcsQ0FBQyxLQUFVLEVBQVcsRUFBRTtZQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxFQUFDLENBQUM7WUFDekQsS0FBSyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMzQixPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLG1CQUFjOzs7UUFBRyxHQUFZLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPOzs7O1lBQUMsQ0FBQyxLQUFVLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQztZQUMvRCxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLHdCQUFtQjs7O1FBQUcsR0FBVSxFQUFFOztnQkFDNUIsSUFBSSxHQUFHLEVBQUU7WUFDYixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLEdBQVEsRUFBRSxFQUFFO2dCQUNqQyxJQUFJLEdBQUcsQ0FBQyxJQUFJO29CQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDL0IsQ0FBQyxFQUFDLENBQUM7WUFDSCxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLHVCQUFrQjs7O1FBQUcsR0FBWSxFQUFFO1lBQ2pDLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsa0JBQWE7Ozs7UUFBRyxDQUFDLEtBQVUsRUFBVSxFQUFFO1lBQ3JDLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUM7UUFFRixXQUFNOzs7O1FBQUcsQ0FBQyxLQUFhLEVBQVEsRUFBRTtZQUMvQixJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssS0FBSyxFQUFFO2dCQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDeEI7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVM7Z0JBQUUsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUM7UUFFRixVQUFLOzs7O1FBQUcsQ0FBQyxLQUFhLEVBQVEsRUFBRTtZQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7Ozs7O1lBQUMsQ0FBQyxDQUFNLEVBQUUsQ0FBTSxFQUFFLEVBQUU7Z0JBQ3BDLElBQ0UsQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUEsQ0FBQyxDQUFDLE9BQU87b0JBQ3pDLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUV6QyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNaLElBQ0UsQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUEsQ0FBQyxDQUFDLE9BQU87b0JBQ3pDLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUV6QyxPQUFPLENBQUMsQ0FBQztnQkFDWCxPQUFPLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDeEIsQ0FBQyxFQUFDO1FBRUYsV0FBTTs7OztRQUFHLENBQUMsS0FBYSxFQUFRLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJOzs7OztZQUFDLENBQUMsQ0FBTSxFQUFFLENBQU0sRUFBRSxFQUFFO2dCQUNwQyxJQUNFLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUN6QyxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBQSxDQUFDLENBQUMsT0FBTztvQkFFekMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDWixJQUNFLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUN6QyxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBQSxDQUFDLENBQUMsT0FBTztvQkFFekMsT0FBTyxDQUFDLENBQUM7Z0JBQ1gsT0FBTyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLENBQUMsRUFBQztRQUVGLGFBQVE7OztRQUFHLEdBQVksRUFBRTtZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07Z0JBQUUsTUFBTSxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQzs7Z0JBRXBELEtBQUssR0FBRyxJQUFJO1lBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTzs7OztZQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQzVCLElBQUksR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU07b0JBQy9ELEtBQUssR0FBRyxLQUFLLENBQUM7WUFDbEIsQ0FBQyxFQUFDLENBQUM7WUFDSCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQztRQXBLQSxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO2dCQUN0QixNQUFNLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2FBQ25DO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUV2QyxJQUFJLFVBQVUsQ0FBQyxJQUFJLEVBQUU7b0JBQ25CLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTzs7OztvQkFBQyxDQUFDLFFBQWtCLEVBQUUsRUFBRTt3QkFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDL0IsQ0FBQyxFQUFDLENBQUM7aUJBQ0o7Z0JBRUQsSUFBSSxVQUFVLENBQUMsV0FBVztvQkFDeEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNwRDtTQUNGO0lBQ0gsQ0FBQztDQW9LRjs7Ozs7O0lBOUxDLHVCQUFvQjs7Ozs7SUFDcEIsMEJBQW1DOzs7OztJQUNuQyxzQkFBa0M7Ozs7O0lBQ2xDLDRCQUE2Qzs7SUF5QjdDLG9DQU9FOztJQUVGLCtCQUVFOztJQUVGLDBCQUtFOztJQUVGLCtCQVdFOztJQUVGLDBCQUVFOztJQUVGLHlCQUtFOztJQUVGLDhCQWtCRTs7SUFFRiw0QkFJRTs7SUFFRiwrQkFHRTs7SUFFRixvQ0FNRTs7SUFFRixtQ0FFRTs7SUFFRiw4QkFFRTs7SUFFRix1QkFRRTs7SUFFRixzQkFnQkU7O0lBRUYsdUJBZ0JFOztJQUVGLHlCQVNFIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gXCIuLi9jb2xsZWN0aW9uLm1vZGVsXCI7XHJcbmltcG9ydCB7IFJvdywgUm93VmFsdWUsIEhlYWRlclZhbHVlIH0gZnJvbSBcIi4vcm93L3Jvdy5tb2RlbFwiO1xyXG5pbXBvcnQge1xyXG4gIFJvd0l0ZW0sXHJcbiAgUm93SXRlbVZhbHVlLFxyXG4gIFJvd0l0ZW1IZWFkZXJWYWx1ZSxcclxufSBmcm9tIFwiLi9yb3cvcm93LWl0ZW0ubW9kZWxcIjtcclxuaW1wb3J0IHtcclxuICBDb250ZXh0TWVudSxcclxuICBDb250ZXh0TWVudVZhbHVlLFxyXG59IGZyb20gXCIuLi9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51Lm1vZGVsXCI7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgVGFibGVWYWx1ZSB7XHJcbiAgaGVhZGVyOiBIZWFkZXJWYWx1ZTtcclxuICByb3dzPzogUm93VmFsdWVbXTtcclxuICBjb250ZXh0TWVudT86IENvbnRleHRNZW51VmFsdWU7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBUYWJsZSBleHRlbmRzIENvbGxlY3Rpb24ge1xyXG4gIHByaXZhdGUgaGVhZGVyOiBSb3c7XHJcbiAgcHJpdmF0ZSBzb3J0ZWRBU0M6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIGluZGV4OiBudW1iZXIgPSB1bmRlZmluZWQ7XHJcbiAgcHJpdmF0ZSBjb250ZXh0TWVudTogQ29udGV4dE1lbnUgPSB1bmRlZmluZWQ7XHJcbiAgLy8gcHJvdGVjdGVkIGNoaWxkcmVuOiBDb2xsZWN0aW9uW10gPSBbXTtcclxuICAvLyBwcm90ZWN0ZWQgcGFyZW50OiBDb2xsZWN0aW9uID0gdW5kZWZpbmVkO1xyXG5cclxuICBjb25zdHJ1Y3Rvcih0YWJsZVZhbHVlPzogVGFibGVWYWx1ZSkge1xyXG4gICAgc3VwZXIoKTtcclxuXHJcbiAgICBpZiAodGFibGVWYWx1ZSkge1xyXG4gICAgICBpZiAoIXRhYmxlVmFsdWUuaGVhZGVyKSB7XHJcbiAgICAgICAgdGhyb3cgRXJyb3IoXCJIZWFkZXIgbXVzdCBleGlzdHNcIik7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZXRIZWFkZXJWYWx1ZSh0YWJsZVZhbHVlLmhlYWRlcik7XHJcblxyXG4gICAgICAgIGlmICh0YWJsZVZhbHVlLnJvd3MpIHtcclxuICAgICAgICAgIHRhYmxlVmFsdWUucm93cy5mb3JFYWNoKChyb3dWYWx1ZTogUm93VmFsdWUpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZFZhbHVlKHJvd1ZhbHVlKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRhYmxlVmFsdWUuY29udGV4dE1lbnUpXHJcbiAgICAgICAgICB0aGlzLnNldENvbnRleHRNZW51VmFsdWUodGFibGVWYWx1ZS5jb250ZXh0TWVudSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldENvbnRleHRNZW51VmFsdWUgPSAoY29udGV4dE1lbnVWYWx1ZTogQ29udGV4dE1lbnVWYWx1ZSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgaWYgKGNvbnRleHRNZW51VmFsdWUuY29udGV4dE1lbnVJdGVtcykge1xyXG4gICAgICB0aGlzLmNvbnRleHRNZW51ID0gbmV3IENvbnRleHRNZW51KGNvbnRleHRNZW51VmFsdWUpO1xyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfTtcclxuXHJcbiAgZ2V0Q29udGV4dE1lbnUgPSAoKTogQ29udGV4dE1lbnUgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dE1lbnU7XHJcbiAgfTtcclxuXHJcbiAgc2V0SGVhZGVyID0gKGhlYWRlcjogUm93KTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLmhlYWRlciA9IGhlYWRlcjtcclxuICAgIHRoaXMuaGVhZGVyLnNldFBhcmVudCh0aGlzKTtcclxuICAgIHRoaXMuaGVhZGVyLmdldENoaWxkcmVuKCkudW5zaGlmdChuZXcgUm93SXRlbShcIk5vLlwiKSk7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBzZXRIZWFkZXJWYWx1ZSA9IChoZWFkZXJWYWx1ZTogSGVhZGVyVmFsdWUpOiBib29sZWFuID0+IHtcclxuICAgIGxldCBoZWFkZXIgPSBuZXcgUm93KCk7XHJcbiAgICBpZiAodGhpcy5zZXRIZWFkZXIoaGVhZGVyKSkge1xyXG4gICAgICBoZWFkZXJWYWx1ZS5oZWFkZXJJdGVtcy5mb3JFYWNoKChoZWFkZXJJdGVtVmFsdWU6IFJvd0l0ZW1IZWFkZXJWYWx1ZSkgPT4ge1xyXG4gICAgICAgIGhlYWRlci5hZGRDaGlsZFZhbHVlKGhlYWRlckl0ZW1WYWx1ZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH07XHJcblxyXG4gIGdldEhlYWRlciA9ICgpOiBSb3cgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaGVhZGVyO1xyXG4gIH07XHJcblxyXG4gIGFkZENoaWxkID0gKGNoaWxkOiBSb3cpOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4ucHVzaChjaGlsZCk7XHJcbiAgICBjaGlsZC5zZXRQYXJlbnQodGhpcyk7XHJcbiAgICBjaGlsZC5nZXRDaGlsZHJlbigpLnVuc2hpZnQobmV3IFJvd0l0ZW0odGhpcy5jaGlsZHJlbi5sZW5ndGggKyBcIi5cIikpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgYWRkQ2hpbGRWYWx1ZSA9IChyb3dWYWx1ZTogUm93VmFsdWUpOiBib29sZWFuID0+IHtcclxuICAgIGxldCByb3cgPSBuZXcgUm93KCk7XHJcblxyXG4gICAgaWYgKHJvd1ZhbHVlLmNvbG9yKSByb3cuY29sb3IgPSByb3dWYWx1ZS5jb2xvcjtcclxuICAgIGlmIChyb3dWYWx1ZS5kcmFnUHJldmlld1RlbXBSZWZTdWJqZWN0KVxyXG4gICAgICByb3cuZHJhZ1ByZXZpZXdUZW1wUmVmU3ViamVjdCA9IHJvd1ZhbHVlLmRyYWdQcmV2aWV3VGVtcFJlZlN1YmplY3Q7XHJcblxyXG4gICAgaWYgKHRoaXMuYWRkQ2hpbGQocm93KSkge1xyXG4gICAgICByb3cuZGF0YSA9IHJvd1ZhbHVlLmRhdGE7XHJcblxyXG4gICAgICByb3dWYWx1ZS5yb3dJdGVtcy5mb3JFYWNoKChyb3dJdGVtVmFsdWU6IFJvd0l0ZW1WYWx1ZSkgPT4ge1xyXG4gICAgICAgIHJvdy5hZGRDaGlsZFZhbHVlKHJvd0l0ZW1WYWx1ZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH07XHJcblxyXG4gIHJlbW92ZUNoaWxkID0gKGNoaWxkOiBSb3cpOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuLmZpbHRlcigoYykgPT4gYyAhPT0gY2hpbGQpO1xyXG4gICAgY2hpbGQuc2V0UGFyZW50KHVuZGVmaW5lZCk7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICByZW1vdmVDaGlsZHJlbiA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaCgoY2hpbGQ6IFJvdykgPT4gdGhpcy5yZW1vdmVDaGlsZChjaGlsZCkpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgZ2V0RmlsdGVyZWRDaGlsZHJlbiA9ICgpOiBSb3dbXSA9PiB7XHJcbiAgICBsZXQgcm93cyA9IFtdO1xyXG4gICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKChyb3c6IFJvdykgPT4ge1xyXG4gICAgICBpZiAocm93LnNob3cpIHJvd3MucHVzaChyb3cpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gcm93cztcclxuICB9O1xyXG5cclxuICBzaG91bGRIYXZlQ2hpbGRyZW4gPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBnZXRDaGlsZEluZGV4ID0gKGNoaWxkOiBSb3cpOiBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hpbGRyZW4uZmluZEluZGV4KChyKSA9PiByID09PSBjaGlsZCk7XHJcbiAgfTtcclxuXHJcbiAgb25Tb3J0ID0gKGluZGV4OiBudW1iZXIpOiB2b2lkID0+IHtcclxuICAgIGlmICh0aGlzLmluZGV4ICE9PSBpbmRleCkge1xyXG4gICAgICB0aGlzLmluZGV4ID0gaW5kZXg7XHJcbiAgICAgIHRoaXMuc29ydGVkQVNDID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0aGlzLnNvcnRlZEFTQykgcmV0dXJuIHRoaXMub25BU0MoaW5kZXgpO1xyXG4gICAgcmV0dXJuIHRoaXMub25ERVNDKGluZGV4KTtcclxuICB9O1xyXG5cclxuICBvbkFTQyA9IChpbmRleDogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmNoaWxkcmVuLnNvcnQoKGE6IFJvdywgYjogUm93KSA9PiB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICAoPFJvd0l0ZW0+YS5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dCA8XHJcbiAgICAgICAgKDxSb3dJdGVtPmIuZ2V0Q2hpbGRyZW4oKVtpbmRleF0pLmNvbnRleHRcclxuICAgICAgKVxyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgICg8Um93SXRlbT5hLmdldENoaWxkcmVuKClbaW5kZXhdKS5jb250ZXh0ID5cclxuICAgICAgICAoPFJvd0l0ZW0+Yi5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dFxyXG4gICAgICApXHJcbiAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgIHJldHVybiAwO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5zb3J0ZWRBU0MgPSB0cnVlO1xyXG4gIH07XHJcblxyXG4gIG9uREVTQyA9IChpbmRleDogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmNoaWxkcmVuLnNvcnQoKGE6IFJvdywgYjogUm93KSA9PiB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICAoPFJvd0l0ZW0+YS5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dCA+XHJcbiAgICAgICAgKDxSb3dJdGVtPmIuZ2V0Q2hpbGRyZW4oKVtpbmRleF0pLmNvbnRleHRcclxuICAgICAgKVxyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgICg8Um93SXRlbT5hLmdldENoaWxkcmVuKClbaW5kZXhdKS5jb250ZXh0IDxcclxuICAgICAgICAoPFJvd0l0ZW0+Yi5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dFxyXG4gICAgICApXHJcbiAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgIHJldHVybiAwO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5zb3J0ZWRBU0MgPSBmYWxzZTtcclxuICB9O1xyXG5cclxuICB2YWxpZGF0ZSA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIGlmICghdGhpcy5oZWFkZXIpIHRocm93IEVycm9yKFwiVGFibGUgbXVzdCBoYXZlIGhlYWRlclwiKTtcclxuXHJcbiAgICBsZXQgdmFsaWQgPSB0cnVlO1xyXG4gICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKChyb3cpID0+IHtcclxuICAgICAgaWYgKHJvdy5nZXRDaGlsZHJlbigpLmxlbmd0aCAhPT0gdGhpcy5oZWFkZXIuZ2V0Q2hpbGRyZW4oKS5sZW5ndGgpXHJcbiAgICAgICAgdmFsaWQgPSBmYWxzZTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIHZhbGlkO1xyXG4gIH07XHJcblxyXG4gIC8vIGdldENoaWxkcmVuID0gKCk6IENvbGxlY3Rpb25bXSA9PiB7XHJcbiAgLy8gICAgIC8vIHZyYWNhbW8ga29waWp1XHJcbiAgLy8gICAgIC8vIHJldHVybiB0aGlzLmNoaWxkcmVuLnNsaWNlKCk7XHJcbiAgLy8gICAgIHJldHVybiB0aGlzLmNoaWxkcmVuO1xyXG4gIC8vIH1cclxuXHJcbiAgLy8gZ2V0UGFyZW50ID0gKCk6IENvbGxlY3Rpb24gPT4ge1xyXG4gIC8vICAgICByZXR1cm4gdGhpcy5wYXJlbnQ7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBzZXRQYXJlbnQgPSAocGFyZW50OiBDb2xsZWN0aW9uKTogYm9vbGVhbiA9PiB7XHJcbiAgLy8gICAgIHRoaXMucGFyZW50ID0gcGFyZW50O1xyXG4gIC8vICAgICByZXR1cm4gdHJ1ZTtcclxuICAvLyB9XHJcbn1cclxuIl19