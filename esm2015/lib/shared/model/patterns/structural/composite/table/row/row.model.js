/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// model
import { Collection } from "../../collection.model";
import { RowItem } from "./row-item.model";
export class Row extends Collection {
    /**
     * @param {?=} color
     */
    constructor(color) {
        super();
        // protected children: Collection[] = [];
        // protected parent: Collection = undefined;
        this.show = true;
        this.dragPreviewTempRefSubject = undefined;
        this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children.push(child);
            child.setParent(this);
            return true;
        });
        this.addChildValue = (/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            let child = new RowItem(value.context);
            if (value.imgSrc)
                child.setImgSrc(value.imgSrc);
            if (value.button) {
                child.setButton(true);
                child.setFunction(value.button.function);
            }
            if (value.rating) {
                child.setRating(true);
                if (value.rating.ratingImgSrc)
                    child.setRatingImgSrc(value.rating.ratingImgSrc);
            }
            return this.addChild(child);
        });
        this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children = this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => c !== child));
            child.setParent(undefined);
            return true;
        });
        this.shouldHaveChildren = (/**
         * @return {?}
         */
        () => {
            return true;
        });
        this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return this.children.findIndex((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r === child));
        });
        // getChildren = (): Collection[] => {
        //     // vracamo kopiju
        //     // return this.children.slice();
        //     return this.children;
        // }
        // getParent = (): Collection => {
        //     return this.parent;
        // }
        // setParent = (parent: Collection): boolean => {
        //     this.parent = parent;
        //     return true;
        // }
        this.clone = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let clone = new Row();
            this.children.forEach((/**
             * @param {?} c
             * @return {?}
             */
            (c) => {
                clone.addChild(c.clone());
            }));
            return clone;
        });
        this.prototype = (/**
         * @param {?} row
         * @return {?}
         */
        (row) => {
            this.children.forEach((/**
             * @param {?} c
             * @return {?}
             */
            (c) => {
                row.addChild(c.clone());
            }));
        });
        if (color)
            this.color = color;
    }
}
if (false) {
    /** @type {?} */
    Row.prototype.show;
    /** @type {?} */
    Row.prototype.data;
    /** @type {?} */
    Row.prototype.color;
    /** @type {?} */
    Row.prototype.dragPreviewTempRefSubject;
    /** @type {?} */
    Row.prototype.addChild;
    /** @type {?} */
    Row.prototype.addChildValue;
    /** @type {?} */
    Row.prototype.removeChild;
    /** @type {?} */
    Row.prototype.shouldHaveChildren;
    /** @type {?} */
    Row.prototype.getChildIndex;
    /** @type {?} */
    Row.prototype.clone;
    /** @type {?} */
    Row.prototype.prototype;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvcGF0dGVybnMvc3RydWN0dXJhbC9jb21wb3NpdGUvdGFibGUvcm93L3Jvdy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUlBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsT0FBTyxFQUFvQyxNQUFNLGtCQUFrQixDQUFDO0FBZTdFLE1BQU0sT0FBTyxHQUFJLFNBQVEsVUFBVTs7OztJQVFqQyxZQUFZLEtBQWM7UUFDeEIsS0FBSyxFQUFFLENBQUM7OztRQU5ILFNBQUksR0FBWSxJQUFJLENBQUM7UUFHckIsOEJBQXlCLEdBQWdDLFNBQVMsQ0FBQztRQVExRSxhQUFROzs7O1FBQUcsQ0FBQyxLQUFjLEVBQVcsRUFBRTtZQUNyQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsa0JBQWE7Ozs7UUFBRyxDQUFDLEtBQW1CLEVBQVcsRUFBRTs7Z0JBQzNDLEtBQUssR0FBRyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBRXRDLElBQUksS0FBSyxDQUFDLE1BQU07Z0JBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFaEQsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFO2dCQUNoQixLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0QixLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDMUM7WUFFRCxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ2hCLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRXRCLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZO29CQUMzQixLQUFLLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDcEQ7WUFFRCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUFDO1FBRUYsZ0JBQVc7Ozs7UUFBRyxDQUFDLEtBQWMsRUFBVyxFQUFFO1lBQ3hDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLEVBQUMsQ0FBQztZQUN6RCxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzNCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsdUJBQWtCOzs7UUFBRyxHQUFZLEVBQUU7WUFDakMsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUM7UUFFRixrQkFBYTs7OztRQUFHLENBQUMsS0FBYyxFQUFVLEVBQUU7WUFDekMsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVM7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBQyxDQUFDO1FBQ3JELENBQUMsRUFBQzs7Ozs7Ozs7Ozs7OztRQWlCRixVQUFLOzs7UUFBRyxHQUFRLEVBQUU7O2dCQUNaLEtBQUssR0FBRyxJQUFJLEdBQUcsRUFBRTtZQUNyQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLENBQVUsRUFBRSxFQUFFO2dCQUNuQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQzVCLENBQUMsRUFBQyxDQUFDO1lBQ0gsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUM7UUFFRixjQUFTOzs7O1FBQUcsQ0FBQyxHQUFRLEVBQVEsRUFBRTtZQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLENBQVUsRUFBRSxFQUFFO2dCQUNuQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQzFCLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDO1FBdEVBLElBQUksS0FBSztZQUFFLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ2hDLENBQUM7Q0FzRUY7OztJQS9FQyxtQkFBNEI7O0lBQzVCLG1CQUFtQjs7SUFDbkIsb0JBQXFCOztJQUNyQix3Q0FBMEU7O0lBUTFFLHVCQUlFOztJQUVGLDRCQWtCRTs7SUFFRiwwQkFJRTs7SUFFRixpQ0FFRTs7SUFFRiw0QkFFRTs7SUFpQkYsb0JBTUU7O0lBRUYsd0JBSUUiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpbnRlcmZhY2VcclxuaW1wb3J0IHsgUHJvdG90eXBlIH0gZnJvbSBcIi4uLy4uLy4uLy4uL2NyZWF0aW9uYWwvcHJvdG90eXBlL3Byb3RvdHlwZS5pbnRlcmZhY2VcIjtcclxuXHJcbi8vIG1vZGVsXHJcbmltcG9ydCB7IENvbGxlY3Rpb24gfSBmcm9tIFwiLi4vLi4vY29sbGVjdGlvbi5tb2RlbFwiO1xyXG5pbXBvcnQgeyBSb3dJdGVtLCBSb3dJdGVtVmFsdWUsIFJvd0l0ZW1IZWFkZXJWYWx1ZSB9IGZyb20gXCIuL3Jvdy1pdGVtLm1vZGVsXCI7XHJcbmltcG9ydCB7IFRlbXBsYXRlUmVmLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSBcInJ4anNcIjtcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBSb3dWYWx1ZSB7XHJcbiAgZGF0YTogYW55O1xyXG4gIGRyYWdQcmV2aWV3VGVtcFJlZlN1YmplY3Q/OiBCZWhhdmlvclN1YmplY3Q8RWxlbWVudFJlZj47XHJcbiAgY29sb3I/OiBzdHJpbmc7XHJcbiAgcm93SXRlbXM6IFJvd0l0ZW1WYWx1ZVtdO1xyXG59XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgSGVhZGVyVmFsdWUge1xyXG4gIGhlYWRlckl0ZW1zOiBSb3dJdGVtSGVhZGVyVmFsdWVbXTtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFJvdyBleHRlbmRzIENvbGxlY3Rpb24gaW1wbGVtZW50cyBQcm90b3R5cGU8Um93PiB7XHJcbiAgLy8gcHJvdGVjdGVkIGNoaWxkcmVuOiBDb2xsZWN0aW9uW10gPSBbXTtcclxuICAvLyBwcm90ZWN0ZWQgcGFyZW50OiBDb2xsZWN0aW9uID0gdW5kZWZpbmVkO1xyXG4gIHB1YmxpYyBzaG93OiBib29sZWFuID0gdHJ1ZTtcclxuICBwdWJsaWMgZGF0YTogYW55W107XHJcbiAgcHVibGljIGNvbG9yOiBzdHJpbmc7XHJcbiAgcHVibGljIGRyYWdQcmV2aWV3VGVtcFJlZlN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxFbGVtZW50UmVmPiA9IHVuZGVmaW5lZDtcclxuXHJcbiAgY29uc3RydWN0b3IoY29sb3I/OiBzdHJpbmcpIHtcclxuICAgIHN1cGVyKCk7XHJcblxyXG4gICAgaWYgKGNvbG9yKSB0aGlzLmNvbG9yID0gY29sb3I7XHJcbiAgfVxyXG5cclxuICBhZGRDaGlsZCA9IChjaGlsZDogUm93SXRlbSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgdGhpcy5jaGlsZHJlbi5wdXNoKGNoaWxkKTtcclxuICAgIGNoaWxkLnNldFBhcmVudCh0aGlzKTtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcblxyXG4gIGFkZENoaWxkVmFsdWUgPSAodmFsdWU6IFJvd0l0ZW1WYWx1ZSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgbGV0IGNoaWxkID0gbmV3IFJvd0l0ZW0odmFsdWUuY29udGV4dCk7XHJcblxyXG4gICAgaWYgKHZhbHVlLmltZ1NyYykgY2hpbGQuc2V0SW1nU3JjKHZhbHVlLmltZ1NyYyk7XHJcblxyXG4gICAgaWYgKHZhbHVlLmJ1dHRvbikge1xyXG4gICAgICBjaGlsZC5zZXRCdXR0b24odHJ1ZSk7XHJcbiAgICAgIGNoaWxkLnNldEZ1bmN0aW9uKHZhbHVlLmJ1dHRvbi5mdW5jdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHZhbHVlLnJhdGluZykge1xyXG4gICAgICBjaGlsZC5zZXRSYXRpbmcodHJ1ZSk7XHJcblxyXG4gICAgICBpZiAodmFsdWUucmF0aW5nLnJhdGluZ0ltZ1NyYylcclxuICAgICAgICBjaGlsZC5zZXRSYXRpbmdJbWdTcmModmFsdWUucmF0aW5nLnJhdGluZ0ltZ1NyYyk7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuYWRkQ2hpbGQoY2hpbGQpO1xyXG4gIH07XHJcblxyXG4gIHJlbW92ZUNoaWxkID0gKGNoaWxkOiBSb3dJdGVtKTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLmNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbi5maWx0ZXIoKGMpID0+IGMgIT09IGNoaWxkKTtcclxuICAgIGNoaWxkLnNldFBhcmVudCh1bmRlZmluZWQpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgc2hvdWxkSGF2ZUNoaWxkcmVuID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgZ2V0Q2hpbGRJbmRleCA9IChjaGlsZDogUm93SXRlbSk6IG51bWJlciA9PiB7XHJcbiAgICByZXR1cm4gdGhpcy5jaGlsZHJlbi5maW5kSW5kZXgoKHIpID0+IHIgPT09IGNoaWxkKTtcclxuICB9O1xyXG5cclxuICAvLyBnZXRDaGlsZHJlbiA9ICgpOiBDb2xsZWN0aW9uW10gPT4ge1xyXG4gIC8vICAgICAvLyB2cmFjYW1vIGtvcGlqdVxyXG4gIC8vICAgICAvLyByZXR1cm4gdGhpcy5jaGlsZHJlbi5zbGljZSgpO1xyXG4gIC8vICAgICByZXR1cm4gdGhpcy5jaGlsZHJlbjtcclxuICAvLyB9XHJcblxyXG4gIC8vIGdldFBhcmVudCA9ICgpOiBDb2xsZWN0aW9uID0+IHtcclxuICAvLyAgICAgcmV0dXJuIHRoaXMucGFyZW50O1xyXG4gIC8vIH1cclxuXHJcbiAgLy8gc2V0UGFyZW50ID0gKHBhcmVudDogQ29sbGVjdGlvbik6IGJvb2xlYW4gPT4ge1xyXG4gIC8vICAgICB0aGlzLnBhcmVudCA9IHBhcmVudDtcclxuICAvLyAgICAgcmV0dXJuIHRydWU7XHJcbiAgLy8gfVxyXG5cclxuICBjbG9uZSA9ICgpOiBSb3cgPT4ge1xyXG4gICAgbGV0IGNsb25lID0gbmV3IFJvdygpO1xyXG4gICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKChjOiBSb3dJdGVtKSA9PiB7XHJcbiAgICAgIGNsb25lLmFkZENoaWxkKGMuY2xvbmUoKSk7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBjbG9uZTtcclxuICB9O1xyXG5cclxuICBwcm90b3R5cGUgPSAocm93OiBSb3cpOiB2b2lkID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaCgoYzogUm93SXRlbSkgPT4ge1xyXG4gICAgICByb3cuYWRkQ2hpbGQoYy5jbG9uZSgpKTtcclxuICAgIH0pO1xyXG4gIH07XHJcbn1cclxuIl19