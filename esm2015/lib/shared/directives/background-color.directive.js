/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Input, } from "@angular/core";
export class LibElColorDirective {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.libElColorValue.currentValue)
            ((/** @type {?} */ (this.elRef.nativeElement))).style.backgroundColor =
                changes.libElColorValue.currentValue;
    }
}
LibElColorDirective.decorators = [
    { type: Directive, args: [{
                selector: "[libElColor]",
            },] }
];
/** @nocollapse */
LibElColorDirective.ctorParameters = () => [
    { type: ElementRef }
];
LibElColorDirective.propDecorators = {
    libElColorValue: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    LibElColorDirective.prototype.libElColorValue;
    /**
     * @type {?}
     * @private
     */
    LibElColorDirective.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFja2dyb3VuZC1jb2xvci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmVzL2JhY2tncm91bmQtY29sb3IuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEdBR04sTUFBTSxlQUFlLENBQUM7QUFLdkIsTUFBTSxPQUFPLG1CQUFtQjs7OztJQUc5QixZQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQUcsQ0FBQzs7Ozs7SUFFekMsV0FBVyxDQUFDLE9BQXNCO1FBQ2hDLElBQUksT0FBTyxDQUFDLGVBQWUsQ0FBQyxZQUFZO1lBQ3RDLENBQUMsbUJBQWEsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlO2dCQUMzRCxPQUFPLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQztJQUMzQyxDQUFDOzs7WUFaRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7YUFDekI7Ozs7WUFSQyxVQUFVOzs7OEJBVVQsS0FBSzs7OztJQUFOLDhDQUFpQzs7Ozs7SUFFckIsb0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBEaXJlY3RpdmUsXHJcbiAgRWxlbWVudFJlZixcclxuICBJbnB1dCxcclxuICBTaW1wbGVDaGFuZ2VzLFxyXG4gIE9uQ2hhbmdlcyxcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6IFwiW2xpYkVsQ29sb3JdXCIsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMaWJFbENvbG9yRGlyZWN0aXZlIGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICBASW5wdXQoKSBsaWJFbENvbG9yVmFsdWU6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge31cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMubGliRWxDb2xvclZhbHVlLmN1cnJlbnRWYWx1ZSlcclxuICAgICAgKDxIVE1MRWxlbWVudD50aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQpLnN0eWxlLmJhY2tncm91bmRDb2xvciA9XHJcbiAgICAgICAgY2hhbmdlcy5saWJFbENvbG9yVmFsdWUuY3VycmVudFZhbHVlO1xyXG4gIH1cclxufVxyXG4iXX0=