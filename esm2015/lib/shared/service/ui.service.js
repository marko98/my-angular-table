/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
export class UiService {
    /**
     * @param {?} matSnackBar
     */
    constructor(matSnackBar) {
        this.matSnackBar = matSnackBar;
        this.onShowSnackBar = (/**
         * @param {?} message
         * @param {?} action
         * @param {?} duration
         * @return {?}
         */
        (message, action, duration) => {
            this.matSnackBar.open(message, action, { duration: duration });
        });
    }
}
UiService.decorators = [
    { type: Injectable, args: [{ providedIn: "root" },] }
];
/** @nocollapse */
UiService.ctorParameters = () => [
    { type: MatSnackBar }
];
/** @nocollapse */ UiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UiService_Factory() { return new UiService(i0.ɵɵinject(i1.MatSnackBar)); }, token: UiService, providedIn: "root" });
if (false) {
    /** @type {?} */
    UiService.prototype.onShowSnackBar;
    /**
     * @type {?}
     * @private
     */
    UiService.prototype.matSnackBar;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2UvdWkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzFELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQUczQyxNQUFNLE9BQU8sU0FBUzs7OztJQUVsQixZQUFvQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUU1QyxtQkFBYzs7Ozs7O1FBQUcsQ0FBQyxPQUFlLEVBQUUsTUFBVyxFQUFFLFFBQWdCLEVBQVEsRUFBRTtZQUN0RSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQUM7UUFDakUsQ0FBQyxFQUFBO0lBSjZDLENBQUM7OztZQUhsRCxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7O1lBSHZCLFdBQVc7Ozs7O0lBUWhCLG1DQUVDOzs7OztJQUpXLGdDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1hdFNuYWNrQmFyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc25hY2stYmFyJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46IFwicm9vdFwifSlcclxuZXhwb3J0IGNsYXNzIFVpU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtYXRTbmFja0JhcjogTWF0U25hY2tCYXIpe31cclxuXHJcbiAgICBvblNob3dTbmFja0JhciA9IChtZXNzYWdlOiBzdHJpbmcsIGFjdGlvbjogYW55LCBkdXJhdGlvbjogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICAgICAgdGhpcy5tYXRTbmFja0Jhci5vcGVuKG1lc3NhZ2UsIGFjdGlvbiwge2R1cmF0aW9uOiBkdXJhdGlvbn0pO1xyXG4gICAgfVxyXG59Il19