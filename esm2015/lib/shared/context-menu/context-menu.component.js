/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild, ChangeDetectorRef, HostListener } from '@angular/core';
export class ContextMenuComponent {
    /**
     * @param {?} changeDetectorRef
     */
    constructor(changeDetectorRef) {
        this.changeDetectorRef = changeDetectorRef;
        this.contextMenuData = {
            state: false,
            menuPositionX: undefined,
            menuPositionY: undefined,
            menuPosition: undefined,
            menuWidth: undefined,
            menuHeight: undefined,
            windowWidth: undefined,
            windowHeight: undefined
        };
        this.setModel = (/**
         * @param {?} model
         * @return {?}
         */
        (model) => {
            this.contextMenu = model;
            return true;
        });
        this.getContextMenuItems = (/**
         * @return {?}
         */
        () => {
            return (/** @type {?} */ (this.contextMenu.getChildren()));
        });
        this.showContextMenu = (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            // console.log(event);
            // console.log(row);
            this.contextMenuData.state = true;
            event.stopPropagation();
            this.positionMenu(event);
        });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    positionMenu(event) {
        this.contextMenuData.menuPosition = this.getPosition(event);
        this.contextMenuData.menuPositionX = this.contextMenuData.menuPosition.x;
        this.contextMenuData.menuPositionY = this.contextMenuData.menuPosition.y;
        this.changeDetectorRef.detectChanges();
        this.contextMenuData.menuWidth = this.contextMenuView.nativeElement.offsetWidth;
        this.contextMenuData.menuHeight = this.contextMenuView.nativeElement.offsetHeight;
        this.contextMenuData.windowWidth = window.innerWidth;
        this.contextMenuData.windowHeight = window.innerHeight;
        if ((this.contextMenuData.windowWidth - this.contextMenuData.menuPositionX) < this.contextMenuData.menuWidth) {
            this.contextMenuData.menuPositionX = this.contextMenuData.windowWidth - this.contextMenuData.menuWidth + "px";
        }
        else {
            this.contextMenuData.menuPositionX = this.contextMenuData.menuPositionX + "px";
        }
        if ((this.contextMenuData.windowHeight - this.contextMenuData.menuPositionY) < this.contextMenuData.menuHeight) {
            this.contextMenuData.menuPositionY = this.contextMenuData.windowHeight - this.contextMenuData.menuHeight + "px";
        }
        else {
            this.contextMenuData.menuPositionY = this.contextMenuData.menuPositionY + "px";
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    getPosition(event) {
        /** @type {?} */
        var posx = 0;
        /** @type {?} */
        var posy = 0;
        if (event.pageX || event.pageY) {
            posx = event.pageX;
            posy = event.pageY;
        }
        else if (event.clientX || event.clientY) {
            posx = event.clientX + document.body.scrollLeft +
                document.documentElement.scrollLeft;
            posy = event.clientY + document.body.scrollTop +
                document.documentElement.scrollTop;
        }
        return { x: posx, y: posy };
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentClick(event) {
        this.contextMenuData.state = false;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentRClick(event) {
        this.contextMenuData.state = false;
    }
}
ContextMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-my-angular-table-context-menu',
                template: "<section\r\n    class=\"no-padding context-active\"\r\n    *ngIf=\"this.contextMenuData.state\"\r\n    [ngStyle]=\"{'left': this.contextMenuData.menuPositionX, 'top': this.contextMenuData.menuPositionY}\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\"\r\n    fxFlex.xs=\"100px\"\r\n    fxFlex.gt-xs=\"150px\"\r\n    #contextMenu>\r\n        <section\r\n            class=\"context-menu-item\"\r\n            *ngFor=\"let contextMenuItem of this.getContextMenuItems()\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"10px\"\r\n            (click)=\"contextMenuItem.f(this.data)\">\r\n                <img \r\n                    *ngIf=\"contextMenuItem.imgSrc\"\r\n                    [src]=\"contextMenuItem.imgSrc\" \r\n                    alt=\"\">\r\n                <p\r\n                    fxFlex></p>\r\n                <p>\r\n                        {{ contextMenuItem.context }}\r\n                </p>\r\n        </section>\r\n</section>",
                styles: [".no-padding{padding:0}.context-active{text-align:start;display:block;position:absolute;background-color:#fff;box-shadow:0 1px 5px rgba(0,0,0,.2),0 1px rgba(0,0,0,.14),0 1px rgba(0,0,0,.12)}section.context-menu-item{width:100%;box-shadow:0 1px #d3d3d3;cursor:pointer}section.context-menu-item:hover{background-color:#ebebeb}img{width:15%;margin:0 0 0 10px}p{margin:10px 10px 10px 0}"]
            }] }
];
/** @nocollapse */
ContextMenuComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
ContextMenuComponent.propDecorators = {
    contextMenuView: [{ type: ViewChild, args: ['contextMenu', { static: false },] }],
    documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }],
    documentRClick: [{ type: HostListener, args: ["document:contextmenu", ["$event"],] }]
};
if (false) {
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenuView;
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenu;
    /** @type {?} */
    ContextMenuComponent.prototype.data;
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenuData;
    /** @type {?} */
    ContextMenuComponent.prototype.setModel;
    /** @type {?} */
    ContextMenuComponent.prototype.getContextMenuItems;
    /** @type {?} */
    ContextMenuComponent.prototype.showContextMenu;
    /**
     * @type {?}
     * @private
     */
    ContextMenuComponent.prototype.changeDetectorRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbnRleHQtbWVudS9jb250ZXh0LW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFXdEYsTUFBTSxPQUFPLG9CQUFvQjs7OztJQWdCN0IsWUFBb0IsaUJBQW9DO1FBQXBDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7UUFYakQsb0JBQWUsR0FBRztZQUNyQixLQUFLLEVBQUUsS0FBSztZQUNaLGFBQWEsRUFBRSxTQUFTO1lBQ3hCLGFBQWEsRUFBRSxTQUFTO1lBQ3hCLFlBQVksRUFBRSxTQUFTO1lBQ3ZCLFNBQVMsRUFBRSxTQUFTO1lBQ3BCLFVBQVUsRUFBRSxTQUFTO1lBQ3JCLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFlBQVksRUFBRSxTQUFTO1NBQzFCLENBQUM7UUFJRixhQUFROzs7O1FBQUcsQ0FBQyxLQUFrQixFQUFXLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFFekIsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxFQUFBO1FBRUQsd0JBQW1COzs7UUFBRyxHQUFzQixFQUFFO1lBQzFDLE9BQU8sbUJBQW1CLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEVBQUEsQ0FBQztRQUM3RCxDQUFDLEVBQUE7UUFFRCxvQkFBZTs7OztRQUFHLENBQUMsS0FBaUIsRUFBUSxFQUFFO1lBQzFDLHNCQUFzQjtZQUN0QixvQkFBb0I7WUFFcEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBRWxDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN4QixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLENBQUMsRUFBQTtJQXBCeUQsQ0FBQzs7Ozs7SUFzQjNELFlBQVksQ0FBQyxLQUFpQjtRQUMxQixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUNoRixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDbEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUNyRCxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZELElBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFHO1lBQzVHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztTQUNqSDthQUFNO1lBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQ2xGO1FBQ0QsSUFBSyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUc7WUFDOUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQ25IO2FBQU07WUFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7U0FDbEY7SUFDTCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFpQjs7WUFDckIsSUFBSSxHQUFHLENBQUM7O1lBQ1IsSUFBSSxHQUFHLENBQUM7UUFDWixJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUM1QixJQUFJLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNuQixJQUFJLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztTQUN0QjthQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ3ZDLElBQUksR0FBRyxLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVTtnQkFDL0MsUUFBUSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUM7WUFDcEMsSUFBSSxHQUFHLEtBQUssQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTO2dCQUM5QyxRQUFRLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQztTQUN0QztRQUNELE9BQU8sRUFBQyxDQUFDLEVBQUUsSUFBSSxFQUFDLENBQUMsRUFBRSxJQUFJLEVBQUMsQ0FBQTtJQUM1QixDQUFDOzs7OztJQUVrRCxhQUFhLENBQUMsS0FBaUI7UUFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRXdELGNBQWMsQ0FBQyxLQUFpQjtRQUNyRixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkMsQ0FBQzs7O1lBckZKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsbUNBQW1DO2dCQUM3Qyw2K0JBQTRDOzthQUUvQzs7OztZQVY4QixpQkFBaUI7Ozs4QkFZM0MsU0FBUyxTQUFDLGFBQWEsRUFBRSxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUM7NEJBeUV4QyxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUM7NkJBSXpDLFlBQVksU0FBQyxzQkFBc0IsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7OztJQTdFaEQsK0NBQWdFOztJQUNoRSwyQ0FBZ0M7O0lBQ2hDLG9DQUFtQjs7SUFFbkIsK0NBU0U7O0lBSUYsd0NBSUM7O0lBRUQsbURBRUM7O0lBRUQsK0NBUUM7Ozs7O0lBcEJXLGlEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBDaGFuZ2VEZXRlY3RvclJlZiwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vLyBtb2RlbFxyXG5pbXBvcnQgeyBDb250ZXh0TWVudSwgQ29udGV4dE1lbnVWYWx1ZSB9IGZyb20gJy4uL21vZGVsL3BhdHRlcm5zL3N0cnVjdHVyYWwvY29tcG9zaXRlL2NvbnRleHQtbWVudS9jb250ZXh0LW1lbnUubW9kZWwnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudUl0ZW0gfSBmcm9tICcuLi9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51LWl0ZW0ubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2xpYi1teS1hbmd1bGFyLXRhYmxlLWNvbnRleHQtbWVudScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGV4dC1tZW51LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NvbnRleHQtbWVudS5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRleHRNZW51Q29tcG9uZW50IHtcclxuICAgIEBWaWV3Q2hpbGQoJ2NvbnRleHRNZW51Jywge3N0YXRpYzogZmFsc2V9KSBjb250ZXh0TWVudVZpZXc6IGFueTtcclxuICAgIHB1YmxpYyBjb250ZXh0TWVudTogQ29udGV4dE1lbnU7XHJcbiAgICBwdWJsaWMgZGF0YTogYW55W107XHJcblxyXG4gICAgcHVibGljIGNvbnRleHRNZW51RGF0YSA9IHtcclxuICAgICAgICBzdGF0ZTogZmFsc2UsXHJcbiAgICAgICAgbWVudVBvc2l0aW9uWDogdW5kZWZpbmVkLFxyXG4gICAgICAgIG1lbnVQb3NpdGlvblk6IHVuZGVmaW5lZCxcclxuICAgICAgICBtZW51UG9zaXRpb246IHVuZGVmaW5lZCxcclxuICAgICAgICBtZW51V2lkdGg6IHVuZGVmaW5lZCxcclxuICAgICAgICBtZW51SGVpZ2h0OiB1bmRlZmluZWQsXHJcbiAgICAgICAgd2luZG93V2lkdGg6IHVuZGVmaW5lZCxcclxuICAgICAgICB3aW5kb3dIZWlnaHQ6IHVuZGVmaW5lZFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZil7fVxyXG5cclxuICAgIHNldE1vZGVsID0gKG1vZGVsOiBDb250ZXh0TWVudSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnUgPSBtb2RlbDtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29udGV4dE1lbnVJdGVtcyA9ICgpOiBDb250ZXh0TWVudUl0ZW1bXSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIDxDb250ZXh0TWVudUl0ZW1bXT50aGlzLmNvbnRleHRNZW51LmdldENoaWxkcmVuKCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0NvbnRleHRNZW51ID0gKGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCA9PiB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coZXZlbnQpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHJvdyk7XHJcblxyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLnN0YXRlID0gdHJ1ZTtcclxuICAgIFxyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIHRoaXMucG9zaXRpb25NZW51KGV2ZW50KTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgcG9zaXRpb25NZW51KGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uID0gdGhpcy5nZXRQb3NpdGlvbihldmVudCk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWCA9IHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvbi54O1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblkgPSB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51UG9zaXRpb24ueTtcclxuICAgICAgICB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51V2lkdGggPSB0aGlzLmNvbnRleHRNZW51Vmlldy5uYXRpdmVFbGVtZW50Lm9mZnNldFdpZHRoO1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVIZWlnaHQgPSB0aGlzLmNvbnRleHRNZW51Vmlldy5uYXRpdmVFbGVtZW50Lm9mZnNldEhlaWdodDtcclxuICAgICAgICB0aGlzLmNvbnRleHRNZW51RGF0YS53aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLndpbmRvd0hlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodDtcclxuICAgICAgICBpZiAoICh0aGlzLmNvbnRleHRNZW51RGF0YS53aW5kb3dXaWR0aCAtIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblgpIDwgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVdpZHRoICkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51UG9zaXRpb25YID0gdGhpcy5jb250ZXh0TWVudURhdGEud2luZG93V2lkdGggLSB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51V2lkdGggKyBcInB4XCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWCA9IHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblggKyBcInB4XCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggKHRoaXMuY29udGV4dE1lbnVEYXRhLndpbmRvd0hlaWdodCAtIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblkpIDwgdGhpcy5jb250ZXh0TWVudURhdGEubWVudUhlaWdodCApIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWSA9IHRoaXMuY29udGV4dE1lbnVEYXRhLndpbmRvd0hlaWdodCAtIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVIZWlnaHQgKyBcInB4XCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWSA9IHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblkgKyBcInB4XCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBnZXRQb3NpdGlvbihldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIHZhciBwb3N4ID0gMDtcclxuICAgICAgICB2YXIgcG9zeSA9IDA7XHJcbiAgICAgICAgaWYgKGV2ZW50LnBhZ2VYIHx8IGV2ZW50LnBhZ2VZKSB7XHJcbiAgICAgICAgICAgIHBvc3ggPSBldmVudC5wYWdlWDtcclxuICAgICAgICAgICAgcG9zeSA9IGV2ZW50LnBhZ2VZO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQuY2xpZW50WCB8fCBldmVudC5jbGllbnRZKSB7XHJcbiAgICAgICAgICAgIHBvc3ggPSBldmVudC5jbGllbnRYICsgZG9jdW1lbnQuYm9keS5zY3JvbGxMZWZ0ICtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnQ7XHJcbiAgICAgICAgICAgIHBvc3kgPSBldmVudC5jbGllbnRZICsgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgK1xyXG4gICAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge3g6IHBvc3gseTogcG9zeX1cclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKFwiZG9jdW1lbnQ6Y2xpY2tcIiwgW1wiJGV2ZW50XCJdKSBwdWJsaWMgZG9jdW1lbnRDbGljayhldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLnN0YXRlID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIEBIb3N0TGlzdGVuZXIoXCJkb2N1bWVudDpjb250ZXh0bWVudVwiLCBbXCIkZXZlbnRcIl0pIHB1YmxpYyBkb2N1bWVudFJDbGljayhldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLnN0YXRlID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG59Il19