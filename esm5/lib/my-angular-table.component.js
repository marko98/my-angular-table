/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ViewChild, HostListener, Output, EventEmitter, ElementRef, } from "@angular/core";
// model
import { Table } from "./shared/model/patterns/structural/composite/table/table.model";
import { UiService } from "./shared/service/ui.service";
import { ContextMenuComponent } from "./shared/context-menu/context-menu.component";
import { CdkDropList, } from "@angular/cdk/drag-drop";
var MyAngularTableComponent = /** @class */ (function () {
    function MyAngularTableComponent(uiService) {
        var _this = this;
        this.uiService = uiService;
        this.tempRef = undefined;
        this.criteria = "";
        this.selectedRows = [];
        this.isValid = false;
        this.perPage = 2;
        this.page = 0;
        this.onListInit = new EventEmitter();
        this.showContextMenu = false;
        this.onSort = (/**
         * @param {?} index
         * @return {?}
         */
        function (index) {
            // console.log(index);
            _this.table.onSort(index);
            // console.log(this.table.getChildren());
        });
        // getRows = (): Row[] => {
        //   return <Row[]>this.table.getChildren();
        // }
        this.onCriteriaChange = (/**
         * @return {?}
         */
        function () {
            // console.log(this.criteria);
            _this.table.getChildren().forEach((/**
             * @param {?} row
             * @return {?}
             */
            function (row) {
                /** @type {?} */
                var value = "";
                row.getChildren().forEach((/**
                 * @param {?} rowItem
                 * @return {?}
                 */
                function (rowItem) {
                    value += rowItem.context.toString().toLowerCase();
                }));
                if (value.includes(_this.criteria))
                    row.show = true;
                else
                    row.show = false;
            }));
        });
        this.onShowSnackBar = (/**
         * @param {?} message
         * @return {?}
         */
        function (message) {
            _this.uiService.onShowSnackBar(message, null, 1500);
        });
        this.onSelect = (/**
         * @param {?} event
         * @param {?} row
         * @return {?}
         */
        function (event, row) {
            if (event.ctrlKey) {
                if (_this.selectedRows.find((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return r === row; }))) {
                    _this.selectedRows = _this.selectedRows.filter((/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) { return r !== row; }));
                }
                else {
                    _this.selectedRows.push(row);
                }
            }
            else if (_this.selectedRows.length > 0) {
                _this.selectedRows = [];
            }
        });
        this.isSelected = (/**
         * @param {?} row
         * @return {?}
         */
        function (row) {
            if (_this.selectedRows.find((/**
             * @param {?} r
             * @return {?}
             */
            function (r) { return r === row; }))) {
                return true;
            }
            return false;
        });
        this.onContextMenu = (/**
         * @param {?} event
         * @param {?} row
         * @return {?}
         */
        function (event, row) {
            // console.log(event);
            // console.log(row);
            if (_this.table.getContextMenu()) {
                _this.showContextMenu = true;
                if (_this.selectedRows.length > 0)
                    _this.contextMenuComponent.data = _this.selectedRows.map((/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) { return r.data; }));
                else
                    _this.contextMenuComponent.data = [row.data];
                _this.contextMenuComponent.showContextMenu(event);
            }
        });
        this.next = (/**
         * @return {?}
         */
        function () {
            if (_this.page + 1 <
                Math.ceil(_this.table.getChildren().length / _this.perPage))
                _this.page++;
        });
        this.previous = (/**
         * @return {?}
         */
        function () {
            if (_this.page > 0)
                _this.page--;
        });
    }
    Object.defineProperty(MyAngularTableComponent.prototype, "list", {
        set: /**
         * @param {?} list
         * @return {?}
         */
        function (list) {
            list.data = this.table;
            // console.log(list);
            this.onListInit.emit(list);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyAngularTableComponent.prototype, "contextMenu", {
        set: /**
         * @param {?} contextMenuComponent
         * @return {?}
         */
        function (contextMenuComponent) {
            this.contextMenuComponent = contextMenuComponent;
            this.contextMenuComponent.setModel(this.table.getContextMenu());
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} event
     * @return {?}
     */
    MyAngularTableComponent.prototype.documentClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.showContextMenu = false;
        if (!event.ctrlKey)
            this.selectedRows = [];
    };
    /**
     * @param {?} event
     * @return {?}
     */
    MyAngularTableComponent.prototype.documentRClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.showContextMenu = false;
        if (!event.ctrlKey)
            this.selectedRows = [];
    };
    /**
     * @return {?}
     */
    MyAngularTableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.isValid = this.table.validate();
        if (!this.isValid)
            throw new Error("Number of header items must be the same as the number in each row");
        // console.log("MyAngularTableComponent init");
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    MyAngularTableComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        // console.log(changes);
    };
    /**
     * @return {?}
     */
    MyAngularTableComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        // console.log("MyAngularTableComponent destroyed");
    };
    MyAngularTableComponent.decorators = [
        { type: Component, args: [{
                    selector: "lib-my-angular-table",
                    template: "<style>\r\n\r\n    .example-list {\r\n        /* width: 500px; */\r\n        /* max-width: 100%; */\r\n        /* border: solid 1px #ccc; */\r\n        /* min-height: 60px; */\r\n        display: block;\r\n        /* background: white; */\r\n        /* border-radius: 4px; */\r\n        /* overflow: hidden; */\r\n    }\r\n\r\n    .example-box {\r\n        /* padding: 20px 10px; */\r\n        /* border-bottom: solid 1px #ccc; */\r\n        /* color: rgba(0, 0, 0, 0.87); */\r\n        /* display: flex; */\r\n        /* flex-direction: row; */\r\n        /* align-items: center; */\r\n        /* justify-content: space-between; */\r\n        /* box-sizing: border-box; */\r\n        cursor: move;\r\n        /* background: white; */\r\n        /* font-size: 14px; */\r\n    }\r\n\r\n    .cdk-drag-preview {\r\n        box-sizing: border-box;\r\n        border-radius: 4px;\r\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\r\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\r\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\r\n    }\r\n\r\n    .cdk-drag-placeholder {\r\n        opacity: 0;\r\n    }\r\n\r\n    .cdk-drag-animating {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n    .example-box:last-child {\r\n        border: none;\r\n    }\r\n\r\n    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n</style>\r\n\r\n<section\r\n    *ngIf=\"this.isValid\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\">\r\n        <section\r\n            class=\"no-padding\"\r\n            fxLayout=\"column\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"25px\">\r\n\r\n                <!-- FILTER -->\r\n                <mat-form-field>\r\n                    <mat-label>filter...</mat-label>\r\n                    <input \r\n                        matInput\r\n                        type=\"text\"\r\n                        [(ngModel)]=\"criteria\"\r\n                        (ngModelChange)=\"onCriteriaChange()\">\r\n                </mat-form-field>\r\n\r\n                \r\n                <section \r\n                    class=\"no-padding example-list\"\r\n                    cdkDropList\r\n                    #list=\"cdkDropList\">\r\n                    <!-- HEADER -->\r\n                    <section\r\n                        class=\"header\"\r\n                        fxLayout\r\n                        fxLayoutAlign=\"center center\"\r\n                        fxLayoutGap=\"10px\">\r\n                            <p\r\n                                class=\"header-item\"\r\n                                *ngFor=\"let headerItem of table.getHeader().getChildren()\"\r\n                                fxFlex\r\n                                (click)=\"onSort(headerItem.getParent().getChildIndex(headerItem))\">\r\n                                    {{ headerItem.context }}\r\n                            </p>\r\n                    </section>\r\n\r\n                    <section\r\n                        class=\"no-padding example-box\"\r\n                        cdkDrag\r\n                        *ngFor=\"let row of this.table.getFilteredChildren() | pagination: {page: this.page, perPage: this.perPage}\"\r\n                        libElColor\r\n                        [libElColorValue]=\"this.row.color\"\r\n                        [ngClass]=\"{'selected': this.isSelected(row)}\"\r\n                        (click)=\"onSelect($event, row)\"\r\n                        (contextmenu)=\"onContextMenu($event, row); false\">\r\n                            <section\r\n                                class=\"row\"\r\n                                *ngIf=\"row.show\"\r\n                                fxLayout\r\n                                fxLayoutAlign=\"start center\"\r\n                                fxLayoutGap=\"10px\">\r\n\r\n                                    <section\r\n                                        class=\"row-item no-padding\"\r\n                                        *ngFor=\"let rowItem of row.getChildren()\"\r\n                                        fxLayoutAlign=\"start center\"\r\n                                        fxLayoutGap=\"10px\"\r\n                                        fxFlex>\r\n                                            <img \r\n                                                fxHide.xs=\"true\"\r\n                                                *ngIf=\"rowItem.getImgSrc() ? true : false\"\r\n                                                [src]=\"rowItem.getImgSrc()\" \r\n                                                alt=\"row icon\">\r\n                                            <p \r\n                                                *ngIf=\"!rowItem.getButton() && !rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }}\r\n                                            </p>\r\n                                            <section\r\n                                                class=\"no-padding\"\r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\">\r\n                                                <img \r\n                                                    class=\"rating\"\r\n                                                    *ngFor=\"let i of rowItem.getRatingCollection()\"\r\n                                                    [src]=\"rowItem.getRatingImgSrc()\" \r\n                                                    alt=\"row icon\">\r\n                                            </section>\r\n                                            <!-- <p \r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }} rating\r\n                                            </p> -->\r\n                                            <button\r\n                                                *ngIf=\"rowItem.getButton()\"\r\n                                                (click)=\"rowItem.getFunction()([row.data])\"\r\n                                                class=\"row-item\">\r\n                                                    {{ rowItem.context }}\r\n                                            </button>\r\n                                    </section>\r\n                            </section>                            \r\n\r\n                            <section \r\n                                style=\"padding: 0;\"\r\n                                *ngIf='this.tempRef'>\r\n                                    <section *cdkDragPreview>\r\n                                        <section \r\n                                            *ngIf=\"false else this.tempRef\"></section>\r\n                                    </section>\r\n                            </section>\r\n\r\n                    </section>\r\n                </section>\r\n                \r\n                <section\r\n                    fxLayout\r\n                    fxLayoutAlign=\"center center\"\r\n                    fxLayoutGap=\"50px\">\r\n                    <mat-form-field\r\n                        fxFlex=\"50px\">\r\n                            <mat-label>per page:</mat-label>\r\n                            <input \r\n                                matInput\r\n                                type=\"number\"\r\n                                min=\"1\"\r\n                                [(ngModel)]=\"this.perPage\">\r\n                    </mat-form-field>\r\n                    <mat-button-toggle-group appearance=\"legacy\">\r\n                        <mat-button-toggle\r\n                            (click)=\"previous()\">\r\n                                <mat-icon>keyboard_arrow_left</mat-icon>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle\r\n                            (click)=\"next()\">\r\n                                <mat-icon>keyboard_arrow_right</mat-icon>\r\n                        </mat-button-toggle>\r\n                    </mat-button-toggle-group>\r\n                </section>\r\n        </section>\r\n\r\n</section>\r\n\r\n<lib-my-angular-table-context-menu\r\n    [hidden]=\"!this.showContextMenu\"></lib-my-angular-table-context-menu>",
                    styles: ["section{padding:10px}section.header{width:80vw;height:25px;padding:20px;border-top:1px solid grey;border-bottom:1px solid #d3d3d3}p.header-item{color:silver;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}p.header-item:hover{cursor:pointer;border-radius:2px;box-shadow:0 1px #d3d3d3}section.row{width:80vw;height:25px;padding:20px;box-shadow:0 1px #d3d3d3}section.row-item{cursor:default;overflow:hidden}p.row-item{color:grey;text-align:center;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item{overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item:focus{outline:0}.no-padding{padding:0}.box-shadow{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}mat-form-field{max-width:300px}img{width:15%;height:15%;border-radius:40%}img.rating{width:12%;height:12%}hr{width:20px}button{border:none;background-color:transparent;cursor:pointer;height:30px;border-radius:5px}button:hover{background-color:#ebebeb}.no-margin{margin:0}.selected{background-color:rgba(221,221,221,.2);box-shadow:0 1px 1px rgba(0,0,0,.2)}"]
                }] }
    ];
    /** @nocollapse */
    MyAngularTableComponent.ctorParameters = function () { return [
        { type: UiService }
    ]; };
    MyAngularTableComponent.propDecorators = {
        tempRef: [{ type: Input }],
        table: [{ type: Input }],
        perPage: [{ type: Input }],
        onListInit: [{ type: Output }],
        list: [{ type: ViewChild, args: ["list", { static: false },] }],
        contextMenu: [{ type: ViewChild, args: [ContextMenuComponent, { static: false },] }],
        documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }],
        documentRClick: [{ type: HostListener, args: ["document:contextmenu", ["$event"],] }]
    };
    return MyAngularTableComponent;
}());
export { MyAngularTableComponent };
if (false) {
    /** @type {?} */
    MyAngularTableComponent.prototype.tempRef;
    /** @type {?} */
    MyAngularTableComponent.prototype.table;
    /** @type {?} */
    MyAngularTableComponent.prototype.criteria;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.contextMenuComponent;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.selectedRows;
    /** @type {?} */
    MyAngularTableComponent.prototype.isValid;
    /** @type {?} */
    MyAngularTableComponent.prototype.perPage;
    /** @type {?} */
    MyAngularTableComponent.prototype.page;
    /** @type {?} */
    MyAngularTableComponent.prototype.onListInit;
    /** @type {?} */
    MyAngularTableComponent.prototype.showContextMenu;
    /** @type {?} */
    MyAngularTableComponent.prototype.onSort;
    /** @type {?} */
    MyAngularTableComponent.prototype.onCriteriaChange;
    /** @type {?} */
    MyAngularTableComponent.prototype.onShowSnackBar;
    /** @type {?} */
    MyAngularTableComponent.prototype.onSelect;
    /** @type {?} */
    MyAngularTableComponent.prototype.isSelected;
    /** @type {?} */
    MyAngularTableComponent.prototype.onContextMenu;
    /** @type {?} */
    MyAngularTableComponent.prototype.next;
    /** @type {?} */
    MyAngularTableComponent.prototype.previous;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.uiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktYW5ndWxhci10YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL215LWFuZ3VsYXItdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULEtBQUssRUFHTCxTQUFTLEVBQ1QsWUFBWSxFQUNaLE1BQU0sRUFDTixZQUFZLEVBR1osVUFBVSxHQUNYLE1BQU0sZUFBZSxDQUFDOztBQUd2QixPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sZ0VBQWdFLENBQUM7QUFHdkYsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ3BGLE9BQU8sRUFHTCxXQUFXLEdBRVosTUFBTSx3QkFBd0IsQ0FBQztBQUVoQztJQWtDRSxpQ0FBb0IsU0FBb0I7UUFBeEMsaUJBQTRDO1FBQXhCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUE1Qi9CLFlBQU8sR0FBZ0IsU0FBUyxDQUFDO1FBRW5DLGFBQVEsR0FBVyxFQUFFLENBQUM7UUFFckIsaUJBQVksR0FBVSxFQUFFLENBQUM7UUFDMUIsWUFBTyxHQUFZLEtBQUssQ0FBQztRQUN2QixZQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3RCLFNBQUksR0FBVyxDQUFDLENBQUM7UUFFZCxlQUFVLEdBQThCLElBQUksWUFBWSxFQUUvRCxDQUFDO1FBZUcsb0JBQWUsR0FBWSxLQUFLLENBQUM7UUFJeEMsV0FBTTs7OztRQUFHLFVBQUMsS0FBYTtZQUNyQixzQkFBc0I7WUFFdEIsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFekIseUNBQXlDO1FBQzNDLENBQUMsRUFBQzs7OztRQU1GLHFCQUFnQjs7O1FBQUc7WUFDakIsOEJBQThCO1lBQzlCLEtBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsR0FBUTs7b0JBQ3BDLEtBQUssR0FBRyxFQUFFO2dCQUNkLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUMsT0FBZ0I7b0JBQ3pDLEtBQUssSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNwRCxDQUFDLEVBQUMsQ0FBQztnQkFDSCxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQztvQkFBRSxHQUFHLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzs7b0JBQzlDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDO1FBRUYsbUJBQWM7Ozs7UUFBRyxVQUFDLE9BQWU7WUFDL0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUM7UUFFRixhQUFROzs7OztRQUFHLFVBQUMsS0FBaUIsRUFBRSxHQUFRO1lBQ3JDLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDakIsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLEtBQUssR0FBRyxFQUFULENBQVMsRUFBQyxFQUFFO29CQUM1QyxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTTs7OztvQkFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsS0FBSyxHQUFHLEVBQVQsQ0FBUyxFQUFDLENBQUM7aUJBQ2hFO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUM3QjthQUNGO2lCQUFNLElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUN2QyxLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQzthQUN4QjtRQUNILENBQUMsRUFBQztRQUVGLGVBQVU7Ozs7UUFBRyxVQUFDLEdBQVE7WUFDcEIsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsS0FBSyxHQUFHLEVBQVQsQ0FBUyxFQUFDLEVBQUU7Z0JBQzVDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQztRQUVGLGtCQUFhOzs7OztRQUFHLFVBQUMsS0FBaUIsRUFBRSxHQUFRO1lBQzFDLHNCQUFzQjtZQUN0QixvQkFBb0I7WUFFcEIsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxFQUFFO2dCQUMvQixLQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztnQkFFNUIsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxDQUFDO29CQUM5QixLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRzs7OztvQkFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxJQUFJLEVBQU4sQ0FBTSxFQUFDLENBQUM7O29CQUNuRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVqRCxLQUFJLENBQUMsb0JBQW9CLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2xEO1FBQ0gsQ0FBQyxFQUFDO1FBa0JGLFNBQUk7OztRQUFHO1lBQ0wsSUFDRSxLQUFJLENBQUMsSUFBSSxHQUFHLENBQUM7Z0JBQ2IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDO2dCQUV6RCxLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQyxFQUFDO1FBRUYsYUFBUTs7O1FBQUc7WUFDVCxJQUFJLEtBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQztnQkFBRSxLQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDakMsQ0FBQyxFQUFDO0lBMUZ5QyxDQUFDO0lBaEI1QyxzQkFBMEMseUNBQUk7Ozs7O1FBQTlDLFVBQStDLElBQWlCO1lBQzlELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUN2QixxQkFBcUI7WUFFckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFFRCxzQkFBd0QsZ0RBQVc7Ozs7O1FBQW5FLFVBQ0Usb0JBQXlCO1lBRXpCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxvQkFBb0IsQ0FBQztZQUNqRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztRQUNsRSxDQUFDOzs7T0FBQTs7Ozs7SUFvRWtELCtDQUFhOzs7O0lBQWhFLFVBQ0UsS0FBaUI7UUFFakIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFFN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPO1lBQUUsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFFd0QsZ0RBQWM7Ozs7SUFBdkUsVUFDRSxLQUFpQjtRQUVqQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUU3QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87WUFBRSxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUM3QyxDQUFDOzs7O0lBY0QsMENBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRXJDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTztZQUNmLE1BQU0sSUFBSSxLQUFLLENBQ2IsbUVBQW1FLENBQ3BFLENBQUM7UUFFSiwrQ0FBK0M7SUFDakQsQ0FBQzs7Ozs7SUFFRCw2Q0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDaEMseUdBQXlHO1FBQ3pHLDZDQUE2QztRQUM3Qyx3QkFBd0I7SUFDMUIsQ0FBQzs7OztJQUVELDZDQUFXOzs7SUFBWDtRQUNFLG9EQUFvRDtJQUN0RCxDQUFDOztnQkFqSkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLG1vUkFBZ0Q7O2lCQUVqRDs7OztnQkFiUSxTQUFTOzs7MEJBZWYsS0FBSzt3QkFDTCxLQUFLOzBCQUtMLEtBQUs7NkJBR0wsTUFBTTt1QkFHTixTQUFTLFNBQUMsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs4QkFPbkMsU0FBUyxTQUFDLG9CQUFvQixFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTtnQ0F5RWpELFlBQVksU0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQztpQ0FRekMsWUFBWSxTQUFDLHNCQUFzQixFQUFFLENBQUMsUUFBUSxDQUFDOztJQXdDbEQsOEJBQUM7Q0FBQSxBQWxKRCxJQWtKQztTQTdJWSx1QkFBdUI7OztJQUNsQywwQ0FBMEM7O0lBQzFDLHdDQUFzQjs7SUFDdEIsMkNBQTZCOzs7OztJQUM3Qix1REFBbUQ7Ozs7O0lBQ25ELCtDQUFpQzs7SUFDakMsMENBQWdDOztJQUNoQywwQ0FBNkI7O0lBQzdCLHVDQUF3Qjs7SUFFeEIsNkNBRUk7O0lBZUosa0RBQXdDOztJQUl4Qyx5Q0FNRTs7SUFNRixtREFVRTs7SUFFRixpREFFRTs7SUFFRiwyQ0FVRTs7SUFFRiw2Q0FLRTs7SUFFRixnREFhRTs7SUFrQkYsdUNBTUU7O0lBRUYsMkNBRUU7Ozs7O0lBMUZVLDRDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIENvbXBvbmVudCxcbiAgT25Jbml0LFxuICBJbnB1dCxcbiAgT25EZXN0cm95LFxuICBWaWV3RW5jYXBzdWxhdGlvbixcbiAgVmlld0NoaWxkLFxuICBIb3N0TGlzdGVuZXIsXG4gIE91dHB1dCxcbiAgRXZlbnRFbWl0dGVyLFxuICBTaW1wbGVDaGFuZ2VzLFxuICBPbkNoYW5nZXMsXG4gIEVsZW1lbnRSZWYsXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbi8vIG1vZGVsXG5pbXBvcnQgeyBUYWJsZSB9IGZyb20gXCIuL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS90YWJsZS90YWJsZS5tb2RlbFwiO1xuaW1wb3J0IHsgUm93IH0gZnJvbSBcIi4vc2hhcmVkL21vZGVsL3BhdHRlcm5zL3N0cnVjdHVyYWwvY29tcG9zaXRlL3RhYmxlL3Jvdy9yb3cubW9kZWxcIjtcbmltcG9ydCB7IFJvd0l0ZW0gfSBmcm9tIFwiLi9zaGFyZWQvbW9kZWwvcGF0dGVybnMvc3RydWN0dXJhbC9jb21wb3NpdGUvdGFibGUvcm93L3Jvdy1pdGVtLm1vZGVsXCI7XG5pbXBvcnQgeyBVaVNlcnZpY2UgfSBmcm9tIFwiLi9zaGFyZWQvc2VydmljZS91aS5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBDb250ZXh0TWVudUNvbXBvbmVudCB9IGZyb20gXCIuL3NoYXJlZC9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51LmNvbXBvbmVudFwiO1xuaW1wb3J0IHtcbiAgQ2RrRHJhZ0Ryb3AsXG4gIG1vdmVJdGVtSW5BcnJheSxcbiAgQ2RrRHJvcExpc3QsXG4gIHRyYW5zZmVyQXJyYXlJdGVtLFxufSBmcm9tIFwiQGFuZ3VsYXIvY2RrL2RyYWctZHJvcFwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwibGliLW15LWFuZ3VsYXItdGFibGVcIixcbiAgdGVtcGxhdGVVcmw6IFwiLi9teS1hbmd1bGFyLXRhYmxlLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9teS1hbmd1bGFyLXRhYmxlLmNvbXBvbmVudC5jc3NcIl0sXG59KVxuZXhwb3J0IGNsYXNzIE15QW5ndWxhclRhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIHRlbXBSZWY/OiBFbGVtZW50UmVmID0gdW5kZWZpbmVkO1xuICBASW5wdXQoKSB0YWJsZTogVGFibGU7XG4gIHB1YmxpYyBjcml0ZXJpYTogc3RyaW5nID0gXCJcIjtcbiAgcHJpdmF0ZSBjb250ZXh0TWVudUNvbXBvbmVudDogQ29udGV4dE1lbnVDb21wb25lbnQ7XG4gIHByaXZhdGUgc2VsZWN0ZWRSb3dzOiBhbnlbXSA9IFtdO1xuICBwdWJsaWMgaXNWYWxpZDogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSBwZXJQYWdlOiBudW1iZXIgPSAyO1xuICBwdWJsaWMgcGFnZTogbnVtYmVyID0gMDtcblxuICBAT3V0cHV0KCkgb25MaXN0SW5pdDogRXZlbnRFbWl0dGVyPENka0Ryb3BMaXN0PiA9IG5ldyBFdmVudEVtaXR0ZXI8XG4gICAgQ2RrRHJvcExpc3RcbiAgPigpO1xuICBAVmlld0NoaWxkKFwibGlzdFwiLCB7IHN0YXRpYzogZmFsc2UgfSkgc2V0IGxpc3QobGlzdDogQ2RrRHJvcExpc3QpIHtcbiAgICBsaXN0LmRhdGEgPSB0aGlzLnRhYmxlO1xuICAgIC8vIGNvbnNvbGUubG9nKGxpc3QpO1xuXG4gICAgdGhpcy5vbkxpc3RJbml0LmVtaXQobGlzdCk7XG4gIH1cblxuICBAVmlld0NoaWxkKENvbnRleHRNZW51Q29tcG9uZW50LCB7IHN0YXRpYzogZmFsc2UgfSkgc2V0IGNvbnRleHRNZW51KFxuICAgIGNvbnRleHRNZW51Q29tcG9uZW50OiBhbnlcbiAgKSB7XG4gICAgdGhpcy5jb250ZXh0TWVudUNvbXBvbmVudCA9IGNvbnRleHRNZW51Q29tcG9uZW50O1xuICAgIHRoaXMuY29udGV4dE1lbnVDb21wb25lbnQuc2V0TW9kZWwodGhpcy50YWJsZS5nZXRDb250ZXh0TWVudSgpKTtcbiAgfVxuXG4gIHB1YmxpYyBzaG93Q29udGV4dE1lbnU6IGJvb2xlYW4gPSBmYWxzZTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHVpU2VydmljZTogVWlTZXJ2aWNlKSB7fVxuXG4gIG9uU29ydCA9IChpbmRleDogbnVtYmVyKTogdm9pZCA9PiB7XG4gICAgLy8gY29uc29sZS5sb2coaW5kZXgpO1xuXG4gICAgdGhpcy50YWJsZS5vblNvcnQoaW5kZXgpO1xuXG4gICAgLy8gY29uc29sZS5sb2codGhpcy50YWJsZS5nZXRDaGlsZHJlbigpKTtcbiAgfTtcblxuICAvLyBnZXRSb3dzID0gKCk6IFJvd1tdID0+IHtcbiAgLy8gICByZXR1cm4gPFJvd1tdPnRoaXMudGFibGUuZ2V0Q2hpbGRyZW4oKTtcbiAgLy8gfVxuXG4gIG9uQ3JpdGVyaWFDaGFuZ2UgPSAoKTogdm9pZCA9PiB7XG4gICAgLy8gY29uc29sZS5sb2codGhpcy5jcml0ZXJpYSk7XG4gICAgdGhpcy50YWJsZS5nZXRDaGlsZHJlbigpLmZvckVhY2goKHJvdzogUm93KSA9PiB7XG4gICAgICBsZXQgdmFsdWUgPSBcIlwiO1xuICAgICAgcm93LmdldENoaWxkcmVuKCkuZm9yRWFjaCgocm93SXRlbTogUm93SXRlbSkgPT4ge1xuICAgICAgICB2YWx1ZSArPSByb3dJdGVtLmNvbnRleHQudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpO1xuICAgICAgfSk7XG4gICAgICBpZiAodmFsdWUuaW5jbHVkZXModGhpcy5jcml0ZXJpYSkpIHJvdy5zaG93ID0gdHJ1ZTtcbiAgICAgIGVsc2Ugcm93LnNob3cgPSBmYWxzZTtcbiAgICB9KTtcbiAgfTtcblxuICBvblNob3dTbmFja0JhciA9IChtZXNzYWdlOiBzdHJpbmcpOiB2b2lkID0+IHtcbiAgICB0aGlzLnVpU2VydmljZS5vblNob3dTbmFja0JhcihtZXNzYWdlLCBudWxsLCAxNTAwKTtcbiAgfTtcblxuICBvblNlbGVjdCA9IChldmVudDogTW91c2VFdmVudCwgcm93OiBSb3cpOiB2b2lkID0+IHtcbiAgICBpZiAoZXZlbnQuY3RybEtleSkge1xuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRSb3dzLmZpbmQoKHIpID0+IHIgPT09IHJvdykpIHtcbiAgICAgICAgdGhpcy5zZWxlY3RlZFJvd3MgPSB0aGlzLnNlbGVjdGVkUm93cy5maWx0ZXIoKHIpID0+IHIgIT09IHJvdyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnNlbGVjdGVkUm93cy5wdXNoKHJvdyk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICh0aGlzLnNlbGVjdGVkUm93cy5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLnNlbGVjdGVkUm93cyA9IFtdO1xuICAgIH1cbiAgfTtcblxuICBpc1NlbGVjdGVkID0gKHJvdzogUm93KTogYm9vbGVhbiA9PiB7XG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRSb3dzLmZpbmQoKHIpID0+IHIgPT09IHJvdykpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG4gIH07XG5cbiAgb25Db250ZXh0TWVudSA9IChldmVudDogTW91c2VFdmVudCwgcm93OiBSb3cpOiB2b2lkID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyhldmVudCk7XG4gICAgLy8gY29uc29sZS5sb2cocm93KTtcblxuICAgIGlmICh0aGlzLnRhYmxlLmdldENvbnRleHRNZW51KCkpIHtcbiAgICAgIHRoaXMuc2hvd0NvbnRleHRNZW51ID0gdHJ1ZTtcblxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRSb3dzLmxlbmd0aCA+IDApXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVDb21wb25lbnQuZGF0YSA9IHRoaXMuc2VsZWN0ZWRSb3dzLm1hcCgocikgPT4gci5kYXRhKTtcbiAgICAgIGVsc2UgdGhpcy5jb250ZXh0TWVudUNvbXBvbmVudC5kYXRhID0gW3Jvdy5kYXRhXTtcblxuICAgICAgdGhpcy5jb250ZXh0TWVudUNvbXBvbmVudC5zaG93Q29udGV4dE1lbnUoZXZlbnQpO1xuICAgIH1cbiAgfTtcblxuICBASG9zdExpc3RlbmVyKFwiZG9jdW1lbnQ6Y2xpY2tcIiwgW1wiJGV2ZW50XCJdKSBwdWJsaWMgZG9jdW1lbnRDbGljayhcbiAgICBldmVudDogTW91c2VFdmVudFxuICApOiB2b2lkIHtcbiAgICB0aGlzLnNob3dDb250ZXh0TWVudSA9IGZhbHNlO1xuXG4gICAgaWYgKCFldmVudC5jdHJsS2V5KSB0aGlzLnNlbGVjdGVkUm93cyA9IFtdO1xuICB9XG5cbiAgQEhvc3RMaXN0ZW5lcihcImRvY3VtZW50OmNvbnRleHRtZW51XCIsIFtcIiRldmVudFwiXSkgcHVibGljIGRvY3VtZW50UkNsaWNrKFxuICAgIGV2ZW50OiBNb3VzZUV2ZW50XG4gICk6IHZvaWQge1xuICAgIHRoaXMuc2hvd0NvbnRleHRNZW51ID0gZmFsc2U7XG5cbiAgICBpZiAoIWV2ZW50LmN0cmxLZXkpIHRoaXMuc2VsZWN0ZWRSb3dzID0gW107XG4gIH1cblxuICBuZXh0ID0gKCk6IHZvaWQgPT4ge1xuICAgIGlmIChcbiAgICAgIHRoaXMucGFnZSArIDEgPFxuICAgICAgTWF0aC5jZWlsKHRoaXMudGFibGUuZ2V0Q2hpbGRyZW4oKS5sZW5ndGggLyB0aGlzLnBlclBhZ2UpXG4gICAgKVxuICAgICAgdGhpcy5wYWdlKys7XG4gIH07XG5cbiAgcHJldmlvdXMgPSAoKTogdm9pZCA9PiB7XG4gICAgaWYgKHRoaXMucGFnZSA+IDApIHRoaXMucGFnZS0tO1xuICB9O1xuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuaXNWYWxpZCA9IHRoaXMudGFibGUudmFsaWRhdGUoKTtcblxuICAgIGlmICghdGhpcy5pc1ZhbGlkKVxuICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICBcIk51bWJlciBvZiBoZWFkZXIgaXRlbXMgbXVzdCBiZSB0aGUgc2FtZSBhcyB0aGUgbnVtYmVyIGluIGVhY2ggcm93XCJcbiAgICAgICk7XG5cbiAgICAvLyBjb25zb2xlLmxvZyhcIk15QW5ndWxhclRhYmxlQ29tcG9uZW50IGluaXRcIik7XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgLy9DYWxsZWQgYmVmb3JlIGFueSBvdGhlciBsaWZlY3ljbGUgaG9vay4gVXNlIGl0IHRvIGluamVjdCBkZXBlbmRlbmNpZXMsIGJ1dCBhdm9pZCBhbnkgc2VyaW91cyB3b3JrIGhlcmUuXG4gICAgLy9BZGQgJyR7aW1wbGVtZW50cyBPbkNoYW5nZXN9JyB0byB0aGUgY2xhc3MuXG4gICAgLy8gY29uc29sZS5sb2coY2hhbmdlcyk7XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhcIk15QW5ndWxhclRhYmxlQ29tcG9uZW50IGRlc3Ryb3llZFwiKTtcbiAgfVxufVxuIl19