/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from "@angular/core";
import { MyAngularTableComponent } from "./my-angular-table.component";
import { SharedModule } from "./shared/shared.module";
var MyAngularTableModule = /** @class */ (function () {
    function MyAngularTableModule() {
    }
    MyAngularTableModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [MyAngularTableComponent],
                    imports: [SharedModule],
                    exports: [MyAngularTableComponent],
                    providers: [],
                },] }
    ];
    return MyAngularTableModule;
}());
export { MyAngularTableModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktYW5ndWxhci10YWJsZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL215LWFuZ3VsYXItdGFibGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUV0RDtJQUFBO0lBTW1DLENBQUM7O2dCQU5uQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsdUJBQXVCLENBQUM7b0JBQ3ZDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDdkIsT0FBTyxFQUFFLENBQUMsdUJBQXVCLENBQUM7b0JBQ2xDLFNBQVMsRUFBRSxFQUFFO2lCQUNkOztJQUNrQywyQkFBQztDQUFBLEFBTnBDLElBTW9DO1NBQXZCLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE15QW5ndWxhclRhYmxlQ29tcG9uZW50IH0gZnJvbSBcIi4vbXktYW5ndWxhci10YWJsZS5jb21wb25lbnRcIjtcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gXCIuL3NoYXJlZC9zaGFyZWQubW9kdWxlXCI7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW015QW5ndWxhclRhYmxlQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1NoYXJlZE1vZHVsZV0sXG4gIGV4cG9ydHM6IFtNeUFuZ3VsYXJUYWJsZUNvbXBvbmVudF0sXG4gIHByb3ZpZGVyczogW10sXG59KVxuZXhwb3J0IGNsYXNzIE15QW5ndWxhclRhYmxlTW9kdWxlIHt9XG4iXX0=