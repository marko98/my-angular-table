/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
// model
import { Collection } from '../../collection.model';
var RowItem = /** @class */ (function (_super) {
    tslib_1.__extends(RowItem, _super);
    function RowItem(context) {
        var _this = _super.call(this) || this;
        _this.context = context;
        _this.ratingImgSrc = "https://img.icons8.com/android/2x/star.png";
        _this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return true;
        });
        _this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return true;
        });
        _this.shouldHaveChildren = (/**
         * @return {?}
         */
        function () {
            return false;
        });
        _this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return -1;
        });
        _this.getImgSrc = (/**
         * @return {?}
         */
        function () {
            return _this.imgSrc;
        });
        _this.setImgSrc = (/**
         * @param {?} imgSrc
         * @return {?}
         */
        function (imgSrc) {
            _this.imgSrc = imgSrc;
        });
        _this.getButton = (/**
         * @return {?}
         */
        function () {
            return _this.button;
        });
        _this.setButton = (/**
         * @param {?} button
         * @return {?}
         */
        function (button) {
            _this.button = button;
        });
        _this.getFunction = (/**
         * @return {?}
         */
        function () {
            return _this.function;
        });
        _this.setFunction = (/**
         * @param {?} f
         * @return {?}
         */
        function (f) {
            _this.function = f;
        });
        _this.getRating = (/**
         * @return {?}
         */
        function () {
            return _this.rating;
        });
        _this.setRating = (/**
         * @param {?} rating
         * @return {?}
         */
        function (rating) {
            _this.rating = rating;
        });
        _this.getRatingImgSrc = (/**
         * @return {?}
         */
        function () {
            return _this.ratingImgSrc;
        });
        _this.setRatingImgSrc = (/**
         * @param {?} ratingImgSrc
         * @return {?}
         */
        function (ratingImgSrc) {
            _this.ratingImgSrc = ratingImgSrc;
        });
        _this.getRatingCollection = (/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var number;
            number = +_this.context;
            if (isNaN(number)) {
                number = 0;
            }
            else {
                if (number > 5)
                    number = 5;
                if (number < 0)
                    number = 0;
            }
            return Array(number);
        });
        // getChildren = (): Collection[] => {
        //     // vracamo kopiju
        //     // return this.children.slice();
        //     return this.children;
        // }
        // getParent = (): Collection => {
        //     return this.parent;
        // }
        // setParent = (parent: Collection): boolean => {
        //     this.parent = parent;
        //     return true;
        // }
        _this.clone = (/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var clone = new RowItem(_this.context);
            return clone;
        });
        _this.prototype = (/**
         * @param {?} rowItem
         * @return {?}
         */
        function (rowItem) {
            rowItem.context = _this.context;
        });
        if (typeof _this.context === "number")
            _this.context = _this.context.toString();
        if (_this.context instanceof Date)
            _this.context = _this.context.toLocaleString();
        return _this;
    }
    return RowItem;
}(Collection));
export { RowItem };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.imgSrc;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.button;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.function;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.rating;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.ratingImgSrc;
    /** @type {?} */
    RowItem.prototype.addChild;
    /** @type {?} */
    RowItem.prototype.removeChild;
    /** @type {?} */
    RowItem.prototype.shouldHaveChildren;
    /** @type {?} */
    RowItem.prototype.getChildIndex;
    /** @type {?} */
    RowItem.prototype.getImgSrc;
    /** @type {?} */
    RowItem.prototype.setImgSrc;
    /** @type {?} */
    RowItem.prototype.getButton;
    /** @type {?} */
    RowItem.prototype.setButton;
    /** @type {?} */
    RowItem.prototype.getFunction;
    /** @type {?} */
    RowItem.prototype.setFunction;
    /** @type {?} */
    RowItem.prototype.getRating;
    /** @type {?} */
    RowItem.prototype.setRating;
    /** @type {?} */
    RowItem.prototype.getRatingImgSrc;
    /** @type {?} */
    RowItem.prototype.setRatingImgSrc;
    /** @type {?} */
    RowItem.prototype.getRatingCollection;
    /** @type {?} */
    RowItem.prototype.clone;
    /** @type {?} */
    RowItem.prototype.prototype;
    /** @type {?} */
    RowItem.prototype.context;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93LWl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS90YWJsZS9yb3cvcm93LWl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBd0JwRDtJQUE2QixtQ0FBVTtJQVNuQyxpQkFBbUIsT0FBK0I7UUFBbEQsWUFDSSxpQkFBTyxTQU1WO1FBUGtCLGFBQU8sR0FBUCxPQUFPLENBQXdCO1FBRjFDLGtCQUFZLEdBQVcsNENBQTRDLENBQUM7UUFXNUUsY0FBUTs7OztRQUFHLFVBQUMsS0FBaUI7WUFDekIsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxFQUFBO1FBRUQsaUJBQVc7Ozs7UUFBRyxVQUFDLEtBQWlCO1lBQzVCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUMsRUFBQTtRQUVELHdCQUFrQjs7O1FBQUc7WUFDakIsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxFQUFBO1FBRUQsbUJBQWE7Ozs7UUFBRyxVQUFDLEtBQWlCO1lBQzlCLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFDZCxDQUFDLEVBQUE7UUFFRCxlQUFTOzs7UUFBRztZQUNSLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDLEVBQUE7UUFFRCxlQUFTOzs7O1FBQUcsVUFBQyxNQUFjO1lBQ3ZCLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3pCLENBQUMsRUFBQTtRQUVELGVBQVM7OztRQUFHO1lBQ1IsT0FBTyxLQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUMsRUFBQTtRQUVELGVBQVM7Ozs7UUFBRyxVQUFDLE1BQWU7WUFDeEIsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDekIsQ0FBQyxFQUFBO1FBRUQsaUJBQVc7OztRQUFHO1lBQ1YsT0FBTyxLQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUMsRUFBQTtRQUVELGlCQUFXOzs7O1FBQUcsVUFBQyxDQUFXO1lBQ3RCLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLENBQUMsRUFBQTtRQUVELGVBQVM7OztRQUFHO1lBQ1IsT0FBTyxLQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUMsRUFBQTtRQUVELGVBQVM7Ozs7UUFBRyxVQUFDLE1BQWU7WUFDeEIsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDekIsQ0FBQyxFQUFBO1FBRUQscUJBQWU7OztRQUFHO1lBQ2QsT0FBTyxLQUFJLENBQUMsWUFBWSxDQUFDO1FBQzdCLENBQUMsRUFBQTtRQUVELHFCQUFlOzs7O1FBQUcsVUFBQyxZQUFvQjtZQUNuQyxLQUFJLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUNyQyxDQUFDLEVBQUE7UUFFRCx5QkFBbUI7OztRQUFHOztnQkFDZCxNQUFjO1lBRWxCLE1BQU0sR0FBRyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUM7WUFFdkIsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUM7Z0JBQ2QsTUFBTSxHQUFHLENBQUMsQ0FBQzthQUNkO2lCQUNJO2dCQUNELElBQUksTUFBTSxHQUFHLENBQUM7b0JBQ1YsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDZixJQUFJLE1BQU0sR0FBRyxDQUFDO29CQUNWLE1BQU0sR0FBRyxDQUFDLENBQUM7YUFDbEI7WUFFRCxPQUFPLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6QixDQUFDLEVBQUE7Ozs7Ozs7Ozs7Ozs7UUFpQkQsV0FBSzs7O1FBQUc7O2dCQUNBLEtBQUssR0FBRyxJQUFJLE9BQU8sQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDO1lBQ3JDLE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsRUFBQTtRQUVELGVBQVM7Ozs7UUFBRyxVQUFDLE9BQWdCO1lBQ3pCLE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQztRQUNuQyxDQUFDLEVBQUE7UUF0R0csSUFBSSxPQUFPLEtBQUksQ0FBQyxPQUFPLEtBQUssUUFBUTtZQUNoQyxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0MsSUFBSSxLQUFJLENBQUMsT0FBTyxZQUFZLElBQUk7WUFDNUIsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDOztJQUNyRCxDQUFDO0lBb0dMLGNBQUM7QUFBRCxDQUFDLEFBcEhELENBQTZCLFVBQVUsR0FvSHRDOzs7Ozs7O0lBakhHLHlCQUF1Qjs7Ozs7SUFDdkIseUJBQXdCOzs7OztJQUN4QiwyQkFBMkI7Ozs7O0lBQzNCLHlCQUF3Qjs7Ozs7SUFDeEIsK0JBQTRFOztJQVc1RSwyQkFFQzs7SUFFRCw4QkFFQzs7SUFFRCxxQ0FFQzs7SUFFRCxnQ0FFQzs7SUFFRCw0QkFFQzs7SUFFRCw0QkFFQzs7SUFFRCw0QkFFQzs7SUFFRCw0QkFFQzs7SUFFRCw4QkFFQzs7SUFFRCw4QkFFQzs7SUFFRCw0QkFFQzs7SUFFRCw0QkFFQzs7SUFFRCxrQ0FFQzs7SUFFRCxrQ0FFQzs7SUFFRCxzQ0FnQkM7O0lBaUJELHdCQUdDOztJQUVELDRCQUVDOztJQXpHVywwQkFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vLi4vY29sbGVjdGlvbi5tb2RlbCc7XHJcblxyXG4vLyBpbnRlcmZhY2VcclxuaW1wb3J0IHsgUHJvdG90eXBlIH0gZnJvbSAnLi4vLi4vLi4vLi4vY3JlYXRpb25hbC9wcm90b3R5cGUvcHJvdG90eXBlLmludGVyZmFjZSc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgUm93SXRlbVZhbHVlIHtcclxuICAgIGNvbnRleHQ6IHN0cmluZyB8IG51bWJlciB8IERhdGUsXHJcbiAgICBpbWdTcmM/OiBzdHJpbmcsXHJcbiAgICBidXR0b24/OiBSb3dJdGVtQnV0dG9uVmFsdWVcclxuICAgIHJhdGluZz86IFJvd0l0ZW1SYXRpbmdWYWx1ZVxyXG59XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgUm93SXRlbUhlYWRlclZhbHVlIHtcclxuICAgIGNvbnRleHQ6IHN0cmluZyB8IG51bWJlciB8IERhdGVcclxufVxyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFJvd0l0ZW1CdXR0b25WYWx1ZSB7XHJcbiAgICBmdW5jdGlvbjogRnVuY3Rpb25cclxufVxyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIFJvd0l0ZW1SYXRpbmdWYWx1ZSB7XHJcbiAgICByYXRpbmdJbWdTcmM/OiBzdHJpbmdcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFJvd0l0ZW0gZXh0ZW5kcyBDb2xsZWN0aW9uIGltcGxlbWVudHMgUHJvdG90eXBlPFJvd0l0ZW0+IHtcclxuICAgIC8vIHByb3RlY3RlZCBjaGlsZHJlbjogQ29sbGVjdGlvbltdID0gW107XHJcbiAgICAvLyBwcm90ZWN0ZWQgcGFyZW50OiBDb2xsZWN0aW9uID0gdW5kZWZpbmVkO1xyXG4gICAgcHJpdmF0ZSBpbWdTcmM6IHN0cmluZztcclxuICAgIHByaXZhdGUgYnV0dG9uOiBib29sZWFuO1xyXG4gICAgcHJpdmF0ZSBmdW5jdGlvbjogRnVuY3Rpb247XHJcbiAgICBwcml2YXRlIHJhdGluZzogYm9vbGVhbjtcclxuICAgIHByaXZhdGUgcmF0aW5nSW1nU3JjOiBzdHJpbmcgPSBcImh0dHBzOi8vaW1nLmljb25zOC5jb20vYW5kcm9pZC8yeC9zdGFyLnBuZ1wiO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBjb250ZXh0OiBzdHJpbmcgfCBudW1iZXIgfCBEYXRlKXtcclxuICAgICAgICBzdXBlcigpO1xyXG5cclxuICAgICAgICBpZiAodHlwZW9mIHRoaXMuY29udGV4dCA9PT0gXCJudW1iZXJcIilcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0ID0gdGhpcy5jb250ZXh0LnRvU3RyaW5nKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGV4dCBpbnN0YW5jZW9mIERhdGUpXHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dCA9IHRoaXMuY29udGV4dC50b0xvY2FsZVN0cmluZygpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZENoaWxkID0gKGNoaWxkOiBDb2xsZWN0aW9uKTogYm9vbGVhbiA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlQ2hpbGQgPSAoY2hpbGQ6IENvbGxlY3Rpb24pOiBib29sZWFuID0+IHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBzaG91bGRIYXZlQ2hpbGRyZW4gPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENoaWxkSW5kZXggPSAoY2hpbGQ6IENvbGxlY3Rpb24pOiBudW1iZXIgPT4ge1xyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJbWdTcmMgPSAoKTogc3RyaW5nID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pbWdTcmM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0SW1nU3JjID0gKGltZ1NyYzogc3RyaW5nKTogdm9pZCA9PiB7XHJcbiAgICAgICAgdGhpcy5pbWdTcmMgPSBpbWdTcmM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QnV0dG9uID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmJ1dHRvbjtcclxuICAgIH1cclxuXHJcbiAgICBzZXRCdXR0b24gPSAoYnV0dG9uOiBib29sZWFuKTogdm9pZCA9PiB7XHJcbiAgICAgICAgdGhpcy5idXR0b24gPSBidXR0b247XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RnVuY3Rpb24gPSAoKTogRnVuY3Rpb24gPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZ1bmN0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEZ1bmN0aW9uID0gKGY6IEZ1bmN0aW9uKTogdm9pZCA9PiB7XHJcbiAgICAgICAgdGhpcy5mdW5jdGlvbiA9IGY7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmF0aW5nID0gKCk6IGJvb2xlYW4gPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJhdGluZztcclxuICAgIH1cclxuXHJcbiAgICBzZXRSYXRpbmcgPSAocmF0aW5nOiBib29sZWFuKTogdm9pZCA9PiB7XHJcbiAgICAgICAgdGhpcy5yYXRpbmcgPSByYXRpbmc7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmF0aW5nSW1nU3JjID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmF0aW5nSW1nU3JjO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFJhdGluZ0ltZ1NyYyA9IChyYXRpbmdJbWdTcmM6IHN0cmluZyk6IHZvaWQgPT4ge1xyXG4gICAgICAgIHRoaXMucmF0aW5nSW1nU3JjID0gcmF0aW5nSW1nU3JjO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJhdGluZ0NvbGxlY3Rpb24gPSAoKTogYW55W10gPT4ge1xyXG4gICAgICAgIGxldCBudW1iZXI6IG51bWJlcjtcclxuXHJcbiAgICAgICAgbnVtYmVyID0gK3RoaXMuY29udGV4dDtcclxuXHJcbiAgICAgICAgaWYgKGlzTmFOKG51bWJlcikpe1xyXG4gICAgICAgICAgICBudW1iZXIgPSAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKG51bWJlciA+IDUpXHJcbiAgICAgICAgICAgICAgICBudW1iZXIgPSA1O1xyXG4gICAgICAgICAgICBpZiAobnVtYmVyIDwgMClcclxuICAgICAgICAgICAgICAgIG51bWJlciA9IDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIHJldHVybiBBcnJheShudW1iZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGdldENoaWxkcmVuID0gKCk6IENvbGxlY3Rpb25bXSA9PiB7XHJcbiAgICAvLyAgICAgLy8gdnJhY2FtbyBrb3BpanVcclxuICAgIC8vICAgICAvLyByZXR1cm4gdGhpcy5jaGlsZHJlbi5zbGljZSgpO1xyXG4gICAgLy8gICAgIHJldHVybiB0aGlzLmNoaWxkcmVuO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIC8vIGdldFBhcmVudCA9ICgpOiBDb2xsZWN0aW9uID0+IHtcclxuICAgIC8vICAgICByZXR1cm4gdGhpcy5wYXJlbnQ7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgLy8gc2V0UGFyZW50ID0gKHBhcmVudDogQ29sbGVjdGlvbik6IGJvb2xlYW4gPT4ge1xyXG4gICAgLy8gICAgIHRoaXMucGFyZW50ID0gcGFyZW50O1xyXG4gICAgLy8gICAgIHJldHVybiB0cnVlO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIGNsb25lID0gKCk6IFJvd0l0ZW0gPT4ge1xyXG4gICAgICAgIGxldCBjbG9uZSA9IG5ldyBSb3dJdGVtKHRoaXMuY29udGV4dCk7XHJcbiAgICAgICAgcmV0dXJuIGNsb25lO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RvdHlwZSA9IChyb3dJdGVtOiBSb3dJdGVtKTogdm9pZCA9PiB7XHJcbiAgICAgICAgcm93SXRlbS5jb250ZXh0ID0gdGhpcy5jb250ZXh0O1xyXG4gICAgfVxyXG5cclxufSJdfQ==