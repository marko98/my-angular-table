/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
// model
import { Collection } from "../../collection.model";
import { RowItem } from "./row-item.model";
var Row = /** @class */ (function (_super) {
    tslib_1.__extends(Row, _super);
    function Row(color) {
        var _this = _super.call(this) || this;
        // protected children: Collection[] = [];
        // protected parent: Collection = undefined;
        _this.show = true;
        _this.dragPreviewTempRefSubject = undefined;
        _this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            _this.children.push(child);
            child.setParent(_this);
            return true;
        });
        _this.addChildValue = (/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            /** @type {?} */
            var child = new RowItem(value.context);
            if (value.imgSrc)
                child.setImgSrc(value.imgSrc);
            if (value.button) {
                child.setButton(true);
                child.setFunction(value.button.function);
            }
            if (value.rating) {
                child.setRating(true);
                if (value.rating.ratingImgSrc)
                    child.setRatingImgSrc(value.rating.ratingImgSrc);
            }
            return _this.addChild(child);
        });
        _this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            _this.children = _this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c !== child; }));
            child.setParent(undefined);
            return true;
        });
        _this.shouldHaveChildren = (/**
         * @return {?}
         */
        function () {
            return true;
        });
        _this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return _this.children.findIndex((/**
             * @param {?} r
             * @return {?}
             */
            function (r) { return r === child; }));
        });
        // getChildren = (): Collection[] => {
        //     // vracamo kopiju
        //     // return this.children.slice();
        //     return this.children;
        // }
        // getParent = (): Collection => {
        //     return this.parent;
        // }
        // setParent = (parent: Collection): boolean => {
        //     this.parent = parent;
        //     return true;
        // }
        _this.clone = (/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var clone = new Row();
            _this.children.forEach((/**
             * @param {?} c
             * @return {?}
             */
            function (c) {
                clone.addChild(c.clone());
            }));
            return clone;
        });
        _this.prototype = (/**
         * @param {?} row
         * @return {?}
         */
        function (row) {
            _this.children.forEach((/**
             * @param {?} c
             * @return {?}
             */
            function (c) {
                row.addChild(c.clone());
            }));
        });
        if (color)
            _this.color = color;
        return _this;
    }
    return Row;
}(Collection));
export { Row };
if (false) {
    /** @type {?} */
    Row.prototype.show;
    /** @type {?} */
    Row.prototype.data;
    /** @type {?} */
    Row.prototype.color;
    /** @type {?} */
    Row.prototype.dragPreviewTempRefSubject;
    /** @type {?} */
    Row.prototype.addChild;
    /** @type {?} */
    Row.prototype.addChildValue;
    /** @type {?} */
    Row.prototype.removeChild;
    /** @type {?} */
    Row.prototype.shouldHaveChildren;
    /** @type {?} */
    Row.prototype.getChildIndex;
    /** @type {?} */
    Row.prototype.clone;
    /** @type {?} */
    Row.prototype.prototype;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvcGF0dGVybnMvc3RydWN0dXJhbC9jb21wb3NpdGUvdGFibGUvcm93L3Jvdy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFJQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDcEQsT0FBTyxFQUFFLE9BQU8sRUFBb0MsTUFBTSxrQkFBa0IsQ0FBQztBQWU3RTtJQUF5QiwrQkFBVTtJQVFqQyxhQUFZLEtBQWM7UUFBMUIsWUFDRSxpQkFBTyxTQUdSOzs7UUFUTSxVQUFJLEdBQVksSUFBSSxDQUFDO1FBR3JCLCtCQUF5QixHQUFnQyxTQUFTLENBQUM7UUFRMUUsY0FBUTs7OztRQUFHLFVBQUMsS0FBYztZQUN4QixLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsbUJBQWE7Ozs7UUFBRyxVQUFDLEtBQW1COztnQkFDOUIsS0FBSyxHQUFHLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFFdEMsSUFBSSxLQUFLLENBQUMsTUFBTTtnQkFBRSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUVoRCxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ2hCLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RCLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUMxQztZQUVELElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtnQkFDaEIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFdEIsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVk7b0JBQzNCLEtBQUssQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNwRDtZQUVELE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixDQUFDLEVBQUM7UUFFRixpQkFBVzs7OztRQUFHLFVBQUMsS0FBYztZQUMzQixLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztZQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxLQUFLLEtBQUssRUFBWCxDQUFXLEVBQUMsQ0FBQztZQUN6RCxLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzNCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsd0JBQWtCOzs7UUFBRztZQUNuQixPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLG1CQUFhOzs7O1FBQUcsVUFBQyxLQUFjO1lBQzdCLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLEtBQUssS0FBSyxFQUFYLENBQVcsRUFBQyxDQUFDO1FBQ3JELENBQUMsRUFBQzs7Ozs7Ozs7Ozs7OztRQWlCRixXQUFLOzs7UUFBRzs7Z0JBQ0YsS0FBSyxHQUFHLElBQUksR0FBRyxFQUFFO1lBQ3JCLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsQ0FBVTtnQkFDL0IsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUM1QixDQUFDLEVBQUMsQ0FBQztZQUNILE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDO1FBRUYsZUFBUzs7OztRQUFHLFVBQUMsR0FBUTtZQUNuQixLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFDLENBQVU7Z0JBQy9CLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDMUIsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUM7UUF0RUEsSUFBSSxLQUFLO1lBQUUsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7O0lBQ2hDLENBQUM7SUFzRUgsVUFBQztBQUFELENBQUMsQUFsRkQsQ0FBeUIsVUFBVSxHQWtGbEM7Ozs7SUEvRUMsbUJBQTRCOztJQUM1QixtQkFBbUI7O0lBQ25CLG9CQUFxQjs7SUFDckIsd0NBQTBFOztJQVExRSx1QkFJRTs7SUFFRiw0QkFrQkU7O0lBRUYsMEJBSUU7O0lBRUYsaUNBRUU7O0lBRUYsNEJBRUU7O0lBaUJGLG9CQU1FOztJQUVGLHdCQUlFIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW50ZXJmYWNlXHJcbmltcG9ydCB7IFByb3RvdHlwZSB9IGZyb20gXCIuLi8uLi8uLi8uLi9jcmVhdGlvbmFsL3Byb3RvdHlwZS9wcm90b3R5cGUuaW50ZXJmYWNlXCI7XHJcblxyXG4vLyBtb2RlbFxyXG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSBcIi4uLy4uL2NvbGxlY3Rpb24ubW9kZWxcIjtcclxuaW1wb3J0IHsgUm93SXRlbSwgUm93SXRlbVZhbHVlLCBSb3dJdGVtSGVhZGVyVmFsdWUgfSBmcm9tIFwiLi9yb3ctaXRlbS5tb2RlbFwiO1xyXG5pbXBvcnQgeyBUZW1wbGF0ZVJlZiwgRWxlbWVudFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gXCJyeGpzXCI7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgUm93VmFsdWUge1xyXG4gIGRhdGE6IGFueTtcclxuICBkcmFnUHJldmlld1RlbXBSZWZTdWJqZWN0PzogQmVoYXZpb3JTdWJqZWN0PEVsZW1lbnRSZWY+O1xyXG4gIGNvbG9yPzogc3RyaW5nO1xyXG4gIHJvd0l0ZW1zOiBSb3dJdGVtVmFsdWVbXTtcclxufVxyXG5cclxuZXhwb3J0IGRlY2xhcmUgaW50ZXJmYWNlIEhlYWRlclZhbHVlIHtcclxuICBoZWFkZXJJdGVtczogUm93SXRlbUhlYWRlclZhbHVlW107XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBSb3cgZXh0ZW5kcyBDb2xsZWN0aW9uIGltcGxlbWVudHMgUHJvdG90eXBlPFJvdz4ge1xyXG4gIC8vIHByb3RlY3RlZCBjaGlsZHJlbjogQ29sbGVjdGlvbltdID0gW107XHJcbiAgLy8gcHJvdGVjdGVkIHBhcmVudDogQ29sbGVjdGlvbiA9IHVuZGVmaW5lZDtcclxuICBwdWJsaWMgc2hvdzogYm9vbGVhbiA9IHRydWU7XHJcbiAgcHVibGljIGRhdGE6IGFueVtdO1xyXG4gIHB1YmxpYyBjb2xvcjogc3RyaW5nO1xyXG4gIHB1YmxpYyBkcmFnUHJldmlld1RlbXBSZWZTdWJqZWN0OiBCZWhhdmlvclN1YmplY3Q8RWxlbWVudFJlZj4gPSB1bmRlZmluZWQ7XHJcblxyXG4gIGNvbnN0cnVjdG9yKGNvbG9yPzogc3RyaW5nKSB7XHJcbiAgICBzdXBlcigpO1xyXG5cclxuICAgIGlmIChjb2xvcikgdGhpcy5jb2xvciA9IGNvbG9yO1xyXG4gIH1cclxuXHJcbiAgYWRkQ2hpbGQgPSAoY2hpbGQ6IFJvd0l0ZW0pOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4ucHVzaChjaGlsZCk7XHJcbiAgICBjaGlsZC5zZXRQYXJlbnQodGhpcyk7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBhZGRDaGlsZFZhbHVlID0gKHZhbHVlOiBSb3dJdGVtVmFsdWUpOiBib29sZWFuID0+IHtcclxuICAgIGxldCBjaGlsZCA9IG5ldyBSb3dJdGVtKHZhbHVlLmNvbnRleHQpO1xyXG5cclxuICAgIGlmICh2YWx1ZS5pbWdTcmMpIGNoaWxkLnNldEltZ1NyYyh2YWx1ZS5pbWdTcmMpO1xyXG5cclxuICAgIGlmICh2YWx1ZS5idXR0b24pIHtcclxuICAgICAgY2hpbGQuc2V0QnV0dG9uKHRydWUpO1xyXG4gICAgICBjaGlsZC5zZXRGdW5jdGlvbih2YWx1ZS5idXR0b24uZnVuY3Rpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh2YWx1ZS5yYXRpbmcpIHtcclxuICAgICAgY2hpbGQuc2V0UmF0aW5nKHRydWUpO1xyXG5cclxuICAgICAgaWYgKHZhbHVlLnJhdGluZy5yYXRpbmdJbWdTcmMpXHJcbiAgICAgICAgY2hpbGQuc2V0UmF0aW5nSW1nU3JjKHZhbHVlLnJhdGluZy5yYXRpbmdJbWdTcmMpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB0aGlzLmFkZENoaWxkKGNoaWxkKTtcclxuICB9O1xyXG5cclxuICByZW1vdmVDaGlsZCA9IChjaGlsZDogUm93SXRlbSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgdGhpcy5jaGlsZHJlbiA9IHRoaXMuY2hpbGRyZW4uZmlsdGVyKChjKSA9PiBjICE9PSBjaGlsZCk7XHJcbiAgICBjaGlsZC5zZXRQYXJlbnQodW5kZWZpbmVkKTtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcblxyXG4gIHNob3VsZEhhdmVDaGlsZHJlbiA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHJldHVybiB0cnVlO1xyXG4gIH07XHJcblxyXG4gIGdldENoaWxkSW5kZXggPSAoY2hpbGQ6IFJvd0l0ZW0pOiBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hpbGRyZW4uZmluZEluZGV4KChyKSA9PiByID09PSBjaGlsZCk7XHJcbiAgfTtcclxuXHJcbiAgLy8gZ2V0Q2hpbGRyZW4gPSAoKTogQ29sbGVjdGlvbltdID0+IHtcclxuICAvLyAgICAgLy8gdnJhY2FtbyBrb3BpanVcclxuICAvLyAgICAgLy8gcmV0dXJuIHRoaXMuY2hpbGRyZW4uc2xpY2UoKTtcclxuICAvLyAgICAgcmV0dXJuIHRoaXMuY2hpbGRyZW47XHJcbiAgLy8gfVxyXG5cclxuICAvLyBnZXRQYXJlbnQgPSAoKTogQ29sbGVjdGlvbiA9PiB7XHJcbiAgLy8gICAgIHJldHVybiB0aGlzLnBhcmVudDtcclxuICAvLyB9XHJcblxyXG4gIC8vIHNldFBhcmVudCA9IChwYXJlbnQ6IENvbGxlY3Rpb24pOiBib29sZWFuID0+IHtcclxuICAvLyAgICAgdGhpcy5wYXJlbnQgPSBwYXJlbnQ7XHJcbiAgLy8gICAgIHJldHVybiB0cnVlO1xyXG4gIC8vIH1cclxuXHJcbiAgY2xvbmUgPSAoKTogUm93ID0+IHtcclxuICAgIGxldCBjbG9uZSA9IG5ldyBSb3coKTtcclxuICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaCgoYzogUm93SXRlbSkgPT4ge1xyXG4gICAgICBjbG9uZS5hZGRDaGlsZChjLmNsb25lKCkpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gY2xvbmU7XHJcbiAgfTtcclxuXHJcbiAgcHJvdG90eXBlID0gKHJvdzogUm93KTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmNoaWxkcmVuLmZvckVhY2goKGM6IFJvd0l0ZW0pID0+IHtcclxuICAgICAgcm93LmFkZENoaWxkKGMuY2xvbmUoKSk7XHJcbiAgICB9KTtcclxuICB9O1xyXG59XHJcbiJdfQ==