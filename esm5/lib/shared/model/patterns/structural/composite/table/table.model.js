/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
// model
import { Collection } from "../collection.model";
import { Row } from "./row/row.model";
import { RowItem, } from "./row/row-item.model";
import { ContextMenu, } from "../context-menu/context-menu.model";
var Table = /** @class */ (function (_super) {
    tslib_1.__extends(Table, _super);
    // protected children: Collection[] = [];
    // protected parent: Collection = undefined;
    function Table(tableValue) {
        var _this = _super.call(this) || this;
        _this.sortedASC = false;
        _this.index = undefined;
        _this.contextMenu = undefined;
        _this.setContextMenuValue = (/**
         * @param {?} contextMenuValue
         * @return {?}
         */
        function (contextMenuValue) {
            if (contextMenuValue.contextMenuItems) {
                _this.contextMenu = new ContextMenu(contextMenuValue);
                return true;
            }
            return false;
        });
        _this.getContextMenu = (/**
         * @return {?}
         */
        function () {
            return _this.contextMenu;
        });
        _this.setHeader = (/**
         * @param {?} header
         * @return {?}
         */
        function (header) {
            _this.header = header;
            _this.header.setParent(_this);
            _this.header.getChildren().unshift(new RowItem("No."));
            return true;
        });
        _this.setHeaderValue = (/**
         * @param {?} headerValue
         * @return {?}
         */
        function (headerValue) {
            /** @type {?} */
            var header = new Row();
            if (_this.setHeader(header)) {
                headerValue.headerItems.forEach((/**
                 * @param {?} headerItemValue
                 * @return {?}
                 */
                function (headerItemValue) {
                    header.addChildValue(headerItemValue);
                }));
                return true;
            }
            return false;
        });
        _this.getHeader = (/**
         * @return {?}
         */
        function () {
            return _this.header;
        });
        _this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            _this.children.push(child);
            child.setParent(_this);
            child.getChildren().unshift(new RowItem(_this.children.length + "."));
            return true;
        });
        _this.addChildValue = (/**
         * @param {?} rowValue
         * @return {?}
         */
        function (rowValue) {
            /** @type {?} */
            var row = new Row();
            if (rowValue.color)
                row.color = rowValue.color;
            if (rowValue.dragPreviewTempRefSubject)
                row.dragPreviewTempRefSubject = rowValue.dragPreviewTempRefSubject;
            if (_this.addChild(row)) {
                row.data = rowValue.data;
                rowValue.rowItems.forEach((/**
                 * @param {?} rowItemValue
                 * @return {?}
                 */
                function (rowItemValue) {
                    row.addChildValue(rowItemValue);
                }));
                return true;
            }
            return false;
        });
        _this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            _this.children = _this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c !== child; }));
            child.setParent(undefined);
            return true;
        });
        _this.removeChildren = (/**
         * @return {?}
         */
        function () {
            _this.children.forEach((/**
             * @param {?} child
             * @return {?}
             */
            function (child) { return _this.removeChild(child); }));
            return true;
        });
        _this.getFilteredChildren = (/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var rows = [];
            _this.children.forEach((/**
             * @param {?} row
             * @return {?}
             */
            function (row) {
                if (row.show)
                    rows.push(row);
            }));
            return rows;
        });
        _this.shouldHaveChildren = (/**
         * @return {?}
         */
        function () {
            return true;
        });
        _this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return _this.children.findIndex((/**
             * @param {?} r
             * @return {?}
             */
            function (r) { return r === child; }));
        });
        _this.onSort = (/**
         * @param {?} index
         * @return {?}
         */
        function (index) {
            if (_this.index !== index) {
                _this.index = index;
                _this.sortedASC = false;
            }
            if (!_this.sortedASC)
                return _this.onASC(index);
            return _this.onDESC(index);
        });
        _this.onASC = (/**
         * @param {?} index
         * @return {?}
         */
        function (index) {
            _this.children.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) {
                if (((/** @type {?} */ (a.getChildren()[index]))).context <
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return -1;
                if (((/** @type {?} */ (a.getChildren()[index]))).context >
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return 1;
                return 0;
            }));
            _this.sortedASC = true;
        });
        _this.onDESC = (/**
         * @param {?} index
         * @return {?}
         */
        function (index) {
            _this.children.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) {
                if (((/** @type {?} */ (a.getChildren()[index]))).context >
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return -1;
                if (((/** @type {?} */ (a.getChildren()[index]))).context <
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return 1;
                return 0;
            }));
            _this.sortedASC = false;
        });
        _this.validate = (/**
         * @return {?}
         */
        function () {
            if (!_this.header)
                throw Error("Table must have header");
            /** @type {?} */
            var valid = true;
            _this.children.forEach((/**
             * @param {?} row
             * @return {?}
             */
            function (row) {
                if (row.getChildren().length !== _this.header.getChildren().length)
                    valid = false;
            }));
            return valid;
        });
        if (tableValue) {
            if (!tableValue.header) {
                throw Error("Header must exists");
            }
            else {
                _this.setHeaderValue(tableValue.header);
                if (tableValue.rows) {
                    tableValue.rows.forEach((/**
                     * @param {?} rowValue
                     * @return {?}
                     */
                    function (rowValue) {
                        _this.addChildValue(rowValue);
                    }));
                }
                if (tableValue.contextMenu)
                    _this.setContextMenuValue(tableValue.contextMenu);
            }
        }
        return _this;
    }
    return Table;
}(Collection));
export { Table };
if (false) {
    /**
     * @type {?}
     * @private
     */
    Table.prototype.header;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.sortedASC;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.index;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.contextMenu;
    /** @type {?} */
    Table.prototype.setContextMenuValue;
    /** @type {?} */
    Table.prototype.getContextMenu;
    /** @type {?} */
    Table.prototype.setHeader;
    /** @type {?} */
    Table.prototype.setHeaderValue;
    /** @type {?} */
    Table.prototype.getHeader;
    /** @type {?} */
    Table.prototype.addChild;
    /** @type {?} */
    Table.prototype.addChildValue;
    /** @type {?} */
    Table.prototype.removeChild;
    /** @type {?} */
    Table.prototype.removeChildren;
    /** @type {?} */
    Table.prototype.getFilteredChildren;
    /** @type {?} */
    Table.prototype.shouldHaveChildren;
    /** @type {?} */
    Table.prototype.getChildIndex;
    /** @type {?} */
    Table.prototype.onSort;
    /** @type {?} */
    Table.prototype.onASC;
    /** @type {?} */
    Table.prototype.onDESC;
    /** @type {?} */
    Table.prototype.validate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS90YWJsZS90YWJsZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakQsT0FBTyxFQUFFLEdBQUcsRUFBeUIsTUFBTSxpQkFBaUIsQ0FBQztBQUM3RCxPQUFPLEVBQ0wsT0FBTyxHQUdSLE1BQU0sc0JBQXNCLENBQUM7QUFDOUIsT0FBTyxFQUNMLFdBQVcsR0FFWixNQUFNLG9DQUFvQyxDQUFDO0FBUTVDO0lBQTJCLGlDQUFVO0lBS25DLHlDQUF5QztJQUN6Qyw0Q0FBNEM7SUFFNUMsZUFBWSxVQUF1QjtRQUFuQyxZQUNFLGlCQUFPLFNBa0JSO1FBekJPLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsV0FBSyxHQUFXLFNBQVMsQ0FBQztRQUMxQixpQkFBVyxHQUFnQixTQUFTLENBQUM7UUF5QjdDLHlCQUFtQjs7OztRQUFHLFVBQUMsZ0JBQWtDO1lBQ3ZELElBQUksZ0JBQWdCLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3JDLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFFckQsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDO1FBRUYsb0JBQWM7OztRQUFHO1lBQ2YsT0FBTyxLQUFJLENBQUMsV0FBVyxDQUFDO1FBQzFCLENBQUMsRUFBQztRQUVGLGVBQVM7Ozs7UUFBRyxVQUFDLE1BQVc7WUFDdEIsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFDckIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLENBQUM7WUFDNUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN0RCxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLG9CQUFjOzs7O1FBQUcsVUFBQyxXQUF3Qjs7Z0JBQ3BDLE1BQU0sR0FBRyxJQUFJLEdBQUcsRUFBRTtZQUN0QixJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQzFCLFdBQVcsQ0FBQyxXQUFXLENBQUMsT0FBTzs7OztnQkFBQyxVQUFDLGVBQW1DO29CQUNsRSxNQUFNLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN4QyxDQUFDLEVBQUMsQ0FBQztnQkFFSCxPQUFPLElBQUksQ0FBQzthQUNiO1lBRUQsT0FBTyxLQUFLLENBQUM7UUFDZixDQUFDLEVBQUM7UUFFRixlQUFTOzs7UUFBRztZQUNWLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQztRQUNyQixDQUFDLEVBQUM7UUFFRixjQUFROzs7O1FBQUcsVUFBQyxLQUFVO1lBQ3BCLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLENBQUM7WUFDdEIsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE9BQU8sQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsbUJBQWE7Ozs7UUFBRyxVQUFDLFFBQWtCOztnQkFDN0IsR0FBRyxHQUFHLElBQUksR0FBRyxFQUFFO1lBRW5CLElBQUksUUFBUSxDQUFDLEtBQUs7Z0JBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1lBQy9DLElBQUksUUFBUSxDQUFDLHlCQUF5QjtnQkFDcEMsR0FBRyxDQUFDLHlCQUF5QixHQUFHLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQztZQUVyRSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ3RCLEdBQUcsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFFekIsUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUMsWUFBMEI7b0JBQ25ELEdBQUcsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ2xDLENBQUMsRUFBQyxDQUFDO2dCQUVILE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNmLENBQUMsRUFBQztRQUVGLGlCQUFXOzs7O1FBQUcsVUFBQyxLQUFVO1lBQ3ZCLEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLEtBQUssS0FBSyxFQUFYLENBQVcsRUFBQyxDQUFDO1lBQ3pELEtBQUssQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDM0IsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUM7UUFFRixvQkFBYzs7O1FBQUc7WUFDZixLQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFDLEtBQVUsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCLEVBQUMsQ0FBQztZQUMvRCxPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsRUFBQztRQUVGLHlCQUFtQjs7O1FBQUc7O2dCQUNoQixJQUFJLEdBQUcsRUFBRTtZQUNiLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsR0FBUTtnQkFDN0IsSUFBSSxHQUFHLENBQUMsSUFBSTtvQkFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9CLENBQUMsRUFBQyxDQUFDO1lBQ0gsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLEVBQUM7UUFFRix3QkFBa0I7OztRQUFHO1lBQ25CLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxFQUFDO1FBRUYsbUJBQWE7Ozs7UUFBRyxVQUFDLEtBQVU7WUFDekIsT0FBTyxLQUFJLENBQUMsUUFBUSxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsS0FBSyxLQUFLLEVBQVgsQ0FBVyxFQUFDLENBQUM7UUFDckQsQ0FBQyxFQUFDO1FBRUYsWUFBTTs7OztRQUFHLFVBQUMsS0FBYTtZQUNyQixJQUFJLEtBQUksQ0FBQyxLQUFLLEtBQUssS0FBSyxFQUFFO2dCQUN4QixLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDbkIsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDeEI7WUFFRCxJQUFJLENBQUMsS0FBSSxDQUFDLFNBQVM7Z0JBQUUsT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzlDLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUM7UUFFRixXQUFLOzs7O1FBQUcsVUFBQyxLQUFhO1lBQ3BCLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSTs7Ozs7WUFBQyxVQUFDLENBQU0sRUFBRSxDQUFNO2dCQUNoQyxJQUNFLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUN6QyxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBQSxDQUFDLENBQUMsT0FBTztvQkFFekMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDWixJQUNFLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUN6QyxDQUFDLG1CQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBQSxDQUFDLENBQUMsT0FBTztvQkFFekMsT0FBTyxDQUFDLENBQUM7Z0JBQ1gsT0FBTyxDQUFDLENBQUM7WUFDWCxDQUFDLEVBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLENBQUMsRUFBQztRQUVGLFlBQU07Ozs7UUFBRyxVQUFDLEtBQWE7WUFDckIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJOzs7OztZQUFDLFVBQUMsQ0FBTSxFQUFFLENBQU07Z0JBQ2hDLElBQ0UsQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUEsQ0FBQyxDQUFDLE9BQU87b0JBQ3pDLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUV6QyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUNaLElBQ0UsQ0FBQyxtQkFBUyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUEsQ0FBQyxDQUFDLE9BQU87b0JBQ3pDLENBQUMsbUJBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFBLENBQUMsQ0FBQyxPQUFPO29CQUV6QyxPQUFPLENBQUMsQ0FBQztnQkFDWCxPQUFPLENBQUMsQ0FBQztZQUNYLENBQUMsRUFBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDekIsQ0FBQyxFQUFDO1FBRUYsY0FBUTs7O1FBQUc7WUFDVCxJQUFJLENBQUMsS0FBSSxDQUFDLE1BQU07Z0JBQUUsTUFBTSxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQzs7Z0JBRXBELEtBQUssR0FBRyxJQUFJO1lBQ2hCLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsR0FBRztnQkFDeEIsSUFBSSxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxLQUFLLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTTtvQkFDL0QsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNsQixDQUFDLEVBQUMsQ0FBQztZQUNILE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDO1FBcEtBLElBQUksVUFBVSxFQUFFO1lBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3RCLE1BQU0sS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRXZDLElBQUksVUFBVSxDQUFDLElBQUksRUFBRTtvQkFDbkIsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPOzs7O29CQUFDLFVBQUMsUUFBa0I7d0JBQ3pDLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQy9CLENBQUMsRUFBQyxDQUFDO2lCQUNKO2dCQUVELElBQUksVUFBVSxDQUFDLFdBQVc7b0JBQ3hCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDcEQ7U0FDRjs7SUFDSCxDQUFDO0lBb0tILFlBQUM7QUFBRCxDQUFDLEFBL0xELENBQTJCLFVBQVUsR0ErTHBDOzs7Ozs7O0lBOUxDLHVCQUFvQjs7Ozs7SUFDcEIsMEJBQW1DOzs7OztJQUNuQyxzQkFBa0M7Ozs7O0lBQ2xDLDRCQUE2Qzs7SUF5QjdDLG9DQU9FOztJQUVGLCtCQUVFOztJQUVGLDBCQUtFOztJQUVGLCtCQVdFOztJQUVGLDBCQUVFOztJQUVGLHlCQUtFOztJQUVGLDhCQWtCRTs7SUFFRiw0QkFJRTs7SUFFRiwrQkFHRTs7SUFFRixvQ0FNRTs7SUFFRixtQ0FFRTs7SUFFRiw4QkFFRTs7SUFFRix1QkFRRTs7SUFFRixzQkFnQkU7O0lBRUYsdUJBZ0JFOztJQUVGLHlCQVNFIiwic291cmNlc0NvbnRlbnQiOlsiLy8gbW9kZWxcclxuaW1wb3J0IHsgQ29sbGVjdGlvbiB9IGZyb20gXCIuLi9jb2xsZWN0aW9uLm1vZGVsXCI7XHJcbmltcG9ydCB7IFJvdywgUm93VmFsdWUsIEhlYWRlclZhbHVlIH0gZnJvbSBcIi4vcm93L3Jvdy5tb2RlbFwiO1xyXG5pbXBvcnQge1xyXG4gIFJvd0l0ZW0sXHJcbiAgUm93SXRlbVZhbHVlLFxyXG4gIFJvd0l0ZW1IZWFkZXJWYWx1ZSxcclxufSBmcm9tIFwiLi9yb3cvcm93LWl0ZW0ubW9kZWxcIjtcclxuaW1wb3J0IHtcclxuICBDb250ZXh0TWVudSxcclxuICBDb250ZXh0TWVudVZhbHVlLFxyXG59IGZyb20gXCIuLi9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51Lm1vZGVsXCI7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgVGFibGVWYWx1ZSB7XHJcbiAgaGVhZGVyOiBIZWFkZXJWYWx1ZTtcclxuICByb3dzPzogUm93VmFsdWVbXTtcclxuICBjb250ZXh0TWVudT86IENvbnRleHRNZW51VmFsdWU7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBUYWJsZSBleHRlbmRzIENvbGxlY3Rpb24ge1xyXG4gIHByaXZhdGUgaGVhZGVyOiBSb3c7XHJcbiAgcHJpdmF0ZSBzb3J0ZWRBU0M6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIGluZGV4OiBudW1iZXIgPSB1bmRlZmluZWQ7XHJcbiAgcHJpdmF0ZSBjb250ZXh0TWVudTogQ29udGV4dE1lbnUgPSB1bmRlZmluZWQ7XHJcbiAgLy8gcHJvdGVjdGVkIGNoaWxkcmVuOiBDb2xsZWN0aW9uW10gPSBbXTtcclxuICAvLyBwcm90ZWN0ZWQgcGFyZW50OiBDb2xsZWN0aW9uID0gdW5kZWZpbmVkO1xyXG5cclxuICBjb25zdHJ1Y3Rvcih0YWJsZVZhbHVlPzogVGFibGVWYWx1ZSkge1xyXG4gICAgc3VwZXIoKTtcclxuXHJcbiAgICBpZiAodGFibGVWYWx1ZSkge1xyXG4gICAgICBpZiAoIXRhYmxlVmFsdWUuaGVhZGVyKSB7XHJcbiAgICAgICAgdGhyb3cgRXJyb3IoXCJIZWFkZXIgbXVzdCBleGlzdHNcIik7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZXRIZWFkZXJWYWx1ZSh0YWJsZVZhbHVlLmhlYWRlcik7XHJcblxyXG4gICAgICAgIGlmICh0YWJsZVZhbHVlLnJvd3MpIHtcclxuICAgICAgICAgIHRhYmxlVmFsdWUucm93cy5mb3JFYWNoKChyb3dWYWx1ZTogUm93VmFsdWUpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hZGRDaGlsZFZhbHVlKHJvd1ZhbHVlKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRhYmxlVmFsdWUuY29udGV4dE1lbnUpXHJcbiAgICAgICAgICB0aGlzLnNldENvbnRleHRNZW51VmFsdWUodGFibGVWYWx1ZS5jb250ZXh0TWVudSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHNldENvbnRleHRNZW51VmFsdWUgPSAoY29udGV4dE1lbnVWYWx1ZTogQ29udGV4dE1lbnVWYWx1ZSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgaWYgKGNvbnRleHRNZW51VmFsdWUuY29udGV4dE1lbnVJdGVtcykge1xyXG4gICAgICB0aGlzLmNvbnRleHRNZW51ID0gbmV3IENvbnRleHRNZW51KGNvbnRleHRNZW51VmFsdWUpO1xyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfTtcclxuXHJcbiAgZ2V0Q29udGV4dE1lbnUgPSAoKTogQ29udGV4dE1lbnUgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY29udGV4dE1lbnU7XHJcbiAgfTtcclxuXHJcbiAgc2V0SGVhZGVyID0gKGhlYWRlcjogUm93KTogYm9vbGVhbiA9PiB7XHJcbiAgICB0aGlzLmhlYWRlciA9IGhlYWRlcjtcclxuICAgIHRoaXMuaGVhZGVyLnNldFBhcmVudCh0aGlzKTtcclxuICAgIHRoaXMuaGVhZGVyLmdldENoaWxkcmVuKCkudW5zaGlmdChuZXcgUm93SXRlbShcIk5vLlwiKSk7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBzZXRIZWFkZXJWYWx1ZSA9IChoZWFkZXJWYWx1ZTogSGVhZGVyVmFsdWUpOiBib29sZWFuID0+IHtcclxuICAgIGxldCBoZWFkZXIgPSBuZXcgUm93KCk7XHJcbiAgICBpZiAodGhpcy5zZXRIZWFkZXIoaGVhZGVyKSkge1xyXG4gICAgICBoZWFkZXJWYWx1ZS5oZWFkZXJJdGVtcy5mb3JFYWNoKChoZWFkZXJJdGVtVmFsdWU6IFJvd0l0ZW1IZWFkZXJWYWx1ZSkgPT4ge1xyXG4gICAgICAgIGhlYWRlci5hZGRDaGlsZFZhbHVlKGhlYWRlckl0ZW1WYWx1ZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH07XHJcblxyXG4gIGdldEhlYWRlciA9ICgpOiBSb3cgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuaGVhZGVyO1xyXG4gIH07XHJcblxyXG4gIGFkZENoaWxkID0gKGNoaWxkOiBSb3cpOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4ucHVzaChjaGlsZCk7XHJcbiAgICBjaGlsZC5zZXRQYXJlbnQodGhpcyk7XHJcbiAgICBjaGlsZC5nZXRDaGlsZHJlbigpLnVuc2hpZnQobmV3IFJvd0l0ZW0odGhpcy5jaGlsZHJlbi5sZW5ndGggKyBcIi5cIikpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgYWRkQ2hpbGRWYWx1ZSA9IChyb3dWYWx1ZTogUm93VmFsdWUpOiBib29sZWFuID0+IHtcclxuICAgIGxldCByb3cgPSBuZXcgUm93KCk7XHJcblxyXG4gICAgaWYgKHJvd1ZhbHVlLmNvbG9yKSByb3cuY29sb3IgPSByb3dWYWx1ZS5jb2xvcjtcclxuICAgIGlmIChyb3dWYWx1ZS5kcmFnUHJldmlld1RlbXBSZWZTdWJqZWN0KVxyXG4gICAgICByb3cuZHJhZ1ByZXZpZXdUZW1wUmVmU3ViamVjdCA9IHJvd1ZhbHVlLmRyYWdQcmV2aWV3VGVtcFJlZlN1YmplY3Q7XHJcblxyXG4gICAgaWYgKHRoaXMuYWRkQ2hpbGQocm93KSkge1xyXG4gICAgICByb3cuZGF0YSA9IHJvd1ZhbHVlLmRhdGE7XHJcblxyXG4gICAgICByb3dWYWx1ZS5yb3dJdGVtcy5mb3JFYWNoKChyb3dJdGVtVmFsdWU6IFJvd0l0ZW1WYWx1ZSkgPT4ge1xyXG4gICAgICAgIHJvdy5hZGRDaGlsZFZhbHVlKHJvd0l0ZW1WYWx1ZSk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH07XHJcblxyXG4gIHJlbW92ZUNoaWxkID0gKGNoaWxkOiBSb3cpOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4gPSB0aGlzLmNoaWxkcmVuLmZpbHRlcigoYykgPT4gYyAhPT0gY2hpbGQpO1xyXG4gICAgY2hpbGQuc2V0UGFyZW50KHVuZGVmaW5lZCk7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICByZW1vdmVDaGlsZHJlbiA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaCgoY2hpbGQ6IFJvdykgPT4gdGhpcy5yZW1vdmVDaGlsZChjaGlsZCkpO1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfTtcclxuXHJcbiAgZ2V0RmlsdGVyZWRDaGlsZHJlbiA9ICgpOiBSb3dbXSA9PiB7XHJcbiAgICBsZXQgcm93cyA9IFtdO1xyXG4gICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKChyb3c6IFJvdykgPT4ge1xyXG4gICAgICBpZiAocm93LnNob3cpIHJvd3MucHVzaChyb3cpO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gcm93cztcclxuICB9O1xyXG5cclxuICBzaG91bGRIYXZlQ2hpbGRyZW4gPSAoKTogYm9vbGVhbiA9PiB7XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9O1xyXG5cclxuICBnZXRDaGlsZEluZGV4ID0gKGNoaWxkOiBSb3cpOiBudW1iZXIgPT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuY2hpbGRyZW4uZmluZEluZGV4KChyKSA9PiByID09PSBjaGlsZCk7XHJcbiAgfTtcclxuXHJcbiAgb25Tb3J0ID0gKGluZGV4OiBudW1iZXIpOiB2b2lkID0+IHtcclxuICAgIGlmICh0aGlzLmluZGV4ICE9PSBpbmRleCkge1xyXG4gICAgICB0aGlzLmluZGV4ID0gaW5kZXg7XHJcbiAgICAgIHRoaXMuc29ydGVkQVNDID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCF0aGlzLnNvcnRlZEFTQykgcmV0dXJuIHRoaXMub25BU0MoaW5kZXgpO1xyXG4gICAgcmV0dXJuIHRoaXMub25ERVNDKGluZGV4KTtcclxuICB9O1xyXG5cclxuICBvbkFTQyA9IChpbmRleDogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmNoaWxkcmVuLnNvcnQoKGE6IFJvdywgYjogUm93KSA9PiB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICAoPFJvd0l0ZW0+YS5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dCA8XHJcbiAgICAgICAgKDxSb3dJdGVtPmIuZ2V0Q2hpbGRyZW4oKVtpbmRleF0pLmNvbnRleHRcclxuICAgICAgKVxyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgICg8Um93SXRlbT5hLmdldENoaWxkcmVuKClbaW5kZXhdKS5jb250ZXh0ID5cclxuICAgICAgICAoPFJvd0l0ZW0+Yi5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dFxyXG4gICAgICApXHJcbiAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgIHJldHVybiAwO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5zb3J0ZWRBU0MgPSB0cnVlO1xyXG4gIH07XHJcblxyXG4gIG9uREVTQyA9IChpbmRleDogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICB0aGlzLmNoaWxkcmVuLnNvcnQoKGE6IFJvdywgYjogUm93KSA9PiB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICAoPFJvd0l0ZW0+YS5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dCA+XHJcbiAgICAgICAgKDxSb3dJdGVtPmIuZ2V0Q2hpbGRyZW4oKVtpbmRleF0pLmNvbnRleHRcclxuICAgICAgKVxyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgICg8Um93SXRlbT5hLmdldENoaWxkcmVuKClbaW5kZXhdKS5jb250ZXh0IDxcclxuICAgICAgICAoPFJvd0l0ZW0+Yi5nZXRDaGlsZHJlbigpW2luZGV4XSkuY29udGV4dFxyXG4gICAgICApXHJcbiAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgIHJldHVybiAwO1xyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5zb3J0ZWRBU0MgPSBmYWxzZTtcclxuICB9O1xyXG5cclxuICB2YWxpZGF0ZSA9ICgpOiBib29sZWFuID0+IHtcclxuICAgIGlmICghdGhpcy5oZWFkZXIpIHRocm93IEVycm9yKFwiVGFibGUgbXVzdCBoYXZlIGhlYWRlclwiKTtcclxuXHJcbiAgICBsZXQgdmFsaWQgPSB0cnVlO1xyXG4gICAgdGhpcy5jaGlsZHJlbi5mb3JFYWNoKChyb3cpID0+IHtcclxuICAgICAgaWYgKHJvdy5nZXRDaGlsZHJlbigpLmxlbmd0aCAhPT0gdGhpcy5oZWFkZXIuZ2V0Q2hpbGRyZW4oKS5sZW5ndGgpXHJcbiAgICAgICAgdmFsaWQgPSBmYWxzZTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIHZhbGlkO1xyXG4gIH07XHJcblxyXG4gIC8vIGdldENoaWxkcmVuID0gKCk6IENvbGxlY3Rpb25bXSA9PiB7XHJcbiAgLy8gICAgIC8vIHZyYWNhbW8ga29waWp1XHJcbiAgLy8gICAgIC8vIHJldHVybiB0aGlzLmNoaWxkcmVuLnNsaWNlKCk7XHJcbiAgLy8gICAgIHJldHVybiB0aGlzLmNoaWxkcmVuO1xyXG4gIC8vIH1cclxuXHJcbiAgLy8gZ2V0UGFyZW50ID0gKCk6IENvbGxlY3Rpb24gPT4ge1xyXG4gIC8vICAgICByZXR1cm4gdGhpcy5wYXJlbnQ7XHJcbiAgLy8gfVxyXG5cclxuICAvLyBzZXRQYXJlbnQgPSAocGFyZW50OiBDb2xsZWN0aW9uKTogYm9vbGVhbiA9PiB7XHJcbiAgLy8gICAgIHRoaXMucGFyZW50ID0gcGFyZW50O1xyXG4gIC8vICAgICByZXR1cm4gdHJ1ZTtcclxuICAvLyB9XHJcbn1cclxuIl19