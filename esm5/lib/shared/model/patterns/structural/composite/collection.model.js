/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
Collection = /** @class */ (function () {
    function Collection() {
        var _this = this;
        this.children = [];
        this.parent = undefined;
        this.getChildren = (/**
         * @return {?}
         */
        function () {
            // vracamo kopiju
            // return this.children.slice();
            return _this.children;
        });
        this.getParent = (/**
         * @return {?}
         */
        function () {
            return _this.parent;
        });
        this.setParent = (/**
         * @param {?} parent
         * @return {?}
         */
        function (parent) {
            _this.parent = parent;
            return true;
        });
    }
    return Collection;
}());
/**
 * @abstract
 */
export { Collection };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    Collection.prototype.children;
    /**
     * @type {?}
     * @protected
     */
    Collection.prototype.parent;
    /** @type {?} */
    Collection.prototype.getChildren;
    /** @type {?} */
    Collection.prototype.getParent;
    /** @type {?} */
    Collection.prototype.setParent;
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.addChild = function (child) { };
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.removeChild = function (child) { };
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.getChildIndex = function (child) { };
    /**
     * @abstract
     * @return {?}
     */
    Collection.prototype.shouldHaveChildren = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL21vZGVsL3BhdHRlcm5zL3N0cnVjdHVyYWwvY29tcG9zaXRlL2NvbGxlY3Rpb24ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0lBQUE7UUFBQSxpQkEwQkM7UUF6QmEsYUFBUSxHQUFpQixFQUFFLENBQUM7UUFDNUIsV0FBTSxHQUFlLFNBQVMsQ0FBQztRQVV6QyxnQkFBVzs7O1FBQUc7WUFDVixpQkFBaUI7WUFDakIsZ0NBQWdDO1lBQ2hDLE9BQU8sS0FBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDLEVBQUE7UUFFRCxjQUFTOzs7UUFBRztZQUNSLE9BQU8sS0FBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDLEVBQUE7UUFFRCxjQUFTOzs7O1FBQUcsVUFBQyxNQUFrQjtZQUMzQixLQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztZQUNyQixPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDLEVBQUE7SUFDTCxDQUFDO0lBQUQsaUJBQUM7QUFBRCxDQUFDLEFBMUJELElBMEJDOzs7Ozs7Ozs7O0lBekJHLDhCQUFzQzs7Ozs7SUFDdEMsNEJBQXlDOztJQVV6QyxpQ0FJQzs7SUFFRCwrQkFFQzs7SUFFRCwrQkFHQzs7Ozs7O0lBckJELHFEQUE4Qzs7Ozs7O0lBRTlDLHdEQUFpRDs7Ozs7O0lBRWpELDBEQUFrRDs7Ozs7SUFFbEQsMERBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGFic3RyYWN0IGNsYXNzIENvbGxlY3Rpb24ge1xyXG4gICAgcHJvdGVjdGVkIGNoaWxkcmVuOiBDb2xsZWN0aW9uW10gPSBbXTtcclxuICAgIHByb3RlY3RlZCBwYXJlbnQ6IENvbGxlY3Rpb24gPSB1bmRlZmluZWQ7XHJcblxyXG4gICAgYWJzdHJhY3QgYWRkQ2hpbGQoY2hpbGQ6IENvbGxlY3Rpb24pOiBib29sZWFuO1xyXG5cclxuICAgIGFic3RyYWN0IHJlbW92ZUNoaWxkKGNoaWxkOiBDb2xsZWN0aW9uKTogYm9vbGVhbjtcclxuXHJcbiAgICBhYnN0cmFjdCBnZXRDaGlsZEluZGV4KGNoaWxkOiBDb2xsZWN0aW9uKTogbnVtYmVyO1xyXG5cclxuICAgIGFic3RyYWN0IHNob3VsZEhhdmVDaGlsZHJlbigpOiBib29sZWFuO1xyXG5cclxuICAgIGdldENoaWxkcmVuID0gKCk6IENvbGxlY3Rpb25bXSA9PiB7XHJcbiAgICAgICAgLy8gdnJhY2FtbyBrb3BpanVcclxuICAgICAgICAvLyByZXR1cm4gdGhpcy5jaGlsZHJlbi5zbGljZSgpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNoaWxkcmVuO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFBhcmVudCA9ICgpOiBDb2xsZWN0aW9uID0+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYXJlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0UGFyZW50ID0gKHBhcmVudDogQ29sbGVjdGlvbik6IGJvb2xlYW4gPT4ge1xyXG4gICAgICAgIHRoaXMucGFyZW50ID0gcGFyZW50O1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59Il19