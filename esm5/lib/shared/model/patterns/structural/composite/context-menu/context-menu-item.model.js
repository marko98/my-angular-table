/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
// model
import { Collection } from '../collection.model';
var ContextMenuItem = /** @class */ (function (_super) {
    tslib_1.__extends(ContextMenuItem, _super);
    function ContextMenuItem(context, f, imgSrc) {
        var _this = _super.call(this) || this;
        _this.context = context;
        _this.f = f;
        _this.imgSrc = imgSrc;
        return _this;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    ContextMenuItem.prototype.addChild = /**
     * @param {?} child
     * @return {?}
     */
    function (child) {
        return true;
    };
    /**
     * @param {?} child
     * @return {?}
     */
    ContextMenuItem.prototype.removeChild = /**
     * @param {?} child
     * @return {?}
     */
    function (child) {
        return true;
    };
    /**
     * @param {?} child
     * @return {?}
     */
    ContextMenuItem.prototype.getChildIndex = /**
     * @param {?} child
     * @return {?}
     */
    function (child) {
        return -1;
    };
    /**
     * @return {?}
     */
    ContextMenuItem.prototype.shouldHaveChildren = /**
     * @return {?}
     */
    function () {
        return false;
    };
    return ContextMenuItem;
}(Collection));
export { ContextMenuItem };
if (false) {
    /** @type {?} */
    ContextMenuItem.prototype.context;
    /** @type {?} */
    ContextMenuItem.prototype.f;
    /** @type {?} */
    ContextMenuItem.prototype.imgSrc;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LWl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51LWl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBUWpEO0lBQXFDLDJDQUFVO0lBRTNDLHlCQUFtQixPQUFlLEVBQVMsQ0FBVyxFQUFTLE1BQWU7UUFBOUUsWUFDSSxpQkFBTyxTQUNWO1FBRmtCLGFBQU8sR0FBUCxPQUFPLENBQVE7UUFBUyxPQUFDLEdBQUQsQ0FBQyxDQUFVO1FBQVMsWUFBTSxHQUFOLE1BQU0sQ0FBUzs7SUFFOUUsQ0FBQzs7Ozs7SUFFRCxrQ0FBUTs7OztJQUFSLFVBQVMsS0FBaUI7UUFDdEIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFRCxxQ0FBVzs7OztJQUFYLFVBQVksS0FBaUI7UUFDekIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFFRCx1Q0FBYTs7OztJQUFiLFVBQWMsS0FBaUI7UUFDM0IsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUNkLENBQUM7Ozs7SUFFRCw0Q0FBa0I7OztJQUFsQjtRQUNJLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFTCxzQkFBQztBQUFELENBQUMsQUF0QkQsQ0FBcUMsVUFBVSxHQXNCOUM7Ozs7SUFwQmUsa0NBQXNCOztJQUFFLDRCQUFrQjs7SUFBRSxpQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vY29sbGVjdGlvbi5tb2RlbCc7XHJcblxyXG5leHBvcnQgZGVjbGFyZSBpbnRlcmZhY2UgQ29udGV4dE1lbnVJdGVtVmFsdWUge1xyXG4gICAgY29udGV4dDogc3RyaW5nLFxyXG4gICAgZnVuY3Rpb246IEZ1bmN0aW9uLFxyXG4gICAgaW1nU3JjPzogc3RyaW5nXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudUl0ZW0gZXh0ZW5kcyBDb2xsZWN0aW9uIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgY29udGV4dDogc3RyaW5nLCBwdWJsaWMgZjogRnVuY3Rpb24sIHB1YmxpYyBpbWdTcmM/OiBzdHJpbmcpe1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQ2hpbGQoY2hpbGQ6IENvbGxlY3Rpb24pOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVDaGlsZChjaGlsZDogQ29sbGVjdGlvbik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENoaWxkSW5kZXgoY2hpbGQ6IENvbGxlY3Rpb24pOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiAtMTtcclxuICAgIH1cclxuXHJcbiAgICBzaG91bGRIYXZlQ2hpbGRyZW4oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgXHJcbn0iXX0=