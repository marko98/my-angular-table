/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
// model
import { Collection } from '../collection.model';
import { ContextMenuItem } from './context-menu-item.model';
var ContextMenu = /** @class */ (function (_super) {
    tslib_1.__extends(ContextMenu, _super);
    function ContextMenu(contextMenuValue) {
        var _this = _super.call(this) || this;
        _this.contextMenuValue = contextMenuValue;
        _this.addChildValue = (/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value.context && value.function) {
                /** @type {?} */
                var i = new ContextMenuItem(value.context, value.function, value.imgSrc);
                _this.addChild(i);
                return true;
            }
            return false;
        });
        contextMenuValue.contextMenuItems.forEach((/**
         * @param {?} i
         * @return {?}
         */
        function (i) {
            _this.addChildValue(i);
        }));
        return _this;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    ContextMenu.prototype.addChild = /**
     * @param {?} child
     * @return {?}
     */
    function (child) {
        this.children.push(child);
        child.setParent(this);
        return true;
    };
    /**
     * @param {?} child
     * @return {?}
     */
    ContextMenu.prototype.removeChild = /**
     * @param {?} child
     * @return {?}
     */
    function (child) {
        this.children = this.children.filter((/**
         * @param {?} c
         * @return {?}
         */
        function (c) { return c !== child; }));
        child.setParent(undefined);
        return true;
    };
    /**
     * @param {?} child
     * @return {?}
     */
    ContextMenu.prototype.getChildIndex = /**
     * @param {?} child
     * @return {?}
     */
    function (child) {
        return this.children.findIndex((/**
         * @param {?} c
         * @return {?}
         */
        function (c) { return c === child; }));
    };
    /**
     * @return {?}
     */
    ContextMenu.prototype.shouldHaveChildren = /**
     * @return {?}
     */
    function () {
        return true;
    };
    return ContextMenu;
}(Collection));
export { ContextMenu };
if (false) {
    /** @type {?} */
    ContextMenu.prototype.addChildValue;
    /**
     * @type {?}
     * @private
     */
    ContextMenu.prototype.contextMenuValue;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktYW5ndWxhci10YWJsZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvbW9kZWwvcGF0dGVybnMvc3RydWN0dXJhbC9jb21wb3NpdGUvY29udGV4dC1tZW51L2NvbnRleHQtbWVudS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakQsT0FBTyxFQUF3QixlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQU1sRjtJQUFpQyx1Q0FBVTtJQUV2QyxxQkFBb0IsZ0JBQWtDO1FBQXRELFlBQ0ksaUJBQU8sU0FJVjtRQUxtQixzQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBYXRELG1CQUFhOzs7O1FBQUcsVUFBQyxLQUEyQjtZQUN4QyxJQUFHLEtBQUssQ0FBQyxPQUFPLElBQUksS0FBSyxDQUFDLFFBQVEsRUFBQzs7b0JBRTNCLENBQUMsR0FBRyxJQUFJLGVBQWUsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLE1BQU0sQ0FBQztnQkFDeEUsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFFakIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsRUFBQTtRQXBCRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxDQUF1QjtZQUM5RCxLQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDOztJQUNQLENBQUM7Ozs7O0lBRUQsOEJBQVE7Ozs7SUFBUixVQUFTLEtBQXNCO1FBQzNCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEIsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFhRCxpQ0FBVzs7OztJQUFYLFVBQVksS0FBc0I7UUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsS0FBSyxLQUFLLEVBQVgsQ0FBVyxFQUFDLENBQUM7UUFDdkQsS0FBSyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzQixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELG1DQUFhOzs7O0lBQWIsVUFBYyxLQUFzQjtRQUNoQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLEtBQUssRUFBWCxDQUFXLEVBQUMsQ0FBQztJQUNyRCxDQUFDOzs7O0lBRUQsd0NBQWtCOzs7SUFBbEI7UUFDSSxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUwsa0JBQUM7QUFBRCxDQUFDLEFBeENELENBQWlDLFVBQVUsR0F3QzFDOzs7O0lBekJHLG9DQVNDOzs7OztJQXRCVyx1Q0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBtb2RlbFxyXG5pbXBvcnQgeyBDb2xsZWN0aW9uIH0gZnJvbSAnLi4vY29sbGVjdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IENvbnRleHRNZW51SXRlbVZhbHVlLCBDb250ZXh0TWVudUl0ZW0gfSBmcm9tICcuL2NvbnRleHQtbWVudS1pdGVtLm1vZGVsJztcclxuXHJcbmV4cG9ydCBkZWNsYXJlIGludGVyZmFjZSBDb250ZXh0TWVudVZhbHVlIHtcclxuICAgIGNvbnRleHRNZW51SXRlbXM6IENvbnRleHRNZW51SXRlbVZhbHVlW11cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIENvbnRleHRNZW51IGV4dGVuZHMgQ29sbGVjdGlvbiB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb250ZXh0TWVudVZhbHVlOiBDb250ZXh0TWVudVZhbHVlKXtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIGNvbnRleHRNZW51VmFsdWUuY29udGV4dE1lbnVJdGVtcy5mb3JFYWNoKChpOiBDb250ZXh0TWVudUl0ZW1WYWx1ZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFkZENoaWxkVmFsdWUoaSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQ2hpbGQoY2hpbGQ6IENvbnRleHRNZW51SXRlbSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHRoaXMuY2hpbGRyZW4ucHVzaChjaGlsZCk7XHJcbiAgICAgICAgY2hpbGQuc2V0UGFyZW50KHRoaXMpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZENoaWxkVmFsdWUgPSAodmFsdWU6IENvbnRleHRNZW51SXRlbVZhbHVlKTogYm9vbGVhbiA9PiB7XHJcbiAgICAgICAgaWYodmFsdWUuY29udGV4dCAmJiB2YWx1ZS5mdW5jdGlvbil7XHJcblxyXG4gICAgICAgICAgICBsZXQgaSA9IG5ldyBDb250ZXh0TWVudUl0ZW0odmFsdWUuY29udGV4dCwgdmFsdWUuZnVuY3Rpb24sIHZhbHVlLmltZ1NyYyk7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkQ2hpbGQoaSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZUNoaWxkKGNoaWxkOiBDb250ZXh0TWVudUl0ZW0pOiBib29sZWFuIHtcclxuICAgICAgICB0aGlzLmNoaWxkcmVuID0gdGhpcy5jaGlsZHJlbi5maWx0ZXIoYyA9PiBjICE9PSBjaGlsZCk7XHJcbiAgICAgICAgY2hpbGQuc2V0UGFyZW50KHVuZGVmaW5lZCk7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2hpbGRJbmRleChjaGlsZDogQ29udGV4dE1lbnVJdGVtKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jaGlsZHJlbi5maW5kSW5kZXgoYyA9PiBjID09PSBjaGlsZCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdWxkSGF2ZUNoaWxkcmVuKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxufSJdfQ==