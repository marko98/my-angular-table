/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
var PaginationPipe = /** @class */ (function () {
    function PaginationPipe() {
    }
    /**
     * @param {?} value
     * @param {?} data
     * @return {?}
     */
    PaginationPipe.prototype.transform = /**
     * @param {?} value
     * @param {?} data
     * @return {?}
     */
    function (value, data) {
        // console.log(data);
        return value.slice(data.page * data.perPage, data.page * data.perPage + data.perPage);
    };
    PaginationPipe.decorators = [
        { type: Pipe, args: [{ name: 'pagination' },] }
    ];
    return PaginationPipe;
}());
export { PaginationPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyLXBhZ2UucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3BpcGVzL3Blci1wYWdlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxJQUFJLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBRXBEO0lBQUE7SUFRQSxDQUFDOzs7Ozs7SUFOQyxrQ0FBUzs7Ozs7SUFBVCxVQUFVLEtBQVksRUFBRSxJQUFxQztRQUUzRCxxQkFBcUI7UUFFckIsT0FBTyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxPQUFPLEdBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2xGLENBQUM7O2dCQVBGLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxZQUFZLEVBQUM7O0lBUTFCLHFCQUFDO0NBQUEsQUFSRCxJQVFDO1NBUFksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtuYW1lOiAncGFnaW5hdGlvbid9KVxyXG5leHBvcnQgY2xhc3MgUGFnaW5hdGlvblBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICB0cmFuc2Zvcm0odmFsdWU6IGFueVtdLCBkYXRhOiB7cGFnZTogbnVtYmVyLCBwZXJQYWdlOiBudW1iZXJ9KTogYW55W10ge1xyXG5cclxuICAgIC8vIGNvbnNvbGUubG9nKGRhdGEpO1xyXG5cclxuICAgIHJldHVybiB2YWx1ZS5zbGljZShkYXRhLnBhZ2UqZGF0YS5wZXJQYWdlLCBkYXRhLnBhZ2UqZGF0YS5wZXJQYWdlK2RhdGEucGVyUGFnZSk7XHJcbiAgfVxyXG59Il19