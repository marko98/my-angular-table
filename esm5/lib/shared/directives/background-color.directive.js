/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Input, } from "@angular/core";
var LibElColorDirective = /** @class */ (function () {
    function LibElColorDirective(elRef) {
        this.elRef = elRef;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    LibElColorDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.libElColorValue.currentValue)
            ((/** @type {?} */ (this.elRef.nativeElement))).style.backgroundColor =
                changes.libElColorValue.currentValue;
    };
    LibElColorDirective.decorators = [
        { type: Directive, args: [{
                    selector: "[libElColor]",
                },] }
    ];
    /** @nocollapse */
    LibElColorDirective.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    LibElColorDirective.propDecorators = {
        libElColorValue: [{ type: Input }]
    };
    return LibElColorDirective;
}());
export { LibElColorDirective };
if (false) {
    /** @type {?} */
    LibElColorDirective.prototype.libElColorValue;
    /**
     * @type {?}
     * @private
     */
    LibElColorDirective.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFja2dyb3VuZC1jb2xvci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1hbmd1bGFyLXRhYmxlLyIsInNvdXJjZXMiOlsibGliL3NoYXJlZC9kaXJlY3RpdmVzL2JhY2tncm91bmQtY29sb3IuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEdBR04sTUFBTSxlQUFlLENBQUM7QUFFdkI7SUFNRSw2QkFBb0IsS0FBaUI7UUFBakIsVUFBSyxHQUFMLEtBQUssQ0FBWTtJQUFHLENBQUM7Ozs7O0lBRXpDLHlDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUNoQyxJQUFJLE9BQU8sQ0FBQyxlQUFlLENBQUMsWUFBWTtZQUN0QyxDQUFDLG1CQUFhLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFBLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZTtnQkFDM0QsT0FBTyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7SUFDM0MsQ0FBQzs7Z0JBWkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO2lCQUN6Qjs7OztnQkFSQyxVQUFVOzs7a0NBVVQsS0FBSzs7SUFTUiwwQkFBQztDQUFBLEFBYkQsSUFhQztTQVZZLG1CQUFtQjs7O0lBQzlCLDhDQUFpQzs7Ozs7SUFFckIsb0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBEaXJlY3RpdmUsXHJcbiAgRWxlbWVudFJlZixcclxuICBJbnB1dCxcclxuICBTaW1wbGVDaGFuZ2VzLFxyXG4gIE9uQ2hhbmdlcyxcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6IFwiW2xpYkVsQ29sb3JdXCIsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMaWJFbENvbG9yRGlyZWN0aXZlIGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICBASW5wdXQoKSBsaWJFbENvbG9yVmFsdWU6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge31cclxuXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgaWYgKGNoYW5nZXMubGliRWxDb2xvclZhbHVlLmN1cnJlbnRWYWx1ZSlcclxuICAgICAgKDxIVE1MRWxlbWVudD50aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQpLnN0eWxlLmJhY2tncm91bmRDb2xvciA9XHJcbiAgICAgICAgY2hhbmdlcy5saWJFbENvbG9yVmFsdWUuY3VycmVudFZhbHVlO1xyXG4gIH1cclxufVxyXG4iXX0=