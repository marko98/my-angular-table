/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild, ChangeDetectorRef, HostListener } from '@angular/core';
var ContextMenuComponent = /** @class */ (function () {
    function ContextMenuComponent(changeDetectorRef) {
        var _this = this;
        this.changeDetectorRef = changeDetectorRef;
        this.contextMenuData = {
            state: false,
            menuPositionX: undefined,
            menuPositionY: undefined,
            menuPosition: undefined,
            menuWidth: undefined,
            menuHeight: undefined,
            windowWidth: undefined,
            windowHeight: undefined
        };
        this.setModel = (/**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            _this.contextMenu = model;
            return true;
        });
        this.getContextMenuItems = (/**
         * @return {?}
         */
        function () {
            return (/** @type {?} */ (_this.contextMenu.getChildren()));
        });
        this.showContextMenu = (/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            // console.log(event);
            // console.log(row);
            _this.contextMenuData.state = true;
            event.stopPropagation();
            _this.positionMenu(event);
        });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuComponent.prototype.positionMenu = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.contextMenuData.menuPosition = this.getPosition(event);
        this.contextMenuData.menuPositionX = this.contextMenuData.menuPosition.x;
        this.contextMenuData.menuPositionY = this.contextMenuData.menuPosition.y;
        this.changeDetectorRef.detectChanges();
        this.contextMenuData.menuWidth = this.contextMenuView.nativeElement.offsetWidth;
        this.contextMenuData.menuHeight = this.contextMenuView.nativeElement.offsetHeight;
        this.contextMenuData.windowWidth = window.innerWidth;
        this.contextMenuData.windowHeight = window.innerHeight;
        if ((this.contextMenuData.windowWidth - this.contextMenuData.menuPositionX) < this.contextMenuData.menuWidth) {
            this.contextMenuData.menuPositionX = this.contextMenuData.windowWidth - this.contextMenuData.menuWidth + "px";
        }
        else {
            this.contextMenuData.menuPositionX = this.contextMenuData.menuPositionX + "px";
        }
        if ((this.contextMenuData.windowHeight - this.contextMenuData.menuPositionY) < this.contextMenuData.menuHeight) {
            this.contextMenuData.menuPositionY = this.contextMenuData.windowHeight - this.contextMenuData.menuHeight + "px";
        }
        else {
            this.contextMenuData.menuPositionY = this.contextMenuData.menuPositionY + "px";
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuComponent.prototype.getPosition = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var posx = 0;
        /** @type {?} */
        var posy = 0;
        if (event.pageX || event.pageY) {
            posx = event.pageX;
            posy = event.pageY;
        }
        else if (event.clientX || event.clientY) {
            posx = event.clientX + document.body.scrollLeft +
                document.documentElement.scrollLeft;
            posy = event.clientY + document.body.scrollTop +
                document.documentElement.scrollTop;
        }
        return { x: posx, y: posy };
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuComponent.prototype.documentClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.contextMenuData.state = false;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuComponent.prototype.documentRClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.contextMenuData.state = false;
    };
    ContextMenuComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-my-angular-table-context-menu',
                    template: "<section\r\n    class=\"no-padding context-active\"\r\n    *ngIf=\"this.contextMenuData.state\"\r\n    [ngStyle]=\"{'left': this.contextMenuData.menuPositionX, 'top': this.contextMenuData.menuPositionY}\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\"\r\n    fxFlex.xs=\"100px\"\r\n    fxFlex.gt-xs=\"150px\"\r\n    #contextMenu>\r\n        <section\r\n            class=\"context-menu-item\"\r\n            *ngFor=\"let contextMenuItem of this.getContextMenuItems()\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"10px\"\r\n            (click)=\"contextMenuItem.f(this.data)\">\r\n                <img \r\n                    *ngIf=\"contextMenuItem.imgSrc\"\r\n                    [src]=\"contextMenuItem.imgSrc\" \r\n                    alt=\"\">\r\n                <p\r\n                    fxFlex></p>\r\n                <p>\r\n                        {{ contextMenuItem.context }}\r\n                </p>\r\n        </section>\r\n</section>",
                    styles: [".no-padding{padding:0}.context-active{text-align:start;display:block;position:absolute;background-color:#fff;box-shadow:0 1px 5px rgba(0,0,0,.2),0 1px rgba(0,0,0,.14),0 1px rgba(0,0,0,.12)}section.context-menu-item{width:100%;box-shadow:0 1px #d3d3d3;cursor:pointer}section.context-menu-item:hover{background-color:#ebebeb}img{width:15%;margin:0 0 0 10px}p{margin:10px 10px 10px 0}"]
                }] }
    ];
    /** @nocollapse */
    ContextMenuComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef }
    ]; };
    ContextMenuComponent.propDecorators = {
        contextMenuView: [{ type: ViewChild, args: ['contextMenu', { static: false },] }],
        documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }],
        documentRClick: [{ type: HostListener, args: ["document:contextmenu", ["$event"],] }]
    };
    return ContextMenuComponent;
}());
export { ContextMenuComponent };
if (false) {
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenuView;
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenu;
    /** @type {?} */
    ContextMenuComponent.prototype.data;
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenuData;
    /** @type {?} */
    ContextMenuComponent.prototype.setModel;
    /** @type {?} */
    ContextMenuComponent.prototype.getContextMenuItems;
    /** @type {?} */
    ContextMenuComponent.prototype.showContextMenu;
    /**
     * @type {?}
     * @private
     */
    ContextMenuComponent.prototype.changeDetectorRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL2NvbnRleHQtbWVudS9jb250ZXh0LW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNdEY7SUFxQkksOEJBQW9CLGlCQUFvQztRQUF4RCxpQkFBMkQ7UUFBdkMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQVhqRCxvQkFBZSxHQUFHO1lBQ3JCLEtBQUssRUFBRSxLQUFLO1lBQ1osYUFBYSxFQUFFLFNBQVM7WUFDeEIsYUFBYSxFQUFFLFNBQVM7WUFDeEIsWUFBWSxFQUFFLFNBQVM7WUFDdkIsU0FBUyxFQUFFLFNBQVM7WUFDcEIsVUFBVSxFQUFFLFNBQVM7WUFDckIsV0FBVyxFQUFFLFNBQVM7WUFDdEIsWUFBWSxFQUFFLFNBQVM7U0FDMUIsQ0FBQztRQUlGLGFBQVE7Ozs7UUFBRyxVQUFDLEtBQWtCO1lBQzFCLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1lBRXpCLE9BQU8sSUFBSSxDQUFDO1FBQ2hCLENBQUMsRUFBQTtRQUVELHdCQUFtQjs7O1FBQUc7WUFDbEIsT0FBTyxtQkFBbUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsRUFBQSxDQUFDO1FBQzdELENBQUMsRUFBQTtRQUVELG9CQUFlOzs7O1FBQUcsVUFBQyxLQUFpQjtZQUNoQyxzQkFBc0I7WUFDdEIsb0JBQW9CO1lBRXBCLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUVsQyxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDeEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixDQUFDLEVBQUE7SUFwQnlELENBQUM7Ozs7O0lBc0IzRCwyQ0FBWTs7OztJQUFaLFVBQWEsS0FBaUI7UUFDMUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7UUFDaEYsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBQ2xGLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDckQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUN2RCxJQUFLLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRztZQUM1RyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDakg7YUFBTTtZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztTQUNsRjtRQUNELElBQUssQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFHO1lBQzlHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztTQUNuSDthQUFNO1lBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQ2xGO0lBQ0wsQ0FBQzs7Ozs7SUFFRCwwQ0FBVzs7OztJQUFYLFVBQVksS0FBaUI7O1lBQ3JCLElBQUksR0FBRyxDQUFDOztZQUNSLElBQUksR0FBRyxDQUFDO1FBQ1osSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDNUIsSUFBSSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDbkIsSUFBSSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7U0FDdEI7YUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUN2QyxJQUFJLEdBQUcsS0FBSyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVU7Z0JBQy9DLFFBQVEsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO1lBQ3BDLElBQUksR0FBRyxLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUztnQkFDOUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUM7U0FDdEM7UUFDRCxPQUFPLEVBQUMsQ0FBQyxFQUFFLElBQUksRUFBQyxDQUFDLEVBQUUsSUFBSSxFQUFDLENBQUE7SUFDNUIsQ0FBQzs7Ozs7SUFFa0QsNENBQWE7Ozs7SUFBaEUsVUFBaUUsS0FBaUI7UUFDOUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRXdELDZDQUFjOzs7O0lBQXZFLFVBQXdFLEtBQWlCO1FBQ3JGLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QyxDQUFDOztnQkFyRkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtQ0FBbUM7b0JBQzdDLDYrQkFBNEM7O2lCQUUvQzs7OztnQkFWOEIsaUJBQWlCOzs7a0NBWTNDLFNBQVMsU0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFDO2dDQXlFeEMsWUFBWSxTQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDO2lDQUl6QyxZQUFZLFNBQUMsc0JBQXNCLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBSXBELDJCQUFDO0NBQUEsQUF2RkQsSUF1RkM7U0FsRlksb0JBQW9COzs7SUFDN0IsK0NBQWdFOztJQUNoRSwyQ0FBZ0M7O0lBQ2hDLG9DQUFtQjs7SUFFbkIsK0NBU0U7O0lBSUYsd0NBSUM7O0lBRUQsbURBRUM7O0lBRUQsK0NBUUM7Ozs7O0lBcEJXLGlEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBDaGFuZ2VEZXRlY3RvclJlZiwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vLyBtb2RlbFxyXG5pbXBvcnQgeyBDb250ZXh0TWVudSwgQ29udGV4dE1lbnVWYWx1ZSB9IGZyb20gJy4uL21vZGVsL3BhdHRlcm5zL3N0cnVjdHVyYWwvY29tcG9zaXRlL2NvbnRleHQtbWVudS9jb250ZXh0LW1lbnUubW9kZWwnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudUl0ZW0gfSBmcm9tICcuLi9tb2RlbC9wYXR0ZXJucy9zdHJ1Y3R1cmFsL2NvbXBvc2l0ZS9jb250ZXh0LW1lbnUvY29udGV4dC1tZW51LWl0ZW0ubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2xpYi1teS1hbmd1bGFyLXRhYmxlLWNvbnRleHQtbWVudScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGV4dC1tZW51LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NvbnRleHQtbWVudS5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRleHRNZW51Q29tcG9uZW50IHtcclxuICAgIEBWaWV3Q2hpbGQoJ2NvbnRleHRNZW51Jywge3N0YXRpYzogZmFsc2V9KSBjb250ZXh0TWVudVZpZXc6IGFueTtcclxuICAgIHB1YmxpYyBjb250ZXh0TWVudTogQ29udGV4dE1lbnU7XHJcbiAgICBwdWJsaWMgZGF0YTogYW55W107XHJcblxyXG4gICAgcHVibGljIGNvbnRleHRNZW51RGF0YSA9IHtcclxuICAgICAgICBzdGF0ZTogZmFsc2UsXHJcbiAgICAgICAgbWVudVBvc2l0aW9uWDogdW5kZWZpbmVkLFxyXG4gICAgICAgIG1lbnVQb3NpdGlvblk6IHVuZGVmaW5lZCxcclxuICAgICAgICBtZW51UG9zaXRpb246IHVuZGVmaW5lZCxcclxuICAgICAgICBtZW51V2lkdGg6IHVuZGVmaW5lZCxcclxuICAgICAgICBtZW51SGVpZ2h0OiB1bmRlZmluZWQsXHJcbiAgICAgICAgd2luZG93V2lkdGg6IHVuZGVmaW5lZCxcclxuICAgICAgICB3aW5kb3dIZWlnaHQ6IHVuZGVmaW5lZFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZil7fVxyXG5cclxuICAgIHNldE1vZGVsID0gKG1vZGVsOiBDb250ZXh0TWVudSk6IGJvb2xlYW4gPT4ge1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnUgPSBtb2RlbDtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29udGV4dE1lbnVJdGVtcyA9ICgpOiBDb250ZXh0TWVudUl0ZW1bXSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIDxDb250ZXh0TWVudUl0ZW1bXT50aGlzLmNvbnRleHRNZW51LmdldENoaWxkcmVuKCk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0NvbnRleHRNZW51ID0gKGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCA9PiB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coZXZlbnQpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHJvdyk7XHJcblxyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLnN0YXRlID0gdHJ1ZTtcclxuICAgIFxyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIHRoaXMucG9zaXRpb25NZW51KGV2ZW50KTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgcG9zaXRpb25NZW51KGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uID0gdGhpcy5nZXRQb3NpdGlvbihldmVudCk7XHJcbiAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWCA9IHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvbi54O1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblkgPSB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51UG9zaXRpb24ueTtcclxuICAgICAgICB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51V2lkdGggPSB0aGlzLmNvbnRleHRNZW51Vmlldy5uYXRpdmVFbGVtZW50Lm9mZnNldFdpZHRoO1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVIZWlnaHQgPSB0aGlzLmNvbnRleHRNZW51Vmlldy5uYXRpdmVFbGVtZW50Lm9mZnNldEhlaWdodDtcclxuICAgICAgICB0aGlzLmNvbnRleHRNZW51RGF0YS53aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLndpbmRvd0hlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodDtcclxuICAgICAgICBpZiAoICh0aGlzLmNvbnRleHRNZW51RGF0YS53aW5kb3dXaWR0aCAtIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblgpIDwgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVdpZHRoICkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51UG9zaXRpb25YID0gdGhpcy5jb250ZXh0TWVudURhdGEud2luZG93V2lkdGggLSB0aGlzLmNvbnRleHRNZW51RGF0YS5tZW51V2lkdGggKyBcInB4XCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWCA9IHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblggKyBcInB4XCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggKHRoaXMuY29udGV4dE1lbnVEYXRhLndpbmRvd0hlaWdodCAtIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblkpIDwgdGhpcy5jb250ZXh0TWVudURhdGEubWVudUhlaWdodCApIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWSA9IHRoaXMuY29udGV4dE1lbnVEYXRhLndpbmRvd0hlaWdodCAtIHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVIZWlnaHQgKyBcInB4XCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudURhdGEubWVudVBvc2l0aW9uWSA9IHRoaXMuY29udGV4dE1lbnVEYXRhLm1lbnVQb3NpdGlvblkgKyBcInB4XCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBnZXRQb3NpdGlvbihldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIHZhciBwb3N4ID0gMDtcclxuICAgICAgICB2YXIgcG9zeSA9IDA7XHJcbiAgICAgICAgaWYgKGV2ZW50LnBhZ2VYIHx8IGV2ZW50LnBhZ2VZKSB7XHJcbiAgICAgICAgICAgIHBvc3ggPSBldmVudC5wYWdlWDtcclxuICAgICAgICAgICAgcG9zeSA9IGV2ZW50LnBhZ2VZO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQuY2xpZW50WCB8fCBldmVudC5jbGllbnRZKSB7XHJcbiAgICAgICAgICAgIHBvc3ggPSBldmVudC5jbGllbnRYICsgZG9jdW1lbnQuYm9keS5zY3JvbGxMZWZ0ICtcclxuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbExlZnQ7XHJcbiAgICAgICAgICAgIHBvc3kgPSBldmVudC5jbGllbnRZICsgZG9jdW1lbnQuYm9keS5zY3JvbGxUb3AgK1xyXG4gICAgICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge3g6IHBvc3gseTogcG9zeX1cclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKFwiZG9jdW1lbnQ6Y2xpY2tcIiwgW1wiJGV2ZW50XCJdKSBwdWJsaWMgZG9jdW1lbnRDbGljayhldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLnN0YXRlID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIEBIb3N0TGlzdGVuZXIoXCJkb2N1bWVudDpjb250ZXh0bWVudVwiLCBbXCIkZXZlbnRcIl0pIHB1YmxpYyBkb2N1bWVudFJDbGljayhldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVEYXRhLnN0YXRlID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG59Il19