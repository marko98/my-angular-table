/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
var UiService = /** @class */ (function () {
    function UiService(matSnackBar) {
        var _this = this;
        this.matSnackBar = matSnackBar;
        this.onShowSnackBar = (/**
         * @param {?} message
         * @param {?} action
         * @param {?} duration
         * @return {?}
         */
        function (message, action, duration) {
            _this.matSnackBar.open(message, action, { duration: duration });
        });
    }
    UiService.decorators = [
        { type: Injectable, args: [{ providedIn: "root" },] }
    ];
    /** @nocollapse */
    UiService.ctorParameters = function () { return [
        { type: MatSnackBar }
    ]; };
    /** @nocollapse */ UiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UiService_Factory() { return new UiService(i0.ɵɵinject(i1.MatSnackBar)); }, token: UiService, providedIn: "root" });
    return UiService;
}());
export { UiService };
if (false) {
    /** @type {?} */
    UiService.prototype.onShowSnackBar;
    /**
     * @type {?}
     * @private
     */
    UiService.prototype.matSnackBar;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215LWFuZ3VsYXItdGFibGUvIiwic291cmNlcyI6WyJsaWIvc2hhcmVkL3NlcnZpY2UvdWkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzFELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQUUzQztJQUdJLG1CQUFvQixXQUF3QjtRQUE1QyxpQkFBK0M7UUFBM0IsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFFNUMsbUJBQWM7Ozs7OztRQUFHLFVBQUMsT0FBZSxFQUFFLE1BQVcsRUFBRSxRQUFnQjtZQUM1RCxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQUM7UUFDakUsQ0FBQyxFQUFBO0lBSjZDLENBQUM7O2dCQUhsRCxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7O2dCQUh2QixXQUFXOzs7b0JBQXBCO0NBV0MsQUFSRCxJQVFDO1NBUFksU0FBUzs7O0lBSWxCLG1DQUVDOzs7OztJQUpXLGdDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1hdFNuYWNrQmFyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc25hY2stYmFyJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46IFwicm9vdFwifSlcclxuZXhwb3J0IGNsYXNzIFVpU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtYXRTbmFja0JhcjogTWF0U25hY2tCYXIpe31cclxuXHJcbiAgICBvblNob3dTbmFja0JhciA9IChtZXNzYWdlOiBzdHJpbmcsIGFjdGlvbjogYW55LCBkdXJhdGlvbjogbnVtYmVyKTogdm9pZCA9PiB7XHJcbiAgICAgICAgdGhpcy5tYXRTbmFja0Jhci5vcGVuKG1lc3NhZ2UsIGFjdGlvbiwge2R1cmF0aW9uOiBkdXJhdGlvbn0pO1xyXG4gICAgfVxyXG59Il19