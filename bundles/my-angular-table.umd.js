(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/material/snack-bar'), require('@angular/cdk/drag-drop'), require('@angular/common'), require('@angular/forms'), require('@angular/flex-layout'), require('@angular/material/button'), require('@angular/material/form-field'), require('@angular/material/input'), require('@angular/material/button-toggle'), require('@angular/material/icon')) :
    typeof define === 'function' && define.amd ? define('my-angular-table', ['exports', '@angular/core', '@angular/material/snack-bar', '@angular/cdk/drag-drop', '@angular/common', '@angular/forms', '@angular/flex-layout', '@angular/material/button', '@angular/material/form-field', '@angular/material/input', '@angular/material/button-toggle', '@angular/material/icon'], factory) :
    (global = global || self, factory(global['my-angular-table'] = {}, global.ng.core, global.ng.material['snack-bar'], global.ng.cdk['drag-drop'], global.ng.common, global.ng.forms, global.ng['flex-layout'], global.ng.material.button, global.ng.material['form-field'], global.ng.material.input, global.ng.material['button-toggle'], global.ng.material.icon));
}(this, (function (exports, core, snackBar, dragDrop, common, forms, flexLayout, button, formField, input, buttonToggle, icon) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MyAngularTableService = /** @class */ (function () {
        function MyAngularTableService() {
        }
        MyAngularTableService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        MyAngularTableService.ctorParameters = function () { return []; };
        /** @nocollapse */ MyAngularTableService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function MyAngularTableService_Factory() { return new MyAngularTableService(); }, token: MyAngularTableService, providedIn: "root" });
        return MyAngularTableService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var UiService = /** @class */ (function () {
        function UiService(matSnackBar) {
            var _this = this;
            this.matSnackBar = matSnackBar;
            this.onShowSnackBar = (/**
             * @param {?} message
             * @param {?} action
             * @param {?} duration
             * @return {?}
             */
            function (message, action, duration) {
                _this.matSnackBar.open(message, action, { duration: duration });
            });
        }
        UiService.decorators = [
            { type: core.Injectable, args: [{ providedIn: "root" },] }
        ];
        /** @nocollapse */
        UiService.ctorParameters = function () { return [
            { type: snackBar.MatSnackBar }
        ]; };
        /** @nocollapse */ UiService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function UiService_Factory() { return new UiService(core.ɵɵinject(snackBar.MatSnackBar)); }, token: UiService, providedIn: "root" });
        return UiService;
    }());
    if (false) {
        /** @type {?} */
        UiService.prototype.onShowSnackBar;
        /**
         * @type {?}
         * @private
         */
        UiService.prototype.matSnackBar;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var   /**
     * @abstract
     */
    Collection = /** @class */ (function () {
        function Collection() {
            var _this = this;
            this.children = [];
            this.parent = undefined;
            this.getChildren = (/**
             * @return {?}
             */
            function () {
                // vracamo kopiju
                // return this.children.slice();
                return _this.children;
            });
            this.getParent = (/**
             * @return {?}
             */
            function () {
                return _this.parent;
            });
            this.setParent = (/**
             * @param {?} parent
             * @return {?}
             */
            function (parent) {
                _this.parent = parent;
                return true;
            });
        }
        return Collection;
    }());
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        Collection.prototype.children;
        /**
         * @type {?}
         * @protected
         */
        Collection.prototype.parent;
        /** @type {?} */
        Collection.prototype.getChildren;
        /** @type {?} */
        Collection.prototype.getParent;
        /** @type {?} */
        Collection.prototype.setParent;
        /**
         * @abstract
         * @param {?} child
         * @return {?}
         */
        Collection.prototype.addChild = function (child) { };
        /**
         * @abstract
         * @param {?} child
         * @return {?}
         */
        Collection.prototype.removeChild = function (child) { };
        /**
         * @abstract
         * @param {?} child
         * @return {?}
         */
        Collection.prototype.getChildIndex = function (child) { };
        /**
         * @abstract
         * @return {?}
         */
        Collection.prototype.shouldHaveChildren = function () { };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var RowItem = /** @class */ (function (_super) {
        __extends(RowItem, _super);
        function RowItem(context) {
            var _this = _super.call(this) || this;
            _this.context = context;
            _this.ratingImgSrc = "https://img.icons8.com/android/2x/star.png";
            _this.addChild = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                return true;
            });
            _this.removeChild = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                return true;
            });
            _this.shouldHaveChildren = (/**
             * @return {?}
             */
            function () {
                return false;
            });
            _this.getChildIndex = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                return -1;
            });
            _this.getImgSrc = (/**
             * @return {?}
             */
            function () {
                return _this.imgSrc;
            });
            _this.setImgSrc = (/**
             * @param {?} imgSrc
             * @return {?}
             */
            function (imgSrc) {
                _this.imgSrc = imgSrc;
            });
            _this.getButton = (/**
             * @return {?}
             */
            function () {
                return _this.button;
            });
            _this.setButton = (/**
             * @param {?} button
             * @return {?}
             */
            function (button) {
                _this.button = button;
            });
            _this.getFunction = (/**
             * @return {?}
             */
            function () {
                return _this.function;
            });
            _this.setFunction = (/**
             * @param {?} f
             * @return {?}
             */
            function (f) {
                _this.function = f;
            });
            _this.getRating = (/**
             * @return {?}
             */
            function () {
                return _this.rating;
            });
            _this.setRating = (/**
             * @param {?} rating
             * @return {?}
             */
            function (rating) {
                _this.rating = rating;
            });
            _this.getRatingImgSrc = (/**
             * @return {?}
             */
            function () {
                return _this.ratingImgSrc;
            });
            _this.setRatingImgSrc = (/**
             * @param {?} ratingImgSrc
             * @return {?}
             */
            function (ratingImgSrc) {
                _this.ratingImgSrc = ratingImgSrc;
            });
            _this.getRatingCollection = (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var number;
                number = +_this.context;
                if (isNaN(number)) {
                    number = 0;
                }
                else {
                    if (number > 5)
                        number = 5;
                    if (number < 0)
                        number = 0;
                }
                return Array(number);
            });
            // getChildren = (): Collection[] => {
            //     // vracamo kopiju
            //     // return this.children.slice();
            //     return this.children;
            // }
            // getParent = (): Collection => {
            //     return this.parent;
            // }
            // setParent = (parent: Collection): boolean => {
            //     this.parent = parent;
            //     return true;
            // }
            _this.clone = (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var clone = new RowItem(_this.context);
                return clone;
            });
            _this.prototype = (/**
             * @param {?} rowItem
             * @return {?}
             */
            function (rowItem) {
                rowItem.context = _this.context;
            });
            if (typeof _this.context === "number")
                _this.context = _this.context.toString();
            if (_this.context instanceof Date)
                _this.context = _this.context.toLocaleString();
            return _this;
        }
        return RowItem;
    }(Collection));
    if (false) {
        /**
         * @type {?}
         * @private
         */
        RowItem.prototype.imgSrc;
        /**
         * @type {?}
         * @private
         */
        RowItem.prototype.button;
        /**
         * @type {?}
         * @private
         */
        RowItem.prototype.function;
        /**
         * @type {?}
         * @private
         */
        RowItem.prototype.rating;
        /**
         * @type {?}
         * @private
         */
        RowItem.prototype.ratingImgSrc;
        /** @type {?} */
        RowItem.prototype.addChild;
        /** @type {?} */
        RowItem.prototype.removeChild;
        /** @type {?} */
        RowItem.prototype.shouldHaveChildren;
        /** @type {?} */
        RowItem.prototype.getChildIndex;
        /** @type {?} */
        RowItem.prototype.getImgSrc;
        /** @type {?} */
        RowItem.prototype.setImgSrc;
        /** @type {?} */
        RowItem.prototype.getButton;
        /** @type {?} */
        RowItem.prototype.setButton;
        /** @type {?} */
        RowItem.prototype.getFunction;
        /** @type {?} */
        RowItem.prototype.setFunction;
        /** @type {?} */
        RowItem.prototype.getRating;
        /** @type {?} */
        RowItem.prototype.setRating;
        /** @type {?} */
        RowItem.prototype.getRatingImgSrc;
        /** @type {?} */
        RowItem.prototype.setRatingImgSrc;
        /** @type {?} */
        RowItem.prototype.getRatingCollection;
        /** @type {?} */
        RowItem.prototype.clone;
        /** @type {?} */
        RowItem.prototype.prototype;
        /** @type {?} */
        RowItem.prototype.context;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Row = /** @class */ (function (_super) {
        __extends(Row, _super);
        function Row(color) {
            var _this = _super.call(this) || this;
            // protected children: Collection[] = [];
            // protected parent: Collection = undefined;
            _this.show = true;
            _this.dragPreviewTempRefSubject = undefined;
            _this.addChild = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                _this.children.push(child);
                child.setParent(_this);
                return true;
            });
            _this.addChildValue = (/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                /** @type {?} */
                var child = new RowItem(value.context);
                if (value.imgSrc)
                    child.setImgSrc(value.imgSrc);
                if (value.button) {
                    child.setButton(true);
                    child.setFunction(value.button.function);
                }
                if (value.rating) {
                    child.setRating(true);
                    if (value.rating.ratingImgSrc)
                        child.setRatingImgSrc(value.rating.ratingImgSrc);
                }
                return _this.addChild(child);
            });
            _this.removeChild = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                _this.children = _this.children.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c !== child; }));
                child.setParent(undefined);
                return true;
            });
            _this.shouldHaveChildren = (/**
             * @return {?}
             */
            function () {
                return true;
            });
            _this.getChildIndex = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                return _this.children.findIndex((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return r === child; }));
            });
            // getChildren = (): Collection[] => {
            //     // vracamo kopiju
            //     // return this.children.slice();
            //     return this.children;
            // }
            // getParent = (): Collection => {
            //     return this.parent;
            // }
            // setParent = (parent: Collection): boolean => {
            //     this.parent = parent;
            //     return true;
            // }
            _this.clone = (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var clone = new Row();
                _this.children.forEach((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) {
                    clone.addChild(c.clone());
                }));
                return clone;
            });
            _this.prototype = (/**
             * @param {?} row
             * @return {?}
             */
            function (row) {
                _this.children.forEach((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) {
                    row.addChild(c.clone());
                }));
            });
            if (color)
                _this.color = color;
            return _this;
        }
        return Row;
    }(Collection));
    if (false) {
        /** @type {?} */
        Row.prototype.show;
        /** @type {?} */
        Row.prototype.data;
        /** @type {?} */
        Row.prototype.color;
        /** @type {?} */
        Row.prototype.dragPreviewTempRefSubject;
        /** @type {?} */
        Row.prototype.addChild;
        /** @type {?} */
        Row.prototype.addChildValue;
        /** @type {?} */
        Row.prototype.removeChild;
        /** @type {?} */
        Row.prototype.shouldHaveChildren;
        /** @type {?} */
        Row.prototype.getChildIndex;
        /** @type {?} */
        Row.prototype.clone;
        /** @type {?} */
        Row.prototype.prototype;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ContextMenuItem = /** @class */ (function (_super) {
        __extends(ContextMenuItem, _super);
        function ContextMenuItem(context, f, imgSrc) {
            var _this = _super.call(this) || this;
            _this.context = context;
            _this.f = f;
            _this.imgSrc = imgSrc;
            return _this;
        }
        /**
         * @param {?} child
         * @return {?}
         */
        ContextMenuItem.prototype.addChild = /**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return true;
        };
        /**
         * @param {?} child
         * @return {?}
         */
        ContextMenuItem.prototype.removeChild = /**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return true;
        };
        /**
         * @param {?} child
         * @return {?}
         */
        ContextMenuItem.prototype.getChildIndex = /**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return -1;
        };
        /**
         * @return {?}
         */
        ContextMenuItem.prototype.shouldHaveChildren = /**
         * @return {?}
         */
        function () {
            return false;
        };
        return ContextMenuItem;
    }(Collection));
    if (false) {
        /** @type {?} */
        ContextMenuItem.prototype.context;
        /** @type {?} */
        ContextMenuItem.prototype.f;
        /** @type {?} */
        ContextMenuItem.prototype.imgSrc;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ContextMenu = /** @class */ (function (_super) {
        __extends(ContextMenu, _super);
        function ContextMenu(contextMenuValue) {
            var _this = _super.call(this) || this;
            _this.contextMenuValue = contextMenuValue;
            _this.addChildValue = (/**
             * @param {?} value
             * @return {?}
             */
            function (value) {
                if (value.context && value.function) {
                    /** @type {?} */
                    var i = new ContextMenuItem(value.context, value.function, value.imgSrc);
                    _this.addChild(i);
                    return true;
                }
                return false;
            });
            contextMenuValue.contextMenuItems.forEach((/**
             * @param {?} i
             * @return {?}
             */
            function (i) {
                _this.addChildValue(i);
            }));
            return _this;
        }
        /**
         * @param {?} child
         * @return {?}
         */
        ContextMenu.prototype.addChild = /**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            this.children.push(child);
            child.setParent(this);
            return true;
        };
        /**
         * @param {?} child
         * @return {?}
         */
        ContextMenu.prototype.removeChild = /**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            this.children = this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c !== child; }));
            child.setParent(undefined);
            return true;
        };
        /**
         * @param {?} child
         * @return {?}
         */
        ContextMenu.prototype.getChildIndex = /**
         * @param {?} child
         * @return {?}
         */
        function (child) {
            return this.children.findIndex((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return c === child; }));
        };
        /**
         * @return {?}
         */
        ContextMenu.prototype.shouldHaveChildren = /**
         * @return {?}
         */
        function () {
            return true;
        };
        return ContextMenu;
    }(Collection));
    if (false) {
        /** @type {?} */
        ContextMenu.prototype.addChildValue;
        /**
         * @type {?}
         * @private
         */
        ContextMenu.prototype.contextMenuValue;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var Table = /** @class */ (function (_super) {
        __extends(Table, _super);
        // protected children: Collection[] = [];
        // protected parent: Collection = undefined;
        function Table(tableValue) {
            var _this = _super.call(this) || this;
            _this.sortedASC = false;
            _this.index = undefined;
            _this.contextMenu = undefined;
            _this.setContextMenuValue = (/**
             * @param {?} contextMenuValue
             * @return {?}
             */
            function (contextMenuValue) {
                if (contextMenuValue.contextMenuItems) {
                    _this.contextMenu = new ContextMenu(contextMenuValue);
                    return true;
                }
                return false;
            });
            _this.getContextMenu = (/**
             * @return {?}
             */
            function () {
                return _this.contextMenu;
            });
            _this.setHeader = (/**
             * @param {?} header
             * @return {?}
             */
            function (header) {
                _this.header = header;
                _this.header.setParent(_this);
                _this.header.getChildren().unshift(new RowItem("No."));
                return true;
            });
            _this.setHeaderValue = (/**
             * @param {?} headerValue
             * @return {?}
             */
            function (headerValue) {
                /** @type {?} */
                var header = new Row();
                if (_this.setHeader(header)) {
                    headerValue.headerItems.forEach((/**
                     * @param {?} headerItemValue
                     * @return {?}
                     */
                    function (headerItemValue) {
                        header.addChildValue(headerItemValue);
                    }));
                    return true;
                }
                return false;
            });
            _this.getHeader = (/**
             * @return {?}
             */
            function () {
                return _this.header;
            });
            _this.addChild = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                _this.children.push(child);
                child.setParent(_this);
                child.getChildren().unshift(new RowItem(_this.children.length + "."));
                return true;
            });
            _this.addChildValue = (/**
             * @param {?} rowValue
             * @return {?}
             */
            function (rowValue) {
                /** @type {?} */
                var row = new Row();
                if (rowValue.color)
                    row.color = rowValue.color;
                if (rowValue.dragPreviewTempRefSubject)
                    row.dragPreviewTempRefSubject = rowValue.dragPreviewTempRefSubject;
                if (_this.addChild(row)) {
                    row.data = rowValue.data;
                    rowValue.rowItems.forEach((/**
                     * @param {?} rowItemValue
                     * @return {?}
                     */
                    function (rowItemValue) {
                        row.addChildValue(rowItemValue);
                    }));
                    return true;
                }
                return false;
            });
            _this.removeChild = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                _this.children = _this.children.filter((/**
                 * @param {?} c
                 * @return {?}
                 */
                function (c) { return c !== child; }));
                child.setParent(undefined);
                return true;
            });
            _this.removeChildren = (/**
             * @return {?}
             */
            function () {
                _this.children.forEach((/**
                 * @param {?} child
                 * @return {?}
                 */
                function (child) { return _this.removeChild(child); }));
                return true;
            });
            _this.getFilteredChildren = (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var rows = [];
                _this.children.forEach((/**
                 * @param {?} row
                 * @return {?}
                 */
                function (row) {
                    if (row.show)
                        rows.push(row);
                }));
                return rows;
            });
            _this.shouldHaveChildren = (/**
             * @return {?}
             */
            function () {
                return true;
            });
            _this.getChildIndex = (/**
             * @param {?} child
             * @return {?}
             */
            function (child) {
                return _this.children.findIndex((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return r === child; }));
            });
            _this.onSort = (/**
             * @param {?} index
             * @return {?}
             */
            function (index) {
                if (_this.index !== index) {
                    _this.index = index;
                    _this.sortedASC = false;
                }
                if (!_this.sortedASC)
                    return _this.onASC(index);
                return _this.onDESC(index);
            });
            _this.onASC = (/**
             * @param {?} index
             * @return {?}
             */
            function (index) {
                _this.children.sort((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) {
                    if (((/** @type {?} */ (a.getChildren()[index]))).context <
                        ((/** @type {?} */ (b.getChildren()[index]))).context)
                        return -1;
                    if (((/** @type {?} */ (a.getChildren()[index]))).context >
                        ((/** @type {?} */ (b.getChildren()[index]))).context)
                        return 1;
                    return 0;
                }));
                _this.sortedASC = true;
            });
            _this.onDESC = (/**
             * @param {?} index
             * @return {?}
             */
            function (index) {
                _this.children.sort((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) {
                    if (((/** @type {?} */ (a.getChildren()[index]))).context >
                        ((/** @type {?} */ (b.getChildren()[index]))).context)
                        return -1;
                    if (((/** @type {?} */ (a.getChildren()[index]))).context <
                        ((/** @type {?} */ (b.getChildren()[index]))).context)
                        return 1;
                    return 0;
                }));
                _this.sortedASC = false;
            });
            _this.validate = (/**
             * @return {?}
             */
            function () {
                if (!_this.header)
                    throw Error("Table must have header");
                /** @type {?} */
                var valid = true;
                _this.children.forEach((/**
                 * @param {?} row
                 * @return {?}
                 */
                function (row) {
                    if (row.getChildren().length !== _this.header.getChildren().length)
                        valid = false;
                }));
                return valid;
            });
            if (tableValue) {
                if (!tableValue.header) {
                    throw Error("Header must exists");
                }
                else {
                    _this.setHeaderValue(tableValue.header);
                    if (tableValue.rows) {
                        tableValue.rows.forEach((/**
                         * @param {?} rowValue
                         * @return {?}
                         */
                        function (rowValue) {
                            _this.addChildValue(rowValue);
                        }));
                    }
                    if (tableValue.contextMenu)
                        _this.setContextMenuValue(tableValue.contextMenu);
                }
            }
            return _this;
        }
        return Table;
    }(Collection));
    if (false) {
        /**
         * @type {?}
         * @private
         */
        Table.prototype.header;
        /**
         * @type {?}
         * @private
         */
        Table.prototype.sortedASC;
        /**
         * @type {?}
         * @private
         */
        Table.prototype.index;
        /**
         * @type {?}
         * @private
         */
        Table.prototype.contextMenu;
        /** @type {?} */
        Table.prototype.setContextMenuValue;
        /** @type {?} */
        Table.prototype.getContextMenu;
        /** @type {?} */
        Table.prototype.setHeader;
        /** @type {?} */
        Table.prototype.setHeaderValue;
        /** @type {?} */
        Table.prototype.getHeader;
        /** @type {?} */
        Table.prototype.addChild;
        /** @type {?} */
        Table.prototype.addChildValue;
        /** @type {?} */
        Table.prototype.removeChild;
        /** @type {?} */
        Table.prototype.removeChildren;
        /** @type {?} */
        Table.prototype.getFilteredChildren;
        /** @type {?} */
        Table.prototype.shouldHaveChildren;
        /** @type {?} */
        Table.prototype.getChildIndex;
        /** @type {?} */
        Table.prototype.onSort;
        /** @type {?} */
        Table.prototype.onASC;
        /** @type {?} */
        Table.prototype.onDESC;
        /** @type {?} */
        Table.prototype.validate;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var ContextMenuComponent = /** @class */ (function () {
        function ContextMenuComponent(changeDetectorRef) {
            var _this = this;
            this.changeDetectorRef = changeDetectorRef;
            this.contextMenuData = {
                state: false,
                menuPositionX: undefined,
                menuPositionY: undefined,
                menuPosition: undefined,
                menuWidth: undefined,
                menuHeight: undefined,
                windowWidth: undefined,
                windowHeight: undefined
            };
            this.setModel = (/**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                _this.contextMenu = model;
                return true;
            });
            this.getContextMenuItems = (/**
             * @return {?}
             */
            function () {
                return (/** @type {?} */ (_this.contextMenu.getChildren()));
            });
            this.showContextMenu = (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                // console.log(event);
                // console.log(row);
                _this.contextMenuData.state = true;
                event.stopPropagation();
                _this.positionMenu(event);
            });
        }
        /**
         * @param {?} event
         * @return {?}
         */
        ContextMenuComponent.prototype.positionMenu = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.contextMenuData.menuPosition = this.getPosition(event);
            this.contextMenuData.menuPositionX = this.contextMenuData.menuPosition.x;
            this.contextMenuData.menuPositionY = this.contextMenuData.menuPosition.y;
            this.changeDetectorRef.detectChanges();
            this.contextMenuData.menuWidth = this.contextMenuView.nativeElement.offsetWidth;
            this.contextMenuData.menuHeight = this.contextMenuView.nativeElement.offsetHeight;
            this.contextMenuData.windowWidth = window.innerWidth;
            this.contextMenuData.windowHeight = window.innerHeight;
            if ((this.contextMenuData.windowWidth - this.contextMenuData.menuPositionX) < this.contextMenuData.menuWidth) {
                this.contextMenuData.menuPositionX = this.contextMenuData.windowWidth - this.contextMenuData.menuWidth + "px";
            }
            else {
                this.contextMenuData.menuPositionX = this.contextMenuData.menuPositionX + "px";
            }
            if ((this.contextMenuData.windowHeight - this.contextMenuData.menuPositionY) < this.contextMenuData.menuHeight) {
                this.contextMenuData.menuPositionY = this.contextMenuData.windowHeight - this.contextMenuData.menuHeight + "px";
            }
            else {
                this.contextMenuData.menuPositionY = this.contextMenuData.menuPositionY + "px";
            }
        };
        /**
         * @param {?} event
         * @return {?}
         */
        ContextMenuComponent.prototype.getPosition = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            /** @type {?} */
            var posx = 0;
            /** @type {?} */
            var posy = 0;
            if (event.pageX || event.pageY) {
                posx = event.pageX;
                posy = event.pageY;
            }
            else if (event.clientX || event.clientY) {
                posx = event.clientX + document.body.scrollLeft +
                    document.documentElement.scrollLeft;
                posy = event.clientY + document.body.scrollTop +
                    document.documentElement.scrollTop;
            }
            return { x: posx, y: posy };
        };
        /**
         * @param {?} event
         * @return {?}
         */
        ContextMenuComponent.prototype.documentClick = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.contextMenuData.state = false;
        };
        /**
         * @param {?} event
         * @return {?}
         */
        ContextMenuComponent.prototype.documentRClick = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.contextMenuData.state = false;
        };
        ContextMenuComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lib-my-angular-table-context-menu',
                        template: "<section\r\n    class=\"no-padding context-active\"\r\n    *ngIf=\"this.contextMenuData.state\"\r\n    [ngStyle]=\"{'left': this.contextMenuData.menuPositionX, 'top': this.contextMenuData.menuPositionY}\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\"\r\n    fxFlex.xs=\"100px\"\r\n    fxFlex.gt-xs=\"150px\"\r\n    #contextMenu>\r\n        <section\r\n            class=\"context-menu-item\"\r\n            *ngFor=\"let contextMenuItem of this.getContextMenuItems()\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"10px\"\r\n            (click)=\"contextMenuItem.f(this.data)\">\r\n                <img \r\n                    *ngIf=\"contextMenuItem.imgSrc\"\r\n                    [src]=\"contextMenuItem.imgSrc\" \r\n                    alt=\"\">\r\n                <p\r\n                    fxFlex></p>\r\n                <p>\r\n                        {{ contextMenuItem.context }}\r\n                </p>\r\n        </section>\r\n</section>",
                        styles: [".no-padding{padding:0}.context-active{text-align:start;display:block;position:absolute;background-color:#fff;box-shadow:0 1px 5px rgba(0,0,0,.2),0 1px rgba(0,0,0,.14),0 1px rgba(0,0,0,.12)}section.context-menu-item{width:100%;box-shadow:0 1px #d3d3d3;cursor:pointer}section.context-menu-item:hover{background-color:#ebebeb}img{width:15%;margin:0 0 0 10px}p{margin:10px 10px 10px 0}"]
                    }] }
        ];
        /** @nocollapse */
        ContextMenuComponent.ctorParameters = function () { return [
            { type: core.ChangeDetectorRef }
        ]; };
        ContextMenuComponent.propDecorators = {
            contextMenuView: [{ type: core.ViewChild, args: ['contextMenu', { static: false },] }],
            documentClick: [{ type: core.HostListener, args: ["document:click", ["$event"],] }],
            documentRClick: [{ type: core.HostListener, args: ["document:contextmenu", ["$event"],] }]
        };
        return ContextMenuComponent;
    }());
    if (false) {
        /** @type {?} */
        ContextMenuComponent.prototype.contextMenuView;
        /** @type {?} */
        ContextMenuComponent.prototype.contextMenu;
        /** @type {?} */
        ContextMenuComponent.prototype.data;
        /** @type {?} */
        ContextMenuComponent.prototype.contextMenuData;
        /** @type {?} */
        ContextMenuComponent.prototype.setModel;
        /** @type {?} */
        ContextMenuComponent.prototype.getContextMenuItems;
        /** @type {?} */
        ContextMenuComponent.prototype.showContextMenu;
        /**
         * @type {?}
         * @private
         */
        ContextMenuComponent.prototype.changeDetectorRef;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MyAngularTableComponent = /** @class */ (function () {
        function MyAngularTableComponent(uiService) {
            var _this = this;
            this.uiService = uiService;
            this.tempRef = undefined;
            this.criteria = "";
            this.selectedRows = [];
            this.isValid = false;
            this.perPage = 2;
            this.page = 0;
            this.onListInit = new core.EventEmitter();
            this.showContextMenu = false;
            this.onSort = (/**
             * @param {?} index
             * @return {?}
             */
            function (index) {
                // console.log(index);
                _this.table.onSort(index);
                // console.log(this.table.getChildren());
            });
            // getRows = (): Row[] => {
            //   return <Row[]>this.table.getChildren();
            // }
            this.onCriteriaChange = (/**
             * @return {?}
             */
            function () {
                // console.log(this.criteria);
                _this.table.getChildren().forEach((/**
                 * @param {?} row
                 * @return {?}
                 */
                function (row) {
                    /** @type {?} */
                    var value = "";
                    row.getChildren().forEach((/**
                     * @param {?} rowItem
                     * @return {?}
                     */
                    function (rowItem) {
                        value += rowItem.context.toString().toLowerCase();
                    }));
                    if (value.includes(_this.criteria))
                        row.show = true;
                    else
                        row.show = false;
                }));
            });
            this.onShowSnackBar = (/**
             * @param {?} message
             * @return {?}
             */
            function (message) {
                _this.uiService.onShowSnackBar(message, null, 1500);
            });
            this.onSelect = (/**
             * @param {?} event
             * @param {?} row
             * @return {?}
             */
            function (event, row) {
                if (event.ctrlKey) {
                    if (_this.selectedRows.find((/**
                     * @param {?} r
                     * @return {?}
                     */
                    function (r) { return r === row; }))) {
                        _this.selectedRows = _this.selectedRows.filter((/**
                         * @param {?} r
                         * @return {?}
                         */
                        function (r) { return r !== row; }));
                    }
                    else {
                        _this.selectedRows.push(row);
                    }
                }
                else if (_this.selectedRows.length > 0) {
                    _this.selectedRows = [];
                }
            });
            this.isSelected = (/**
             * @param {?} row
             * @return {?}
             */
            function (row) {
                if (_this.selectedRows.find((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return r === row; }))) {
                    return true;
                }
                return false;
            });
            this.onContextMenu = (/**
             * @param {?} event
             * @param {?} row
             * @return {?}
             */
            function (event, row) {
                // console.log(event);
                // console.log(row);
                if (_this.table.getContextMenu()) {
                    _this.showContextMenu = true;
                    if (_this.selectedRows.length > 0)
                        _this.contextMenuComponent.data = _this.selectedRows.map((/**
                         * @param {?} r
                         * @return {?}
                         */
                        function (r) { return r.data; }));
                    else
                        _this.contextMenuComponent.data = [row.data];
                    _this.contextMenuComponent.showContextMenu(event);
                }
            });
            this.next = (/**
             * @return {?}
             */
            function () {
                if (_this.page + 1 <
                    Math.ceil(_this.table.getChildren().length / _this.perPage))
                    _this.page++;
            });
            this.previous = (/**
             * @return {?}
             */
            function () {
                if (_this.page > 0)
                    _this.page--;
            });
        }
        Object.defineProperty(MyAngularTableComponent.prototype, "list", {
            set: /**
             * @param {?} list
             * @return {?}
             */
            function (list) {
                list.data = this.table;
                // console.log(list);
                this.onListInit.emit(list);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MyAngularTableComponent.prototype, "contextMenu", {
            set: /**
             * @param {?} contextMenuComponent
             * @return {?}
             */
            function (contextMenuComponent) {
                this.contextMenuComponent = contextMenuComponent;
                this.contextMenuComponent.setModel(this.table.getContextMenu());
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} event
         * @return {?}
         */
        MyAngularTableComponent.prototype.documentClick = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.showContextMenu = false;
            if (!event.ctrlKey)
                this.selectedRows = [];
        };
        /**
         * @param {?} event
         * @return {?}
         */
        MyAngularTableComponent.prototype.documentRClick = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.showContextMenu = false;
            if (!event.ctrlKey)
                this.selectedRows = [];
        };
        /**
         * @return {?}
         */
        MyAngularTableComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.isValid = this.table.validate();
            if (!this.isValid)
                throw new Error("Number of header items must be the same as the number in each row");
            // console.log("MyAngularTableComponent init");
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        MyAngularTableComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
            //Add '${implements OnChanges}' to the class.
            // console.log(changes);
        };
        /**
         * @return {?}
         */
        MyAngularTableComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            // console.log("MyAngularTableComponent destroyed");
        };
        MyAngularTableComponent.decorators = [
            { type: core.Component, args: [{
                        selector: "lib-my-angular-table",
                        template: "<style>\r\n\r\n    .example-list {\r\n        /* width: 500px; */\r\n        /* max-width: 100%; */\r\n        /* border: solid 1px #ccc; */\r\n        /* min-height: 60px; */\r\n        display: block;\r\n        /* background: white; */\r\n        /* border-radius: 4px; */\r\n        /* overflow: hidden; */\r\n    }\r\n\r\n    .example-box {\r\n        /* padding: 20px 10px; */\r\n        /* border-bottom: solid 1px #ccc; */\r\n        /* color: rgba(0, 0, 0, 0.87); */\r\n        /* display: flex; */\r\n        /* flex-direction: row; */\r\n        /* align-items: center; */\r\n        /* justify-content: space-between; */\r\n        /* box-sizing: border-box; */\r\n        cursor: move;\r\n        /* background: white; */\r\n        /* font-size: 14px; */\r\n    }\r\n\r\n    .cdk-drag-preview {\r\n        box-sizing: border-box;\r\n        border-radius: 4px;\r\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\r\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\r\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\r\n    }\r\n\r\n    .cdk-drag-placeholder {\r\n        opacity: 0;\r\n    }\r\n\r\n    .cdk-drag-animating {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n    .example-box:last-child {\r\n        border: none;\r\n    }\r\n\r\n    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n</style>\r\n\r\n<section\r\n    *ngIf=\"this.isValid\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\">\r\n        <section\r\n            class=\"no-padding\"\r\n            fxLayout=\"column\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"25px\">\r\n\r\n                <!-- FILTER -->\r\n                <mat-form-field>\r\n                    <mat-label>filter...</mat-label>\r\n                    <input \r\n                        matInput\r\n                        type=\"text\"\r\n                        [(ngModel)]=\"criteria\"\r\n                        (ngModelChange)=\"onCriteriaChange()\">\r\n                </mat-form-field>\r\n\r\n                \r\n                <section \r\n                    class=\"no-padding example-list\"\r\n                    cdkDropList\r\n                    #list=\"cdkDropList\">\r\n                    <!-- HEADER -->\r\n                    <section\r\n                        class=\"header\"\r\n                        fxLayout\r\n                        fxLayoutAlign=\"center center\"\r\n                        fxLayoutGap=\"10px\">\r\n                            <p\r\n                                class=\"header-item\"\r\n                                *ngFor=\"let headerItem of table.getHeader().getChildren()\"\r\n                                fxFlex\r\n                                (click)=\"onSort(headerItem.getParent().getChildIndex(headerItem))\">\r\n                                    {{ headerItem.context }}\r\n                            </p>\r\n                    </section>\r\n\r\n                    <section\r\n                        class=\"no-padding example-box\"\r\n                        cdkDrag\r\n                        *ngFor=\"let row of this.table.getFilteredChildren() | pagination: {page: this.page, perPage: this.perPage}\"\r\n                        libElColor\r\n                        [libElColorValue]=\"this.row.color\"\r\n                        [ngClass]=\"{'selected': this.isSelected(row)}\"\r\n                        (click)=\"onSelect($event, row)\"\r\n                        (contextmenu)=\"onContextMenu($event, row); false\">\r\n                            <section\r\n                                class=\"row\"\r\n                                *ngIf=\"row.show\"\r\n                                fxLayout\r\n                                fxLayoutAlign=\"start center\"\r\n                                fxLayoutGap=\"10px\">\r\n\r\n                                    <section\r\n                                        class=\"row-item no-padding\"\r\n                                        *ngFor=\"let rowItem of row.getChildren()\"\r\n                                        fxLayoutAlign=\"start center\"\r\n                                        fxLayoutGap=\"10px\"\r\n                                        fxFlex>\r\n                                            <img \r\n                                                fxHide.xs=\"true\"\r\n                                                *ngIf=\"rowItem.getImgSrc() ? true : false\"\r\n                                                [src]=\"rowItem.getImgSrc()\" \r\n                                                alt=\"row icon\">\r\n                                            <p \r\n                                                *ngIf=\"!rowItem.getButton() && !rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }}\r\n                                            </p>\r\n                                            <section\r\n                                                class=\"no-padding\"\r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\">\r\n                                                <img \r\n                                                    class=\"rating\"\r\n                                                    *ngFor=\"let i of rowItem.getRatingCollection()\"\r\n                                                    [src]=\"rowItem.getRatingImgSrc()\" \r\n                                                    alt=\"row icon\">\r\n                                            </section>\r\n                                            <!-- <p \r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }} rating\r\n                                            </p> -->\r\n                                            <button\r\n                                                *ngIf=\"rowItem.getButton()\"\r\n                                                (click)=\"rowItem.getFunction()([row.data])\"\r\n                                                class=\"row-item\">\r\n                                                    {{ rowItem.context }}\r\n                                            </button>\r\n                                    </section>\r\n                            </section>                            \r\n\r\n                            <section \r\n                                style=\"padding: 0;\"\r\n                                *ngIf='this.tempRef'>\r\n                                    <section *cdkDragPreview>\r\n                                        <section \r\n                                            *ngIf=\"false else this.tempRef\"></section>\r\n                                    </section>\r\n                            </section>\r\n\r\n                    </section>\r\n                </section>\r\n                \r\n                <section\r\n                    fxLayout\r\n                    fxLayoutAlign=\"center center\"\r\n                    fxLayoutGap=\"50px\">\r\n                    <mat-form-field\r\n                        fxFlex=\"50px\">\r\n                            <mat-label>per page:</mat-label>\r\n                            <input \r\n                                matInput\r\n                                type=\"number\"\r\n                                min=\"1\"\r\n                                [(ngModel)]=\"this.perPage\">\r\n                    </mat-form-field>\r\n                    <mat-button-toggle-group appearance=\"legacy\">\r\n                        <mat-button-toggle\r\n                            (click)=\"previous()\">\r\n                                <mat-icon>keyboard_arrow_left</mat-icon>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle\r\n                            (click)=\"next()\">\r\n                                <mat-icon>keyboard_arrow_right</mat-icon>\r\n                        </mat-button-toggle>\r\n                    </mat-button-toggle-group>\r\n                </section>\r\n        </section>\r\n\r\n</section>\r\n\r\n<lib-my-angular-table-context-menu\r\n    [hidden]=\"!this.showContextMenu\"></lib-my-angular-table-context-menu>",
                        styles: ["section{padding:10px}section.header{width:80vw;height:25px;padding:20px;border-top:1px solid grey;border-bottom:1px solid #d3d3d3}p.header-item{color:silver;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}p.header-item:hover{cursor:pointer;border-radius:2px;box-shadow:0 1px #d3d3d3}section.row{width:80vw;height:25px;padding:20px;box-shadow:0 1px #d3d3d3}section.row-item{cursor:default;overflow:hidden}p.row-item{color:grey;text-align:center;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item{overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item:focus{outline:0}.no-padding{padding:0}.box-shadow{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}mat-form-field{max-width:300px}img{width:15%;height:15%;border-radius:40%}img.rating{width:12%;height:12%}hr{width:20px}button{border:none;background-color:transparent;cursor:pointer;height:30px;border-radius:5px}button:hover{background-color:#ebebeb}.no-margin{margin:0}.selected{background-color:rgba(221,221,221,.2);box-shadow:0 1px 1px rgba(0,0,0,.2)}"]
                    }] }
        ];
        /** @nocollapse */
        MyAngularTableComponent.ctorParameters = function () { return [
            { type: UiService }
        ]; };
        MyAngularTableComponent.propDecorators = {
            tempRef: [{ type: core.Input }],
            table: [{ type: core.Input }],
            perPage: [{ type: core.Input }],
            onListInit: [{ type: core.Output }],
            list: [{ type: core.ViewChild, args: ["list", { static: false },] }],
            contextMenu: [{ type: core.ViewChild, args: [ContextMenuComponent, { static: false },] }],
            documentClick: [{ type: core.HostListener, args: ["document:click", ["$event"],] }],
            documentRClick: [{ type: core.HostListener, args: ["document:contextmenu", ["$event"],] }]
        };
        return MyAngularTableComponent;
    }());
    if (false) {
        /** @type {?} */
        MyAngularTableComponent.prototype.tempRef;
        /** @type {?} */
        MyAngularTableComponent.prototype.table;
        /** @type {?} */
        MyAngularTableComponent.prototype.criteria;
        /**
         * @type {?}
         * @private
         */
        MyAngularTableComponent.prototype.contextMenuComponent;
        /**
         * @type {?}
         * @private
         */
        MyAngularTableComponent.prototype.selectedRows;
        /** @type {?} */
        MyAngularTableComponent.prototype.isValid;
        /** @type {?} */
        MyAngularTableComponent.prototype.perPage;
        /** @type {?} */
        MyAngularTableComponent.prototype.page;
        /** @type {?} */
        MyAngularTableComponent.prototype.onListInit;
        /** @type {?} */
        MyAngularTableComponent.prototype.showContextMenu;
        /** @type {?} */
        MyAngularTableComponent.prototype.onSort;
        /** @type {?} */
        MyAngularTableComponent.prototype.onCriteriaChange;
        /** @type {?} */
        MyAngularTableComponent.prototype.onShowSnackBar;
        /** @type {?} */
        MyAngularTableComponent.prototype.onSelect;
        /** @type {?} */
        MyAngularTableComponent.prototype.isSelected;
        /** @type {?} */
        MyAngularTableComponent.prototype.onContextMenu;
        /** @type {?} */
        MyAngularTableComponent.prototype.next;
        /** @type {?} */
        MyAngularTableComponent.prototype.previous;
        /**
         * @type {?}
         * @private
         */
        MyAngularTableComponent.prototype.uiService;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var material = [
        button.MatButtonModule,
        formField.MatFormFieldModule,
        input.MatInputModule,
        buttonToggle.MatButtonToggleModule,
        icon.MatIconModule,
        snackBar.MatSnackBarModule,
        dragDrop.DragDropModule,
    ];
    var MaterialModule = /** @class */ (function () {
        function MaterialModule() {
        }
        MaterialModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: __spread(material),
                        exports: __spread(material),
                    },] }
        ];
        return MaterialModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PaginationPipe = /** @class */ (function () {
        function PaginationPipe() {
        }
        /**
         * @param {?} value
         * @param {?} data
         * @return {?}
         */
        PaginationPipe.prototype.transform = /**
         * @param {?} value
         * @param {?} data
         * @return {?}
         */
        function (value, data) {
            // console.log(data);
            return value.slice(data.page * data.perPage, data.page * data.perPage + data.perPage);
        };
        PaginationPipe.decorators = [
            { type: core.Pipe, args: [{ name: 'pagination' },] }
        ];
        return PaginationPipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LibElColorDirective = /** @class */ (function () {
        function LibElColorDirective(elRef) {
            this.elRef = elRef;
        }
        /**
         * @param {?} changes
         * @return {?}
         */
        LibElColorDirective.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (changes.libElColorValue.currentValue)
                ((/** @type {?} */ (this.elRef.nativeElement))).style.backgroundColor =
                    changes.libElColorValue.currentValue;
        };
        LibElColorDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: "[libElColor]",
                    },] }
        ];
        /** @nocollapse */
        LibElColorDirective.ctorParameters = function () { return [
            { type: core.ElementRef }
        ]; };
        LibElColorDirective.propDecorators = {
            libElColorValue: [{ type: core.Input }]
        };
        return LibElColorDirective;
    }());
    if (false) {
        /** @type {?} */
        LibElColorDirective.prototype.libElColorValue;
        /**
         * @type {?}
         * @private
         */
        LibElColorDirective.prototype.elRef;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SharedModule = /** @class */ (function () {
        function SharedModule() {
        }
        SharedModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [ContextMenuComponent, PaginationPipe, LibElColorDirective],
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            MaterialModule,
                            flexLayout.FlexLayoutModule,
                        ],
                        exports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            MaterialModule,
                            flexLayout.FlexLayoutModule,
                            ContextMenuComponent,
                            PaginationPipe,
                            LibElColorDirective,
                        ],
                    },] }
        ];
        return SharedModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MyAngularTableModule = /** @class */ (function () {
        function MyAngularTableModule() {
        }
        MyAngularTableModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [MyAngularTableComponent],
                        imports: [SharedModule],
                        exports: [MyAngularTableComponent],
                        providers: [],
                    },] }
        ];
        return MyAngularTableModule;
    }());

    exports.ContextMenu = ContextMenu;
    exports.ContextMenuComponent = ContextMenuComponent;
    exports.ContextMenuItem = ContextMenuItem;
    exports.MaterialModule = MaterialModule;
    exports.MyAngularTableComponent = MyAngularTableComponent;
    exports.MyAngularTableModule = MyAngularTableModule;
    exports.MyAngularTableService = MyAngularTableService;
    exports.Row = Row;
    exports.RowItem = RowItem;
    exports.SharedModule = SharedModule;
    exports.Table = Table;
    exports.UiService = UiService;
    exports.ɵa = Collection;
    exports.ɵb = PaginationPipe;
    exports.ɵc = LibElColorDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=my-angular-table.umd.js.map
