# MyAngularTable

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.

## Code scaffolding

Run `ng generate component component-name --project my-angular-table` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project my-angular-table`.
> Note: Don't forget to add `--project my-angular-table` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build my-angular-table` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build my-angular-table`, go to the dist folder `cd dist/my-angular-table` and run `npm publish`.

## Running unit tests

Run `ng test my-angular-table` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Example
// I way
this.table = new Table({
    header: {
    headerItems: [
        {context: "Name"},
        {context: "header 2asdddddddddddddddddddddddddddddd"},
        {context: "header 3"},
        {context: "header 4"}
    ]
    },
    rows: [
    {
        data: "Marko je car",
        rowItems: [
        {context: "Marko", imgSrc: "https://img.icons8.com/plasticine/2x/person-male.png"},
        {context: "q"},
        {context: "2", rating: {
            ratingImgSrc: "https://img.icons8.com/nolan/2x/star.png"
        }},
        {context: "3"}
        ]
    },
    {
        data: "David is the best",
        rowItems: [
        {context: "David", imgSrc: "https://img.icons8.com/plasticine/2x/person-male.png"},
        {context: "g"},
        {context: "delete", button: {function: this.delete}},
        {context: "marko98marko98marko98marko98marko98marko98"}
        ]
    },
    {
        data: "Vesna is cool",
        rowItems: [
        {context: "Vesna", imgSrc: "https://img.icons8.com/plasticine/2x/person-female.png"},
        {context: new Date()},
        {context: "g"},
        {context: "show", button: {function: this.add}}
        ]
    }
    ],
    contextMenu: {
    contextMenuItems: [
        {context: "add", function: this.add, imgSrc: "https://img.icons8.com/bubbles/2x/add.png"},
        {context: "update", function: this.update, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
        {context: "delete", function: this.delete, imgSrc: "https://img.icons8.com/doodle/2x/delete-sign.png"}
    ]
    }
});

// II way
this.table = new Table();
this.table.setContextMenuValue({
    contextMenuItems: [
    {context: "add", function: this.add, imgSrc: "https://img.icons8.com/bubbles/2x/add.png"},
    {context: "update", function: this.update, imgSrc: "https://img.icons8.com/nolan/2x/approve-and-update.png"},
    {context: "delete", function: this.delete, imgSrc: "https://img.icons8.com/doodle/2x/delete-sign.png"}
    ]
});

// create header
this.table.setHeaderValue({
    headerItems: [
    {context: "Name"},
    {context: "header 2asdddddddddddddddddddddddddddddd"},
    {context: "header 3"},
    {context: "header 4"}
    ]
});

// create rows
this.table.addChildValue({
    data: "Marko's data",
    rowItems: [
    {context: "Marko", imgSrc: "https://img.icons8.com/plasticine/2x/person-male.png"},
    {context: "q"},
    {context: "2", rating: {}},
    {context: "3"}
    ]
});

this.table.addChildValue({
    data: "David is the best brother",
    rowItems: [
    {context: "David", imgSrc: "https://img.icons8.com/plasticine/2x/person-male.png"},
    {context: "g"},
    {context: "delete", button: {function: this.delete}},
    {context: "marko98marko98marko98marko98marko98marko98"}
    ]
});

this.table.addChildValue({
    data: "Vesna's data",
    rowItems: [
    {context: "Vesna", imgSrc: "https://img.icons8.com/plasticine/2x/person-female.png"},
    {context: new Date()},
    {context: "g"},
    {context: "show", button: {function: this.add}}
    ]
});

// III way
this.table = new Table();
// add header
let header = new Row();
header.addChild(new RowItem("first name"));
header.addChild(new RowItem("last name"));
header.addChild(new RowItem("age"));
header.addChild(new RowItem("birthday"));
header.addChild(new RowItem("stars"));
this.table.setHeader(header);

// row
let row = new Row();
row.addChild(new RowItem("Marko"));
row.addChild(new RowItem("Markovic"));
row.addChild(new RowItem(25));
row.addChild(new RowItem(new Date(1995, 5, 18)));
row.addChild(new RowItem("2"));
this.table.addChild(row);

// row
let row2 = new Row();
// add data
row2.data = ["David Davidovic"];

row2.addChild(new RowItem("David"));
row2.addChild(new RowItem("Davidovic"));

// button row item
let button = new RowItem(25);
button.setButton(true);
button.setFunction(this.add);
// add icon
button.setImgSrc("https://img.icons8.com/nolan/2x/star.png");
row2.addChild(button);

row2.addChild(new RowItem(new Date(1995, 4, 17)));

// rating row item
let ir = new RowItem("3");
ir.setRating(true);
ir.setRatingImgSrc("https://img.icons8.com/nolan/2x/star.png");
row2.addChild(ir);

this.table.addChild(row2);

add = (data: any[]): void => {
    console.log("add");
    console.log(data);
}

update = (data: any[]): void => {
    console.log("update");
    console.log(data);
}

delete = (data: any[]): void => {
    console.log("delete");
    console.log(data);
}