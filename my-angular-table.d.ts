/**
 * Generated bundle index. Do not edit.
 */
export * from './public-api';
export { LibElColorDirective as ɵc } from './lib/shared/directives/background-color.directive';
export { Collection as ɵa } from './lib/shared/model/patterns/structural/composite/collection.model';
export { PaginationPipe as ɵb } from './lib/shared/pipes/per-page.pipe';
