import { Injectable, ɵɵdefineInjectable, ɵɵinject, Component, ChangeDetectorRef, ViewChild, HostListener, EventEmitter, Input, Output, NgModule, Pipe, Directive, ElementRef } from '@angular/core';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MyAngularTableService {
    constructor() { }
}
MyAngularTableService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
MyAngularTableService.ctorParameters = () => [];
/** @nocollapse */ MyAngularTableService.ngInjectableDef = ɵɵdefineInjectable({ factory: function MyAngularTableService_Factory() { return new MyAngularTableService(); }, token: MyAngularTableService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UiService {
    /**
     * @param {?} matSnackBar
     */
    constructor(matSnackBar) {
        this.matSnackBar = matSnackBar;
        this.onShowSnackBar = (/**
         * @param {?} message
         * @param {?} action
         * @param {?} duration
         * @return {?}
         */
        (message, action, duration) => {
            this.matSnackBar.open(message, action, { duration: duration });
        });
    }
}
UiService.decorators = [
    { type: Injectable, args: [{ providedIn: "root" },] }
];
/** @nocollapse */
UiService.ctorParameters = () => [
    { type: MatSnackBar }
];
/** @nocollapse */ UiService.ngInjectableDef = ɵɵdefineInjectable({ factory: function UiService_Factory() { return new UiService(ɵɵinject(MatSnackBar)); }, token: UiService, providedIn: "root" });
if (false) {
    /** @type {?} */
    UiService.prototype.onShowSnackBar;
    /**
     * @type {?}
     * @private
     */
    UiService.prototype.matSnackBar;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class Collection {
    constructor() {
        this.children = [];
        this.parent = undefined;
        this.getChildren = (/**
         * @return {?}
         */
        () => {
            // vracamo kopiju
            // return this.children.slice();
            return this.children;
        });
        this.getParent = (/**
         * @return {?}
         */
        () => {
            return this.parent;
        });
        this.setParent = (/**
         * @param {?} parent
         * @return {?}
         */
        (parent) => {
            this.parent = parent;
            return true;
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    Collection.prototype.children;
    /**
     * @type {?}
     * @protected
     */
    Collection.prototype.parent;
    /** @type {?} */
    Collection.prototype.getChildren;
    /** @type {?} */
    Collection.prototype.getParent;
    /** @type {?} */
    Collection.prototype.setParent;
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.addChild = function (child) { };
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.removeChild = function (child) { };
    /**
     * @abstract
     * @param {?} child
     * @return {?}
     */
    Collection.prototype.getChildIndex = function (child) { };
    /**
     * @abstract
     * @return {?}
     */
    Collection.prototype.shouldHaveChildren = function () { };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RowItem extends Collection {
    /**
     * @param {?} context
     */
    constructor(context) {
        super();
        this.context = context;
        this.ratingImgSrc = "https://img.icons8.com/android/2x/star.png";
        this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return true;
        });
        this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return true;
        });
        this.shouldHaveChildren = (/**
         * @return {?}
         */
        () => {
            return false;
        });
        this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return -1;
        });
        this.getImgSrc = (/**
         * @return {?}
         */
        () => {
            return this.imgSrc;
        });
        this.setImgSrc = (/**
         * @param {?} imgSrc
         * @return {?}
         */
        (imgSrc) => {
            this.imgSrc = imgSrc;
        });
        this.getButton = (/**
         * @return {?}
         */
        () => {
            return this.button;
        });
        this.setButton = (/**
         * @param {?} button
         * @return {?}
         */
        (button) => {
            this.button = button;
        });
        this.getFunction = (/**
         * @return {?}
         */
        () => {
            return this.function;
        });
        this.setFunction = (/**
         * @param {?} f
         * @return {?}
         */
        (f) => {
            this.function = f;
        });
        this.getRating = (/**
         * @return {?}
         */
        () => {
            return this.rating;
        });
        this.setRating = (/**
         * @param {?} rating
         * @return {?}
         */
        (rating) => {
            this.rating = rating;
        });
        this.getRatingImgSrc = (/**
         * @return {?}
         */
        () => {
            return this.ratingImgSrc;
        });
        this.setRatingImgSrc = (/**
         * @param {?} ratingImgSrc
         * @return {?}
         */
        (ratingImgSrc) => {
            this.ratingImgSrc = ratingImgSrc;
        });
        this.getRatingCollection = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let number;
            number = +this.context;
            if (isNaN(number)) {
                number = 0;
            }
            else {
                if (number > 5)
                    number = 5;
                if (number < 0)
                    number = 0;
            }
            return Array(number);
        });
        // getChildren = (): Collection[] => {
        //     // vracamo kopiju
        //     // return this.children.slice();
        //     return this.children;
        // }
        // getParent = (): Collection => {
        //     return this.parent;
        // }
        // setParent = (parent: Collection): boolean => {
        //     this.parent = parent;
        //     return true;
        // }
        this.clone = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let clone = new RowItem(this.context);
            return clone;
        });
        this.prototype = (/**
         * @param {?} rowItem
         * @return {?}
         */
        (rowItem) => {
            rowItem.context = this.context;
        });
        if (typeof this.context === "number")
            this.context = this.context.toString();
        if (this.context instanceof Date)
            this.context = this.context.toLocaleString();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.imgSrc;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.button;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.function;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.rating;
    /**
     * @type {?}
     * @private
     */
    RowItem.prototype.ratingImgSrc;
    /** @type {?} */
    RowItem.prototype.addChild;
    /** @type {?} */
    RowItem.prototype.removeChild;
    /** @type {?} */
    RowItem.prototype.shouldHaveChildren;
    /** @type {?} */
    RowItem.prototype.getChildIndex;
    /** @type {?} */
    RowItem.prototype.getImgSrc;
    /** @type {?} */
    RowItem.prototype.setImgSrc;
    /** @type {?} */
    RowItem.prototype.getButton;
    /** @type {?} */
    RowItem.prototype.setButton;
    /** @type {?} */
    RowItem.prototype.getFunction;
    /** @type {?} */
    RowItem.prototype.setFunction;
    /** @type {?} */
    RowItem.prototype.getRating;
    /** @type {?} */
    RowItem.prototype.setRating;
    /** @type {?} */
    RowItem.prototype.getRatingImgSrc;
    /** @type {?} */
    RowItem.prototype.setRatingImgSrc;
    /** @type {?} */
    RowItem.prototype.getRatingCollection;
    /** @type {?} */
    RowItem.prototype.clone;
    /** @type {?} */
    RowItem.prototype.prototype;
    /** @type {?} */
    RowItem.prototype.context;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Row extends Collection {
    /**
     * @param {?=} color
     */
    constructor(color) {
        super();
        // protected children: Collection[] = [];
        // protected parent: Collection = undefined;
        this.show = true;
        this.dragPreviewTempRefSubject = undefined;
        this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children.push(child);
            child.setParent(this);
            return true;
        });
        this.addChildValue = (/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            let child = new RowItem(value.context);
            if (value.imgSrc)
                child.setImgSrc(value.imgSrc);
            if (value.button) {
                child.setButton(true);
                child.setFunction(value.button.function);
            }
            if (value.rating) {
                child.setRating(true);
                if (value.rating.ratingImgSrc)
                    child.setRatingImgSrc(value.rating.ratingImgSrc);
            }
            return this.addChild(child);
        });
        this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children = this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => c !== child));
            child.setParent(undefined);
            return true;
        });
        this.shouldHaveChildren = (/**
         * @return {?}
         */
        () => {
            return true;
        });
        this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return this.children.findIndex((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r === child));
        });
        // getChildren = (): Collection[] => {
        //     // vracamo kopiju
        //     // return this.children.slice();
        //     return this.children;
        // }
        // getParent = (): Collection => {
        //     return this.parent;
        // }
        // setParent = (parent: Collection): boolean => {
        //     this.parent = parent;
        //     return true;
        // }
        this.clone = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let clone = new Row();
            this.children.forEach((/**
             * @param {?} c
             * @return {?}
             */
            (c) => {
                clone.addChild(c.clone());
            }));
            return clone;
        });
        this.prototype = (/**
         * @param {?} row
         * @return {?}
         */
        (row) => {
            this.children.forEach((/**
             * @param {?} c
             * @return {?}
             */
            (c) => {
                row.addChild(c.clone());
            }));
        });
        if (color)
            this.color = color;
    }
}
if (false) {
    /** @type {?} */
    Row.prototype.show;
    /** @type {?} */
    Row.prototype.data;
    /** @type {?} */
    Row.prototype.color;
    /** @type {?} */
    Row.prototype.dragPreviewTempRefSubject;
    /** @type {?} */
    Row.prototype.addChild;
    /** @type {?} */
    Row.prototype.addChildValue;
    /** @type {?} */
    Row.prototype.removeChild;
    /** @type {?} */
    Row.prototype.shouldHaveChildren;
    /** @type {?} */
    Row.prototype.getChildIndex;
    /** @type {?} */
    Row.prototype.clone;
    /** @type {?} */
    Row.prototype.prototype;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ContextMenuItem extends Collection {
    /**
     * @param {?} context
     * @param {?} f
     * @param {?=} imgSrc
     */
    constructor(context, f, imgSrc) {
        super();
        this.context = context;
        this.f = f;
        this.imgSrc = imgSrc;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    addChild(child) {
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    removeChild(child) {
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    getChildIndex(child) {
        return -1;
    }
    /**
     * @return {?}
     */
    shouldHaveChildren() {
        return false;
    }
}
if (false) {
    /** @type {?} */
    ContextMenuItem.prototype.context;
    /** @type {?} */
    ContextMenuItem.prototype.f;
    /** @type {?} */
    ContextMenuItem.prototype.imgSrc;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ContextMenu extends Collection {
    /**
     * @param {?} contextMenuValue
     */
    constructor(contextMenuValue) {
        super();
        this.contextMenuValue = contextMenuValue;
        this.addChildValue = (/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            if (value.context && value.function) {
                /** @type {?} */
                let i = new ContextMenuItem(value.context, value.function, value.imgSrc);
                this.addChild(i);
                return true;
            }
            return false;
        });
        contextMenuValue.contextMenuItems.forEach((/**
         * @param {?} i
         * @return {?}
         */
        (i) => {
            this.addChildValue(i);
        }));
    }
    /**
     * @param {?} child
     * @return {?}
     */
    addChild(child) {
        this.children.push(child);
        child.setParent(this);
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    removeChild(child) {
        this.children = this.children.filter((/**
         * @param {?} c
         * @return {?}
         */
        c => c !== child));
        child.setParent(undefined);
        return true;
    }
    /**
     * @param {?} child
     * @return {?}
     */
    getChildIndex(child) {
        return this.children.findIndex((/**
         * @param {?} c
         * @return {?}
         */
        c => c === child));
    }
    /**
     * @return {?}
     */
    shouldHaveChildren() {
        return true;
    }
}
if (false) {
    /** @type {?} */
    ContextMenu.prototype.addChildValue;
    /**
     * @type {?}
     * @private
     */
    ContextMenu.prototype.contextMenuValue;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Table extends Collection {
    // protected children: Collection[] = [];
    // protected parent: Collection = undefined;
    /**
     * @param {?=} tableValue
     */
    constructor(tableValue) {
        super();
        this.sortedASC = false;
        this.index = undefined;
        this.contextMenu = undefined;
        this.setContextMenuValue = (/**
         * @param {?} contextMenuValue
         * @return {?}
         */
        (contextMenuValue) => {
            if (contextMenuValue.contextMenuItems) {
                this.contextMenu = new ContextMenu(contextMenuValue);
                return true;
            }
            return false;
        });
        this.getContextMenu = (/**
         * @return {?}
         */
        () => {
            return this.contextMenu;
        });
        this.setHeader = (/**
         * @param {?} header
         * @return {?}
         */
        (header) => {
            this.header = header;
            this.header.setParent(this);
            this.header.getChildren().unshift(new RowItem("No."));
            return true;
        });
        this.setHeaderValue = (/**
         * @param {?} headerValue
         * @return {?}
         */
        (headerValue) => {
            /** @type {?} */
            let header = new Row();
            if (this.setHeader(header)) {
                headerValue.headerItems.forEach((/**
                 * @param {?} headerItemValue
                 * @return {?}
                 */
                (headerItemValue) => {
                    header.addChildValue(headerItemValue);
                }));
                return true;
            }
            return false;
        });
        this.getHeader = (/**
         * @return {?}
         */
        () => {
            return this.header;
        });
        this.addChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children.push(child);
            child.setParent(this);
            child.getChildren().unshift(new RowItem(this.children.length + "."));
            return true;
        });
        this.addChildValue = (/**
         * @param {?} rowValue
         * @return {?}
         */
        (rowValue) => {
            /** @type {?} */
            let row = new Row();
            if (rowValue.color)
                row.color = rowValue.color;
            if (rowValue.dragPreviewTempRefSubject)
                row.dragPreviewTempRefSubject = rowValue.dragPreviewTempRefSubject;
            if (this.addChild(row)) {
                row.data = rowValue.data;
                rowValue.rowItems.forEach((/**
                 * @param {?} rowItemValue
                 * @return {?}
                 */
                (rowItemValue) => {
                    row.addChildValue(rowItemValue);
                }));
                return true;
            }
            return false;
        });
        this.removeChild = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            this.children = this.children.filter((/**
             * @param {?} c
             * @return {?}
             */
            (c) => c !== child));
            child.setParent(undefined);
            return true;
        });
        this.removeChildren = (/**
         * @return {?}
         */
        () => {
            this.children.forEach((/**
             * @param {?} child
             * @return {?}
             */
            (child) => this.removeChild(child)));
            return true;
        });
        this.getFilteredChildren = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            let rows = [];
            this.children.forEach((/**
             * @param {?} row
             * @return {?}
             */
            (row) => {
                if (row.show)
                    rows.push(row);
            }));
            return rows;
        });
        this.shouldHaveChildren = (/**
         * @return {?}
         */
        () => {
            return true;
        });
        this.getChildIndex = (/**
         * @param {?} child
         * @return {?}
         */
        (child) => {
            return this.children.findIndex((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r === child));
        });
        this.onSort = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            if (this.index !== index) {
                this.index = index;
                this.sortedASC = false;
            }
            if (!this.sortedASC)
                return this.onASC(index);
            return this.onDESC(index);
        });
        this.onASC = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            this.children.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => {
                if (((/** @type {?} */ (a.getChildren()[index]))).context <
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return -1;
                if (((/** @type {?} */ (a.getChildren()[index]))).context >
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return 1;
                return 0;
            }));
            this.sortedASC = true;
        });
        this.onDESC = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            this.children.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => {
                if (((/** @type {?} */ (a.getChildren()[index]))).context >
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return -1;
                if (((/** @type {?} */ (a.getChildren()[index]))).context <
                    ((/** @type {?} */ (b.getChildren()[index]))).context)
                    return 1;
                return 0;
            }));
            this.sortedASC = false;
        });
        this.validate = (/**
         * @return {?}
         */
        () => {
            if (!this.header)
                throw Error("Table must have header");
            /** @type {?} */
            let valid = true;
            this.children.forEach((/**
             * @param {?} row
             * @return {?}
             */
            (row) => {
                if (row.getChildren().length !== this.header.getChildren().length)
                    valid = false;
            }));
            return valid;
        });
        if (tableValue) {
            if (!tableValue.header) {
                throw Error("Header must exists");
            }
            else {
                this.setHeaderValue(tableValue.header);
                if (tableValue.rows) {
                    tableValue.rows.forEach((/**
                     * @param {?} rowValue
                     * @return {?}
                     */
                    (rowValue) => {
                        this.addChildValue(rowValue);
                    }));
                }
                if (tableValue.contextMenu)
                    this.setContextMenuValue(tableValue.contextMenu);
            }
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    Table.prototype.header;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.sortedASC;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.index;
    /**
     * @type {?}
     * @private
     */
    Table.prototype.contextMenu;
    /** @type {?} */
    Table.prototype.setContextMenuValue;
    /** @type {?} */
    Table.prototype.getContextMenu;
    /** @type {?} */
    Table.prototype.setHeader;
    /** @type {?} */
    Table.prototype.setHeaderValue;
    /** @type {?} */
    Table.prototype.getHeader;
    /** @type {?} */
    Table.prototype.addChild;
    /** @type {?} */
    Table.prototype.addChildValue;
    /** @type {?} */
    Table.prototype.removeChild;
    /** @type {?} */
    Table.prototype.removeChildren;
    /** @type {?} */
    Table.prototype.getFilteredChildren;
    /** @type {?} */
    Table.prototype.shouldHaveChildren;
    /** @type {?} */
    Table.prototype.getChildIndex;
    /** @type {?} */
    Table.prototype.onSort;
    /** @type {?} */
    Table.prototype.onASC;
    /** @type {?} */
    Table.prototype.onDESC;
    /** @type {?} */
    Table.prototype.validate;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ContextMenuComponent {
    /**
     * @param {?} changeDetectorRef
     */
    constructor(changeDetectorRef) {
        this.changeDetectorRef = changeDetectorRef;
        this.contextMenuData = {
            state: false,
            menuPositionX: undefined,
            menuPositionY: undefined,
            menuPosition: undefined,
            menuWidth: undefined,
            menuHeight: undefined,
            windowWidth: undefined,
            windowHeight: undefined
        };
        this.setModel = (/**
         * @param {?} model
         * @return {?}
         */
        (model) => {
            this.contextMenu = model;
            return true;
        });
        this.getContextMenuItems = (/**
         * @return {?}
         */
        () => {
            return (/** @type {?} */ (this.contextMenu.getChildren()));
        });
        this.showContextMenu = (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            // console.log(event);
            // console.log(row);
            this.contextMenuData.state = true;
            event.stopPropagation();
            this.positionMenu(event);
        });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    positionMenu(event) {
        this.contextMenuData.menuPosition = this.getPosition(event);
        this.contextMenuData.menuPositionX = this.contextMenuData.menuPosition.x;
        this.contextMenuData.menuPositionY = this.contextMenuData.menuPosition.y;
        this.changeDetectorRef.detectChanges();
        this.contextMenuData.menuWidth = this.contextMenuView.nativeElement.offsetWidth;
        this.contextMenuData.menuHeight = this.contextMenuView.nativeElement.offsetHeight;
        this.contextMenuData.windowWidth = window.innerWidth;
        this.contextMenuData.windowHeight = window.innerHeight;
        if ((this.contextMenuData.windowWidth - this.contextMenuData.menuPositionX) < this.contextMenuData.menuWidth) {
            this.contextMenuData.menuPositionX = this.contextMenuData.windowWidth - this.contextMenuData.menuWidth + "px";
        }
        else {
            this.contextMenuData.menuPositionX = this.contextMenuData.menuPositionX + "px";
        }
        if ((this.contextMenuData.windowHeight - this.contextMenuData.menuPositionY) < this.contextMenuData.menuHeight) {
            this.contextMenuData.menuPositionY = this.contextMenuData.windowHeight - this.contextMenuData.menuHeight + "px";
        }
        else {
            this.contextMenuData.menuPositionY = this.contextMenuData.menuPositionY + "px";
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    getPosition(event) {
        /** @type {?} */
        var posx = 0;
        /** @type {?} */
        var posy = 0;
        if (event.pageX || event.pageY) {
            posx = event.pageX;
            posy = event.pageY;
        }
        else if (event.clientX || event.clientY) {
            posx = event.clientX + document.body.scrollLeft +
                document.documentElement.scrollLeft;
            posy = event.clientY + document.body.scrollTop +
                document.documentElement.scrollTop;
        }
        return { x: posx, y: posy };
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentClick(event) {
        this.contextMenuData.state = false;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentRClick(event) {
        this.contextMenuData.state = false;
    }
}
ContextMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-my-angular-table-context-menu',
                template: "<section\r\n    class=\"no-padding context-active\"\r\n    *ngIf=\"this.contextMenuData.state\"\r\n    [ngStyle]=\"{'left': this.contextMenuData.menuPositionX, 'top': this.contextMenuData.menuPositionY}\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\"\r\n    fxFlex.xs=\"100px\"\r\n    fxFlex.gt-xs=\"150px\"\r\n    #contextMenu>\r\n        <section\r\n            class=\"context-menu-item\"\r\n            *ngFor=\"let contextMenuItem of this.getContextMenuItems()\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"10px\"\r\n            (click)=\"contextMenuItem.f(this.data)\">\r\n                <img \r\n                    *ngIf=\"contextMenuItem.imgSrc\"\r\n                    [src]=\"contextMenuItem.imgSrc\" \r\n                    alt=\"\">\r\n                <p\r\n                    fxFlex></p>\r\n                <p>\r\n                        {{ contextMenuItem.context }}\r\n                </p>\r\n        </section>\r\n</section>",
                styles: [".no-padding{padding:0}.context-active{text-align:start;display:block;position:absolute;background-color:#fff;box-shadow:0 1px 5px rgba(0,0,0,.2),0 1px rgba(0,0,0,.14),0 1px rgba(0,0,0,.12)}section.context-menu-item{width:100%;box-shadow:0 1px #d3d3d3;cursor:pointer}section.context-menu-item:hover{background-color:#ebebeb}img{width:15%;margin:0 0 0 10px}p{margin:10px 10px 10px 0}"]
            }] }
];
/** @nocollapse */
ContextMenuComponent.ctorParameters = () => [
    { type: ChangeDetectorRef }
];
ContextMenuComponent.propDecorators = {
    contextMenuView: [{ type: ViewChild, args: ['contextMenu', { static: false },] }],
    documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }],
    documentRClick: [{ type: HostListener, args: ["document:contextmenu", ["$event"],] }]
};
if (false) {
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenuView;
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenu;
    /** @type {?} */
    ContextMenuComponent.prototype.data;
    /** @type {?} */
    ContextMenuComponent.prototype.contextMenuData;
    /** @type {?} */
    ContextMenuComponent.prototype.setModel;
    /** @type {?} */
    ContextMenuComponent.prototype.getContextMenuItems;
    /** @type {?} */
    ContextMenuComponent.prototype.showContextMenu;
    /**
     * @type {?}
     * @private
     */
    ContextMenuComponent.prototype.changeDetectorRef;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MyAngularTableComponent {
    /**
     * @param {?} uiService
     */
    constructor(uiService) {
        this.uiService = uiService;
        this.tempRef = undefined;
        this.criteria = "";
        this.selectedRows = [];
        this.isValid = false;
        this.perPage = 2;
        this.page = 0;
        this.onListInit = new EventEmitter();
        this.showContextMenu = false;
        this.onSort = (/**
         * @param {?} index
         * @return {?}
         */
        (index) => {
            // console.log(index);
            this.table.onSort(index);
            // console.log(this.table.getChildren());
        });
        // getRows = (): Row[] => {
        //   return <Row[]>this.table.getChildren();
        // }
        this.onCriteriaChange = (/**
         * @return {?}
         */
        () => {
            // console.log(this.criteria);
            this.table.getChildren().forEach((/**
             * @param {?} row
             * @return {?}
             */
            (row) => {
                /** @type {?} */
                let value = "";
                row.getChildren().forEach((/**
                 * @param {?} rowItem
                 * @return {?}
                 */
                (rowItem) => {
                    value += rowItem.context.toString().toLowerCase();
                }));
                if (value.includes(this.criteria))
                    row.show = true;
                else
                    row.show = false;
            }));
        });
        this.onShowSnackBar = (/**
         * @param {?} message
         * @return {?}
         */
        (message) => {
            this.uiService.onShowSnackBar(message, null, 1500);
        });
        this.onSelect = (/**
         * @param {?} event
         * @param {?} row
         * @return {?}
         */
        (event, row) => {
            if (event.ctrlKey) {
                if (this.selectedRows.find((/**
                 * @param {?} r
                 * @return {?}
                 */
                (r) => r === row))) {
                    this.selectedRows = this.selectedRows.filter((/**
                     * @param {?} r
                     * @return {?}
                     */
                    (r) => r !== row));
                }
                else {
                    this.selectedRows.push(row);
                }
            }
            else if (this.selectedRows.length > 0) {
                this.selectedRows = [];
            }
        });
        this.isSelected = (/**
         * @param {?} row
         * @return {?}
         */
        (row) => {
            if (this.selectedRows.find((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r === row))) {
                return true;
            }
            return false;
        });
        this.onContextMenu = (/**
         * @param {?} event
         * @param {?} row
         * @return {?}
         */
        (event, row) => {
            // console.log(event);
            // console.log(row);
            if (this.table.getContextMenu()) {
                this.showContextMenu = true;
                if (this.selectedRows.length > 0)
                    this.contextMenuComponent.data = this.selectedRows.map((/**
                     * @param {?} r
                     * @return {?}
                     */
                    (r) => r.data));
                else
                    this.contextMenuComponent.data = [row.data];
                this.contextMenuComponent.showContextMenu(event);
            }
        });
        this.next = (/**
         * @return {?}
         */
        () => {
            if (this.page + 1 <
                Math.ceil(this.table.getChildren().length / this.perPage))
                this.page++;
        });
        this.previous = (/**
         * @return {?}
         */
        () => {
            if (this.page > 0)
                this.page--;
        });
    }
    /**
     * @param {?} list
     * @return {?}
     */
    set list(list) {
        list.data = this.table;
        // console.log(list);
        this.onListInit.emit(list);
    }
    /**
     * @param {?} contextMenuComponent
     * @return {?}
     */
    set contextMenu(contextMenuComponent) {
        this.contextMenuComponent = contextMenuComponent;
        this.contextMenuComponent.setModel(this.table.getContextMenu());
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentClick(event) {
        this.showContextMenu = false;
        if (!event.ctrlKey)
            this.selectedRows = [];
    }
    /**
     * @param {?} event
     * @return {?}
     */
    documentRClick(event) {
        this.showContextMenu = false;
        if (!event.ctrlKey)
            this.selectedRows = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.isValid = this.table.validate();
        if (!this.isValid)
            throw new Error("Number of header items must be the same as the number in each row");
        // console.log("MyAngularTableComponent init");
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
        // console.log(changes);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        // console.log("MyAngularTableComponent destroyed");
    }
}
MyAngularTableComponent.decorators = [
    { type: Component, args: [{
                selector: "lib-my-angular-table",
                template: "<style>\r\n\r\n    .example-list {\r\n        /* width: 500px; */\r\n        /* max-width: 100%; */\r\n        /* border: solid 1px #ccc; */\r\n        /* min-height: 60px; */\r\n        display: block;\r\n        /* background: white; */\r\n        /* border-radius: 4px; */\r\n        /* overflow: hidden; */\r\n    }\r\n\r\n    .example-box {\r\n        /* padding: 20px 10px; */\r\n        /* border-bottom: solid 1px #ccc; */\r\n        /* color: rgba(0, 0, 0, 0.87); */\r\n        /* display: flex; */\r\n        /* flex-direction: row; */\r\n        /* align-items: center; */\r\n        /* justify-content: space-between; */\r\n        /* box-sizing: border-box; */\r\n        cursor: move;\r\n        /* background: white; */\r\n        /* font-size: 14px; */\r\n    }\r\n\r\n    .cdk-drag-preview {\r\n        box-sizing: border-box;\r\n        border-radius: 4px;\r\n        box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),\r\n                    0 8px 10px 1px rgba(0, 0, 0, 0.14),\r\n                    0 3px 14px 2px rgba(0, 0, 0, 0.12);\r\n    }\r\n\r\n    .cdk-drag-placeholder {\r\n        opacity: 0;\r\n    }\r\n\r\n    .cdk-drag-animating {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n    .example-box:last-child {\r\n        border: none;\r\n    }\r\n\r\n    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {\r\n        transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);\r\n    }\r\n\r\n</style>\r\n\r\n<section\r\n    *ngIf=\"this.isValid\"\r\n    fxLayout=\"column\"\r\n    fxLayoutAlign=\"start center\">\r\n        <section\r\n            class=\"no-padding\"\r\n            fxLayout=\"column\"\r\n            fxLayoutAlign=\"start center\"\r\n            fxLayoutGap=\"25px\">\r\n\r\n                <!-- FILTER -->\r\n                <mat-form-field>\r\n                    <mat-label>filter...</mat-label>\r\n                    <input \r\n                        matInput\r\n                        type=\"text\"\r\n                        [(ngModel)]=\"criteria\"\r\n                        (ngModelChange)=\"onCriteriaChange()\">\r\n                </mat-form-field>\r\n\r\n                \r\n                <section \r\n                    class=\"no-padding example-list\"\r\n                    cdkDropList\r\n                    #list=\"cdkDropList\">\r\n                    <!-- HEADER -->\r\n                    <section\r\n                        class=\"header\"\r\n                        fxLayout\r\n                        fxLayoutAlign=\"center center\"\r\n                        fxLayoutGap=\"10px\">\r\n                            <p\r\n                                class=\"header-item\"\r\n                                *ngFor=\"let headerItem of table.getHeader().getChildren()\"\r\n                                fxFlex\r\n                                (click)=\"onSort(headerItem.getParent().getChildIndex(headerItem))\">\r\n                                    {{ headerItem.context }}\r\n                            </p>\r\n                    </section>\r\n\r\n                    <section\r\n                        class=\"no-padding example-box\"\r\n                        cdkDrag\r\n                        *ngFor=\"let row of this.table.getFilteredChildren() | pagination: {page: this.page, perPage: this.perPage}\"\r\n                        libElColor\r\n                        [libElColorValue]=\"this.row.color\"\r\n                        [ngClass]=\"{'selected': this.isSelected(row)}\"\r\n                        (click)=\"onSelect($event, row)\"\r\n                        (contextmenu)=\"onContextMenu($event, row); false\">\r\n                            <section\r\n                                class=\"row\"\r\n                                *ngIf=\"row.show\"\r\n                                fxLayout\r\n                                fxLayoutAlign=\"start center\"\r\n                                fxLayoutGap=\"10px\">\r\n\r\n                                    <section\r\n                                        class=\"row-item no-padding\"\r\n                                        *ngFor=\"let rowItem of row.getChildren()\"\r\n                                        fxLayoutAlign=\"start center\"\r\n                                        fxLayoutGap=\"10px\"\r\n                                        fxFlex>\r\n                                            <img \r\n                                                fxHide.xs=\"true\"\r\n                                                *ngIf=\"rowItem.getImgSrc() ? true : false\"\r\n                                                [src]=\"rowItem.getImgSrc()\" \r\n                                                alt=\"row icon\">\r\n                                            <p \r\n                                                *ngIf=\"!rowItem.getButton() && !rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }}\r\n                                            </p>\r\n                                            <section\r\n                                                class=\"no-padding\"\r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\">\r\n                                                <img \r\n                                                    class=\"rating\"\r\n                                                    *ngFor=\"let i of rowItem.getRatingCollection()\"\r\n                                                    [src]=\"rowItem.getRatingImgSrc()\" \r\n                                                    alt=\"row icon\">\r\n                                            </section>\r\n                                            <!-- <p \r\n                                                *ngIf=\"!rowItem.getButton() && rowItem.getRating()\"\r\n                                                class=\"row-item\"\r\n                                                (click)=\"onShowSnackBar(rowItem.context)\">\r\n                                                    {{ rowItem.context }} rating\r\n                                            </p> -->\r\n                                            <button\r\n                                                *ngIf=\"rowItem.getButton()\"\r\n                                                (click)=\"rowItem.getFunction()([row.data])\"\r\n                                                class=\"row-item\">\r\n                                                    {{ rowItem.context }}\r\n                                            </button>\r\n                                    </section>\r\n                            </section>                            \r\n\r\n                            <section \r\n                                style=\"padding: 0;\"\r\n                                *ngIf='this.tempRef'>\r\n                                    <section *cdkDragPreview>\r\n                                        <section \r\n                                            *ngIf=\"false else this.tempRef\"></section>\r\n                                    </section>\r\n                            </section>\r\n\r\n                    </section>\r\n                </section>\r\n                \r\n                <section\r\n                    fxLayout\r\n                    fxLayoutAlign=\"center center\"\r\n                    fxLayoutGap=\"50px\">\r\n                    <mat-form-field\r\n                        fxFlex=\"50px\">\r\n                            <mat-label>per page:</mat-label>\r\n                            <input \r\n                                matInput\r\n                                type=\"number\"\r\n                                min=\"1\"\r\n                                [(ngModel)]=\"this.perPage\">\r\n                    </mat-form-field>\r\n                    <mat-button-toggle-group appearance=\"legacy\">\r\n                        <mat-button-toggle\r\n                            (click)=\"previous()\">\r\n                                <mat-icon>keyboard_arrow_left</mat-icon>\r\n                        </mat-button-toggle>\r\n                        <mat-button-toggle\r\n                            (click)=\"next()\">\r\n                                <mat-icon>keyboard_arrow_right</mat-icon>\r\n                        </mat-button-toggle>\r\n                    </mat-button-toggle-group>\r\n                </section>\r\n        </section>\r\n\r\n</section>\r\n\r\n<lib-my-angular-table-context-menu\r\n    [hidden]=\"!this.showContextMenu\"></lib-my-angular-table-context-menu>",
                styles: ["section{padding:10px}section.header{width:80vw;height:25px;padding:20px;border-top:1px solid grey;border-bottom:1px solid #d3d3d3}p.header-item{color:silver;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}p.header-item:hover{cursor:pointer;border-radius:2px;box-shadow:0 1px #d3d3d3}section.row{width:80vw;height:25px;padding:20px;box-shadow:0 1px #d3d3d3}section.row-item{cursor:default;overflow:hidden}p.row-item{color:grey;text-align:center;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item{overflow:hidden;white-space:nowrap;text-overflow:ellipsis}button.row-item:focus{outline:0}.no-padding{padding:0}.box-shadow{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}mat-form-field{max-width:300px}img{width:15%;height:15%;border-radius:40%}img.rating{width:12%;height:12%}hr{width:20px}button{border:none;background-color:transparent;cursor:pointer;height:30px;border-radius:5px}button:hover{background-color:#ebebeb}.no-margin{margin:0}.selected{background-color:rgba(221,221,221,.2);box-shadow:0 1px 1px rgba(0,0,0,.2)}"]
            }] }
];
/** @nocollapse */
MyAngularTableComponent.ctorParameters = () => [
    { type: UiService }
];
MyAngularTableComponent.propDecorators = {
    tempRef: [{ type: Input }],
    table: [{ type: Input }],
    perPage: [{ type: Input }],
    onListInit: [{ type: Output }],
    list: [{ type: ViewChild, args: ["list", { static: false },] }],
    contextMenu: [{ type: ViewChild, args: [ContextMenuComponent, { static: false },] }],
    documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }],
    documentRClick: [{ type: HostListener, args: ["document:contextmenu", ["$event"],] }]
};
if (false) {
    /** @type {?} */
    MyAngularTableComponent.prototype.tempRef;
    /** @type {?} */
    MyAngularTableComponent.prototype.table;
    /** @type {?} */
    MyAngularTableComponent.prototype.criteria;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.contextMenuComponent;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.selectedRows;
    /** @type {?} */
    MyAngularTableComponent.prototype.isValid;
    /** @type {?} */
    MyAngularTableComponent.prototype.perPage;
    /** @type {?} */
    MyAngularTableComponent.prototype.page;
    /** @type {?} */
    MyAngularTableComponent.prototype.onListInit;
    /** @type {?} */
    MyAngularTableComponent.prototype.showContextMenu;
    /** @type {?} */
    MyAngularTableComponent.prototype.onSort;
    /** @type {?} */
    MyAngularTableComponent.prototype.onCriteriaChange;
    /** @type {?} */
    MyAngularTableComponent.prototype.onShowSnackBar;
    /** @type {?} */
    MyAngularTableComponent.prototype.onSelect;
    /** @type {?} */
    MyAngularTableComponent.prototype.isSelected;
    /** @type {?} */
    MyAngularTableComponent.prototype.onContextMenu;
    /** @type {?} */
    MyAngularTableComponent.prototype.next;
    /** @type {?} */
    MyAngularTableComponent.prototype.previous;
    /**
     * @type {?}
     * @private
     */
    MyAngularTableComponent.prototype.uiService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const material = [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonToggleModule,
    MatIconModule,
    MatSnackBarModule,
    DragDropModule,
];
class MaterialModule {
}
MaterialModule.decorators = [
    { type: NgModule, args: [{
                imports: [...material],
                exports: [...material],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PaginationPipe {
    /**
     * @param {?} value
     * @param {?} data
     * @return {?}
     */
    transform(value, data) {
        // console.log(data);
        return value.slice(data.page * data.perPage, data.page * data.perPage + data.perPage);
    }
}
PaginationPipe.decorators = [
    { type: Pipe, args: [{ name: 'pagination' },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class LibElColorDirective {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.libElColorValue.currentValue)
            ((/** @type {?} */ (this.elRef.nativeElement))).style.backgroundColor =
                changes.libElColorValue.currentValue;
    }
}
LibElColorDirective.decorators = [
    { type: Directive, args: [{
                selector: "[libElColor]",
            },] }
];
/** @nocollapse */
LibElColorDirective.ctorParameters = () => [
    { type: ElementRef }
];
LibElColorDirective.propDecorators = {
    libElColorValue: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    LibElColorDirective.prototype.libElColorValue;
    /**
     * @type {?}
     * @private
     */
    LibElColorDirective.prototype.elRef;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SharedModule {
}
SharedModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ContextMenuComponent, PaginationPipe, LibElColorDirective],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                ],
                exports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MaterialModule,
                    FlexLayoutModule,
                    ContextMenuComponent,
                    PaginationPipe,
                    LibElColorDirective,
                ],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MyAngularTableModule {
}
MyAngularTableModule.decorators = [
    { type: NgModule, args: [{
                declarations: [MyAngularTableComponent],
                imports: [SharedModule],
                exports: [MyAngularTableComponent],
                providers: [],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ContextMenu, ContextMenuComponent, ContextMenuItem, MaterialModule, MyAngularTableComponent, MyAngularTableModule, MyAngularTableService, Row, RowItem, SharedModule, Table, UiService, Collection as ɵa, PaginationPipe as ɵb, LibElColorDirective as ɵc };
//# sourceMappingURL=my-angular-table.js.map
